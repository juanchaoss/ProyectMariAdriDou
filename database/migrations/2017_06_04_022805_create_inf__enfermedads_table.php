<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfEnfermedadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inf__enfermedads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enfermedad');
            $table->string('medicamentoF');
            $table->string('condiFisica');
            $table->string('retraso');
            $table->string('alergia');
            $table->string('regimen');
            $table->integer('sea');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inf__enfermedads');
    }
}
