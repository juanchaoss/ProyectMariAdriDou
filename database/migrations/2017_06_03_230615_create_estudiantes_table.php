<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('representante_id')->unsigned();
            $table->foreign('representante_id')->references('id')->on('representantes');
            $table->string('nombres');
            $table->string('apellidos');
            $table->text('fechaNa');
            $table->integer('nacionalidad');
            $table->integer('edad');
            $table->integer('sexo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiantes');
    }
}
