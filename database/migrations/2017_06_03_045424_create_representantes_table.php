<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nacionalidad');
            $table->string('cedula');
            $table->string('apellido');
            $table->string('nombre');
            $table->string('telefono');
            $table->string('nivel');
            $table->text('direcRD');
            $table->integer('tipovi');
            $table->integer('trabaja');
            $table->text('direcRT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
