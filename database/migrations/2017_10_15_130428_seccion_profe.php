<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeccionProfe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secciones_docentes',function(Blueprint $table){
            $table->increments('id');
            $table->integer('docente_id')->unsigned()->nullable();
            $table->foreign('docente_id')->references('id')->on('docentes');
            $table->integer('seccion_id')->unsigned()->nullable();;
            $table->foreign('seccion_id')->references('id')->on('secciones');
            $table->string('periodo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
