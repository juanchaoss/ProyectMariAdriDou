<?php

use Illuminate\Database\Seeder;

class userDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name'=>'root',
            'email'=>'default@gmail.com',
            'password'=>bcrypt("12345678"),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
