@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="infEst">
                            <h3 class="text-center text-primary">Registro de Estudiantes</h3><br>
                            <h5><strong>Datos del representante</strong></h5>
                                <div class="col-xs-2">
                                    <select name="" class="form-control input-sm" id="na">
                                    <option value="-">Nac.</option>
                                    <option value="1">V</option>
                                    <option value="2">E</option>
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control input-sm" id="searchRepre" placeholder="Cédula">
                                </div>  
                            <span><strong>Nombres:</strong><span id="nomR"></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span><strong>Apellidos:</strong><span id="apeR"></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span><strong>Dirección:</strong><span id="direR"></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" id="idRe">
                            <input type="hidden" id="id">
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <hr>
                        </div>
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li  role="presentation" class="active"><a id="estudi" href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos Básicos</a></li>
                                <li  role="presentation"><a id="enfer" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Condición Médica</a></li>
                                <li  role="presentation"><a id="habi" href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Hábitos</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <form action="" id="formEs">
                                        <div class="form-group"><br>
                                            <label for="">Apellidos</label>
                                            <input type="text" class="form-control" id="apellidosE" placeholder="Apellidos" onkeypress="validarLetras(event);">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nombres</label>
                                            <input type="text" class="form-control" id="nombresE" placeholder="Nombres" onkeypress="validarLetras(event);">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Fecha de Nacimiento</label>
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="fechaE"] class="form-control">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="idEstudi">
                                        <div class="form-group">
                                            <label for="">Nacionalidad</label>
                                            <select name="" class="form-control" id="nacionalidadE">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Venezolano</option>
                                                <option value="2">Extranjero</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Sexo</label>
                                            <select class="form-control" name="" id="sexoE">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Masculino</option>
                                                <option value="2">Femenino</option>
                                            </select>
                                        </div>
                                    </form>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <div id="infEnf"><br>
                                        <label class="col-md-12">¿Padece algúna enfermedad?</label>
                                        <div class="form-group col-md-3">
                                            <select name="" class="form-control" id="enfer" onchange="habilitarEnfermedad();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="enfermedad" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿En caso de fiebre que medicamento toma?</label>
                                        <div class="form-group col-md-12">
                                            <input type="text" class="form-control" id="fiebre">
                                        </div>
                                        <label class="col-md-12">¿Padece alguna condición física?</label>
                                        <div class="form-group col-md-3">
                                            <select name="" class="form-control" id="condi" onchange="habilitarCondicion();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="condicionf" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Sufre retraso de aprendizaje?</label>
                                        <div class="form-group col-md-3">
                                            <select name="" class="form-control" id="retraso" onchange="habilitarRetraso();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div  class="col-md-9">
                                            <input type="text" class="form-control" id="retrasoa" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Padece algúna alergia?</label>
                                        <div class="form-group col-md-3">
                                            <select name="" class="form-control" id="alerg" onchange="habilitarAlergia();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="alergia" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Posee un régimen alimenticio?</label>
                                        <div class="form-group col-md-3">
                                            <select name="" class="form-control" id="regi" onchange="habilitarRegimen();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="regimen" disabled="disabled">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Hará uso del S.A.E?</label>
                                            <select name="" class="form-control" id="sae">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <div id="infHabi">
                                        <div class="form-group col-md-12">
                                            <label for="">¿Come solo?</label>
                                            <select name="" class="form-control" id="come">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Desayuna antes de ir a la escuela?</label>
                                            <select name="" class="form-control" id="desayuna">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Tiene buen apetito?</label>
                                            <select name="" class="form-control" id="apetito">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Cómo es el sueño del niño?</label>
                                            <select name="" class="form-control" id="sueno">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Bueno</option>
                                                <option value="2">Regular</option>
                                                <option value="2">Malo</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Hora de dormir del niño?</label>
                                            <input type="text" class="form-control" id="hora">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Se orina?</label>
                                            <select name="" class="form-control" id="orina">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si, de dia</option>
                                                <option value="2">Si, de noche</option>
                                                <option value="3">Si, siempre</option>
                                                <option value="4">No</option>
                                            </select>
                                        </div>
                                        <div class="form-groupv col-md-12">
                                            <label for="">¿A qué edad dejó los pañales?</label>
                                            <select name="" class="form-control" id="panales">
                                                <option value="-">Seleccione</option>
                                                <option value="1">1 año</option>
                                                <option value="2">2 años</option>
                                                <option value="3">3 años</option>
                                                <option value="4">4 años</option>
                                                <option value="5">5 años</option>
                                            </select>
                                        </div>
                                        <div class="form-groupv col-md-12">
                                            <label for="">¿Puede ir al baño solo?</label>
                                            <select name="" class="form-control" id="bano">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="buttons" class="col-md-12">
                            <center id="groupBo">
                                <button type="reset" class="btn btn-primary">Cancelar</button>
                                <button type="button" class="btn btn-primary" id="btn1">Guardar<i class="fa fa-floppy-o"></i></button>
                            </center>
                        </div>
                    </div>
                        <hr>
                                
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="text-center text-primary col-md-12">Listado de Estudiantes</h3>
                                        <table class="table" id="tblEstudiante">
                                            <thead>
                                            <tr>
                                                <td>Cédula</td>
                                                <td>Apellidos</td>
                                                <td>Nombres</td>
                                                <td>Fecha Nac.</td>
                                                <td>Nacionalidad</td>
                                                <td>Edad</td>
                                                <td>Sexo</td>
                                                <td>Completado</td>
                                                <td>Acciones</td>
                                            </tr>
                                            </thead>
                                            <tbody id="regisEstu">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade" id="modalEstu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización los Datos</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button text-primary" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Datos Básicos
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <form action="" id="formEs">
                                        <div class="form-group col-md-12">
                                            <label for="">Nombres</label>
                                            <input type="text" class="form-control" id="nombresEUp" placeholder="Nombres" onkeypress="validarLetras(event);">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">Apellidos</label>
                                            <input type="text" class="form-control" id="apellidosEUp" placeholder="Apellidos" onkeypress="validarLetras(event);">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">Fecha de Nacimiento</label>
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" id="fechaEUp"] class="form-control">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">Nacionalidad</label>
                                            <select name="" class="form-control" id="nacionalidadEUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Venezolano</option>
                                                <option value="2">Extranjero</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">Sexo</label>
                                            <select class="form-control" name="" id="sexoEUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Masculino</option>
                                                <option value="2">Femenino</option>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed text-primary" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Datos de Enfermedad
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div id="infEnf">
                                    <label class="col-md-12">¿Padece alguna enfermedad?</label>
                                        <div class="form-group col-md-4">
                                            <select name="" class="form-control" id="enferUp" onchange="habilitarEnfermedadup();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="enfermedadUp" disabled="disabled">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿En caso de fiebre que medicamento toma?</label>
                                            <input type="text" class="form-control" id="fiebreUp">
                                        </div>
                                        <label class="col-md-12">¿Padece alguna condición física?</label>
                                        <div class="form-group col-md-4">
                                            <select name="" class="form-control" id="condiUp" onchange="habilitarCondicionup();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="condicionfUp" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Sufre retraso de aprendizaje?</label>
                                        <div class="form-group col-md-4">
                                            <select name="" class="form-control" id="retrasoUp" onchange="habilitarRetrasoup();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="retrasoaUp" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Padece alguna alergia?</label>
                                        <div class="form-group col-md-4">
                                            <select name="" class="form-control" id="alergUp" onchange="habilitarAlergiaup();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="alergiaUp" disabled="disabled">
                                        </div>
                                        <label class="col-md-12">¿Posee un regimen alimenticio?</label>
                                        <div class="form-group col-md-4">
                                            <select name="" class="form-control" id="regiUp" onchange="habilitarRegimenup();">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="regimenUp" disabled="disabled">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>¿Hará uso del S.A.E?</label>
                                            <select name="" class="form-control" id="saeUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed text-primary" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Datos de Hábitos
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div id="infHabi">
                                        <div class="form-group col-md-12">
                                            <label for="">¿Come solo?</label>
                                            <select name="" class="form-control" id="comeUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Desayuna antes de ir a la escuela?</label>
                                            <select name="" class="form-control" id="desayunaUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Tiene buen apetito?</label>
                                            <select name="" class="form-control" id="apetitoUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Cómo es el sueño del niño?</label>
                                            <select name="" class="form-control" id="suenoUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Bueno</option>
                                                <option value="2">Regular</option>
                                                <option value="2">Malo</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Hora de dormir del niño?</label>
                                            <input type="text" class="form-control" id="horaUp">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Se orina?</label>
                                            <select name="" class="form-control" id="orinaUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si, de dia</option>
                                                <option value="2">Si, de noche</option>
                                                <option value="3">Si, siempre</option>
                                                <option value="4">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿A qué edad dejó los pañales?</label>
                                            <select name="" class="form-control" id="panalesUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">1 año</option>
                                                <option value="2">2 años</option>
                                                <option value="3">3 años</option>
                                                <option value="4">4 años</option>
                                                <option value="5">5 años</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="">¿Puede ir al baño solo?</label>
                                            <select name="" class="form-control" id="banoUp">
                                                <option value="-">Seleccione</option>
                                                <option value="1">Si</option>
                                                <option value="2">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendEstUp" class="btn btn-primary">Actualizar <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
@include('footer')
@endsection