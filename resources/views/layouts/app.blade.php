<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'C.E.I. Cumanagotos') }}</title>
    <link rel="stylesheet" href="{{ asset('bower_components/datatables/media/css/jquery.dataTables.css') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('bower_components\bootstrap-multiselect\dist\css\bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/sweetalert2/dist/sweetalert2.css') }}">
    <!--Icons-->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/highcharts/css/highcharts.css')}}">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<input type="hidden" id="_token" value="{{ csrf_token() }}">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <div class="col-xs-4"><a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Cumanagotos') }} 
                    </a></div>
                    <div class="col-xs-8"><img src="{{ asset('img/mem.png') }}" style="width: 600px"></div>
                </div>
                <div class="collapse navbar-collapse center-text" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <center><ul class="nav navbar-nav navbar-right" >                       
                     @if( Auth::check() )
                            <li class="dropdown">
                                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Fichas personales <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/docentes') }}">Docentes</a></li>
                                    <li><a href="{{ url('/estudiantes') }}">Estudiantes</a></li>
                                    <li><a href="{{ url('/representantes') }}">Representantes</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Actividades <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/actividad') }}">Actividades</a></li>
                                    <li><a href="{{ url('/participa') }}">Parcipación</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Matriculación <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/seccione') }}">Secciones</a></li>
                                    <li><a href="{{ url('/periodo') }}">Periodo Académico</a></li>
                                    <li><a href="{{ url('/matriculacion') }}">Matriculación</a></li>
                                </ul>
                            </li>
                        @endif
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                        @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Reportes <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{url('/reporGra')}}">
                                                Reportes Gráficos
                                            </a>

                                        </li>
                                        <li>
                                            <a href="{{url('/reporPDF')}}">
                                                Reportes PDF
                                            </a>

                                        </li>
                                    </ul>
                                </li>
                            <li><a href="{{url('/history')}}">Historial</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        @if(Auth::user()->id == 1)
                                            <a href="{{ url('/usuarios') }}">
                                                Gestión de usuarios
                                            </a>
                                        @endif
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form> 
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul></center>
                </div>
            </div>
        </nav>

        @yield('content')    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/bootstrap-multiselect\dist\js\bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('bower_components/sweetalert2/dist/sweetalert2.js') }}"></script>
    
    <script src="{{ asset('js/docente.js') }}"></script>
    <script src="{{ asset('js/representante.js') }}"></script>
    <script src="{{ asset('js/seccion.js') }}"></script>
    <script src="{{ asset('js/estudiante.js') }}"></script>
    <script src="{{ asset('js/periodo.js') }}"></script>
    <script src="{{ asset('js/actividad.js') }}"></script>
    <script src="{{ asset('js/participa.js') }}"></script>
    <script src="{{ asset('js/comunes.js') }}"></script>
    <script src="{{ asset('js/matriculacion.js') }}"></script>
    <script src="{{ asset('js/reportGraph.js') }}"></script>
    <script src="{{ asset('js/reportPDF.js') }}"></script>
    <script src="{{ asset('js/users.js') }}"></script>
    <script src="{{ asset('js/history.js') }}"></script>
    <script>
        $(document).ready(function(){
            //gestion participacion
            $(document).on('click',"i[id^='showP']",function(){
                var id = $(this).attr('id').split('-').pop();
                getParEsp(id);
            });

            $(document).on('click',"i[id^='delP']",function(){
                var id = $(this).attr('id').split('-').pop();
                deletesParti(id);
            });
            getAllParti();
            /*tabs*/
            $("#estudi").click(function(){
                $("#btn2,#btn3").remove();
                $("#groupBo").append("<button id='btn1' class='btn btn-primary'>Guardar</button>");
            });

            $("#enfer").click(function(){
                $("#btn1,#btn3").remove();
                $("#groupBo").append("<button id='btn2' class='btn btn-primary'>Guardar</button>");
            });

            $("#habi").click(function(){
                $("#btn1,#btn2").remove();
                $("#groupBo").append("<button id='btn3' class='btn btn-primary'>Guardar</button>");
            });
            /*fin tabs*/

            /*CAMBIOS*/
            $("#enfer").click(function(){
                if(step === 0){
                    swal('ERROR','Debe registrar primero un estudiantes');
                    return;
                }
            });

            $("#habi").click(function(){
                if(step === 0){
                    swal('ERROR','Debe registrar primero un estudiantes');
                }
            });
            /*FIN*/
            /*******/
            getPeriodActive();
            getPeriodTeacherSeccion();
            $("#sendAttach").click(function(){
                saveAttach();
            });

            $("#fechaE").change(function(){
                var fecha = $(this).val();
                if(fecha.length === 0){
                    console.log("entro ===0");
                    return;
                }else{
                    var fechaTemp = fecha.split('/');
                    var comDate = new Date().getFullYear();
                    var less    = parseInt(comDate) - parseInt(fechaTemp[2]);

                    if(parseInt(less) <= 0){
                        console.log('entro swal');
                        swal("", "Fecha de nacimiento no admitida","warning");
                    }
                    else if(parseInt(less) > 2 || parseInt(less) < 6){
                        console.log('entro <>',less,parseInt(fechaTemp[2]),comDate);
                        return true;
                    }
                }
            });
            //Matriculacion
            getAllStudentMatri();
            //Docentes;

            obtenerDocentes();
            obtenerRepresentantes();
            getSection();
            $("#formDocen").submit(function(e){
                e.preventDefault();
                registrarDocente();
            });

            $(document).on('change','#secAct',function(){
                var idSecc = $("#secAct").val();
                getDocentes(idSecc);
            });

            $(document).on('change','#aaa',function(){
                var peri = $("#aaa").val();
                getSeccionh(peri);
            });

            $(document).on('change','#secAct',function(){
                var idSecc = $("#secAct").val();
                getEstudiantes(idSecc);
            });

            $(document).on('click',"li[id^='up-']",function(){
                var id = $(this).attr('id').split("-");
                $('#exampleModal').modal('show');
                obtenerDocenteEspecifico(id[1]);
            });

            $(document).on('click',"li[id^='de-']",function(){
                var id = $(this).attr('id').split("-");
                eliminarDocente(id[1]);
            });

            $(document).on('click','#sendUp',function(){
                actualizarDocente();
            });

            //REPRESENTANTES
            $(document).on('click',"label[act^='delR-']",function(){
                var id = $(this).attr('act').split("-");
                eliminarRepresentante(id[1]);
            });

            $(document).on('click',"label[act^='upR-']",function(){
                var id = $(this).attr('act').split("-");
                $('#modalR').modal('show');
                obtenerRepresentanteEspecifico(id[1],null,null);
            });

            $("#formRepre").submit(function(e){
                e.preventDefault();
                registrarRepresentante();
            });

            $("#sendUpR").click(function(){
                actualizarRepresentante();
            });

            //Periodos
            obtenerPeriodo();
            $("#formPeri").submit(function(e){
                e.preventDefault();
                registrarPeriodo();
            });
            $(document).on('click',"li[id^='upP-']",function(){
                var id = $(this).attr('id').split("-");
                $('#modalPeri').modal('show');
                obtenerPeriodoEspecifico(id[1]);
            });
            $(document).on('click',"li[id^='delP-']",function(){
                var id = $(this).attr('id').split("-");
                eliminarPeriodo(id[1]);
            });

            $("#sendPeriUp").click(function(){
                actualizarPeriodo();
            });
        });

        //Actividad
            obtenerActividad();
            $("#formAct").submit(function(e){
                e.preventDefault();
                registrarActividad();
            });
            $(document).on('click',"li[id^='upA-']",function(){
                var id = $(this).attr('id').split("-");
                $('#modalAct').modal('show');
                obtenerActividadEspecifico(id[1]);
            });
            $(document).on('click',"li[id^='delA-']",function(){
                var id = $(this).attr('id').split("-");
                eliminarActividad(id[1]);
            });

            $("#sendActiUp").click(function(){
                actualizarActividad();
            });

            //Secciones
            obtenerSeccione();
            $("#formSecc").submit(function(e){
                e.preventDefault();
                registrarSeccione();
            });
            $(document).on('click',"li[id^='upS-']",function(){
                var id = $(this).attr('id').split("-");
                $('#modalSec').modal('show');
                obtenerSeccioneEspecifico(id[1]);
            });
            $(document).on('click',"li[id^='deS-']",function(){
                var id = $(this).attr('id').split("-");
                eliminarSeccione(id[1]);
            });

            $("#sendSecUp").click(function(){
                actualizarSeccione();
            });

        //Estudiantes
        
        $("#searchRepre").blur(function(){
            var nac = $("#na").val();
            if($(this).val() === ""){
                $("#nomR,#apeR,#direR").text('');
            }else{
                obtenerRepresentanteEspecifico(null,$(this).val(),nac);
            }
        });

        $(document).on('click','#btn1',function(){
            registrarEstudiante(true);
        });

        $(document).on('click',"#btn2",function(){
            registroCondiMedico();
        });

        $(document).on('click',"#btn3",function(){
            registroHabito();
        });


        obtenerEstudiantes();
            $("#formEs").submit(function(e){
                e.preventDefault();
                registrarEstudiante();
            });
            $(document).on('click',"li[id^='upE-']",function(){
                var id = $(this).attr('id').split("-");
                $('#modalEstu').modal('show');
                obtenerEstudianteEspecifico(id[1]);
            });
            $(document).on('click',"li[id^='deE-']",function(){
                var id = $(this).attr('id').split("-");
                eliminarEstudiante(id[1]);
            });

            $("#sendEstUp").click(function(){
                actualizarEstudiante();
            });

        //cosa nueva
        $(document).on('keyup',function(){
            var email = $("#emailR").val();
            setTimeout(function(){
                validEmail(email,'validEmail');
            },1000)
        });
        /*$("#email").keyup(function(e){
           var email = $(this).val();
           validEmail(email,'validEmail');
        });*/

        $('#email').blur(function(){
            if($.trim($(this).val()) === ''){
                $('#email').attr('style','');
            }
        });

        //actividad - PARTICIPA
        getActivities();
        //getPartiTodos();
        $("#sendPar").click(function(){
            var todo;
            var seccion     = $("#secAct").val();
            var docente     = $("#doAct").val();
            if($("#allEst").prop('checked')){
                todo = 1;
                seccion = null;
                docente = null;
            }

            else todo = 0;

           // var docente     = $("#doAct").val();
           // var seccion     = $("#secAct").val();
            var actividad   = $("#listAct").val();
            var estudiantes = $("#estuAct").val();
            var valor       = $("#valor").val();
            if($.trim(actividad) === '' || $.trim(valor) === '-'){
                swal("Error","Existen campos vacíos","error");
                return;
            }
            saveParticipacion(todo,docente,estudiantes,actividad,seccion,valor);
        });
        var espanol = {
            
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            
        }

</script>
</body>
</html>
