@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center text-primary">Registro de Periódos Académicos</h4></div>
                    <div class="panel-body">
                        <form class="form-inline" id="formPeri">
                        <center>
                            <div class="form-group">
                                <label for="">Fecha Inicial</label>
                                <div class="input-group date" data-provide="datepicker">
                                <input type="text" id="fechaI"] class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            </div>
                      		<div class="form-group">
                                <label for="">Fecha Final</label>
                                <div class="input-group date" data-provide="datepicker">
                                <input type="text" id="fechaF"] class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            </div><br><br>
                            <div class="form-group">
                                <label for="">Descripción</label><br>
                                <textarea name="" id="descriP" class="form-control" cols="80" placeholder="Descripción" rows="5"></textarea>
                            </div><br><br></center>
                            <center><button type="submit" class="btn btn-primary">Guardar <i class="fa fa-floppy-o"></i></button>
                            <button type="reset" class="btn btn-primary">Cancelar</button></center>
                        </form>
                        <hr>
                        <h4 class="text-center text-primary">Docente - Sección</h4>
                        <form id="attachDS" class="form-inline">
                            <div class="form-group">
                                <label for="Docentes">Docentes</label>
                                <select class="form-control" name="docentes" id="docenteList">
                                    <option value="-">Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="secciones">Secciones</label>
                                <select class="form-control" name="secciones" id="seccionesList">
                                    <option value="-">Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="actPeriod">Periodo Actual</label>
                                <input type="text" name="actPeriod" class="form-control" id="actPeriod">
                            </div>
                            <br><br>
                            <center><button class="btn btn-primary" id="sendAttach" type="button">Guardar</button></center>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Periódos Académicos</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblPeriodo">
                            <thead>
                            <tr>
                                <td>Fecha Inicial</td>
                                <td>Fecha Final</td>
                                <td>Decripción</td>
                                <td>Estatus</td>
                                <td>Acción</td>
                            </tr>
                            </thead>
                            <tbody id="regisPeri">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
    <div class="modal fade" id="modalPeri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización de Datos</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" id="id">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Fecha Inicial</label>
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" id="fechaIUp"] class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                     	<div class="form-group">
                            <label for="recipient-name" class="control-label">Fecha Final</label>
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" id="fechaFUp"] class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Descripción</label>
                            <textarea class="form-control" id="descriPUp"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Estatus</label>
                            <input type="text" class="form-control" placeholder="1/0" id="status">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendPeriUp" class="btn btn-primary">Actualizar <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
 @include('footer')
@endsection

