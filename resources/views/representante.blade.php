@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Registro de Representantes</h3></div>
                    <div class="panel-body">
                        <form id="formRepre">
                            <label class="col-md-12">Cédula</label><br>
                            <div class="form-group col-md-3">
                                <select name="" class="form-control" id="nacR" onchange="habilitarCedula();">
                                    <option value="-">Seleccione</option>
                                    <option value="1">V</option>
                                    <option value="2">E</option>
                                </select>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="cedulaR" placeholder="Cédula" onkeypress="validarNumeros(event);" onchange="tama_ced(event);" maxlength="8" disabled="disabled">
                            </div><br>
                            <div class="form-group col-md-12">
                                <label for="">Apellidos</label>
                                <input type="text" class="form-control" id="apellidoR" placeholder="Apellido" onkeypress="validarLetras(event);">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Nombres</label>
                                <input type="text" class="form-control" id="nombresR" placeholder="Nombres" onkeypress="validarLetras(event);">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Teléfono</label>
                                <input type="text" class="form-control" id="telefonoR" placeholder="Teléfono" onkeypress="validarTelefonos(event);" maxlength="11">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Nivel de instrucción</label>
                                <select name="" class="form-control" id="niveliR">
                                    <option value="1">Ninguno</option>
                                    <option value="2">Primaria</option>
                                    <option value="3">Bachillerato</option>
                                    <option value="4">TSU</option>
                                    <option value="5">Universitario</option>
                                    <option value="6">Postgrado</option>
                                </select>
                            </div>
                            <input type="hidden" id="token" value="{{ csrf_token() }}">
                            <div class="form-group col-md-12">
                                <label for="">Dirección de la vivienda</label>
                                <textarea name="" id="direcRD" class="form-control" cols="5" placeholder="Dirección" rows="5"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Tipo de vivienda</label>
                                <select name="" class="form-control" id="tipovivienda">
                                    <option value="-">Seleccione</option>
                                    <option value="1">Rancho</option>
                                    <option value="2">Apartamentos en quintas o casas</option>
                                    <option value="3">Apartamentos en edificios</option>
                                    <option value="4">Casas</option>
                                    <option value="5">Quitas</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">¿Trabaja?</label>
                                <select name="" class="form-control" id="trabaR" onchange="habilitarTrabajo();">
                                    <option value="-">Seleccione</option>
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Dirección de trabajo</label>
                                <textarea name="" id="direcRT" class="form-control" cols="5" placeholder="Dirección" rows="5" disabled="disabled"></textarea>
                            </div>
                            <div class="col-md-12">
                            <center><button type="submit" class="btn btn-primary">Guardar <i class="fa fa-floppy-o"></i></button>
                            <button type="reset" class="btn btn-primary">Cancelar </button></center></div>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Representantes</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblRepresentante">
                            <thead>
                            <tr>
                                <td>Cédula</td>
                                <td>Apellidos</td>
                                <td>Nombres</td>
                                <td>Teléfono</td>
                                <td>Nivel de Instrucción</td>
                                <td>Dirección</td>
                                <td>Tipo de Vivienda</td>
                                <td>¿Trabaja?</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody id="regisRe">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
    <div class="modal fade" id="modalR" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización los Datos</h4>
                </div>
                <div class="modal-body">
                    <form id="formRepre">
                        <div class="form-group">
                            <label for="">Cédula</label>
                            <select name="" class="form-control" id="nacRe">
                                <option value="1">V</option>
                                <option value="2">E</option>
                            </select>
                            <input type="text" class="form-control" id="cedulaRe" placeholder="Cédula">
                        </div>
                        <div class="form-group">
                            <label for="">Apellidos</label>
                            <input type="text" class="form-control" id="apellidoRe" placeholder="Apellido">
                        </div>
                        <div class="form-group">
                            <label for="">Nombres</label>
                            <input type="text" class="form-control" id="nombresRe" placeholder="Nombres">
                        </div>
                        <div class="form-group">
                            <label for="">Teléfono</label>
                            <input type="text" class="form-control" id="telefonoRe" placeholder="Teléfono">
                        </div>
                        <div class="form-group">
                            <label for="">Nivel de instrucción</label>
                            <select name="" class="form-control" id="niveliRe">
                                <option value="1">Ninguno</option>
                                <option value="2">Primaria</option>
                                <option value="3">Bachillerato</option>
                                <option value="4">TSU</option>
                                <option value="5">Universitario</option>
                                <option value="6">Postgrado</option>
                            </select>
                        </div>
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idR">
                        <div class="form-group">
                            <label for="">Dirección de la vivienda</label>
                            <textarea name="" id="direcRDe" class="form-control" cols="5" placeholder="Dirección" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Tipo de vivienda</label>
                            <select name="" class="form-control" id="tipoviviendae">
                                <option value="1">Rancho</option>
                                <option value="2">Apartamentos en quintas o casa</option>
                                <option value="3">Apartamentos en edificios</option>
                                <option value="4">Casas</option>
                                <option value="5">Quitas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">¿Trabaja?</label>
                            <select name="" class="form-control" id="trabaRe">
                                <option value="1">Si</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Dirección de trabajo</label>
                            <textarea name="" id="direcRTe" class="form-control" cols="5" placeholder="Dirección" rows="5"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana<i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendUpR" class="btn btn-primary">Actualizar<i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
@include('footer')
@endsection