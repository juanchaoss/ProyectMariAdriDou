@extends('layouts.app')

@section('content')

 <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Reportes PDF</h3></div>
                    <div class="panel-body">
                        <div class="panel-group" id="accordion">
                          <div class="panel panel-default">
                            <a class="accordion-trigger" data-toggle="collapse" href="#collapseOne">
                              <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title">
                                    Estudiantes<i class="indicator glyphicon pull-right glyphicon-chevron-up" aria-hidden="true"></i>
                                </h4>
                              </div>
                            </a>
                            <div id="collapseOne" class="panel-collapse collapse in">
                              <div class="panel-body">
                                <input type="checkbox" id="Est" name="Est" onClick="hEst();">
                                <label for="">Todos</label><br>
                                <input type="checkbox" id="Par" name="Par" onClick="hPar();">
                                <label for="">Participaciones (*)</label><br>
                                <h5 class="panel-title text-primary" style="font-weight: bold; text-decoration: underline;">Filtrados:</h5><br>
                                <input type="checkbox" id="e1" name="e1" onClick="h1();"> 
                                <label for="">Por su cédula</label><br>
                                <div class="col-xs-4"><input type="text" class="form-control" id="ceduE" placeholder="Cédula" onKeyup="h1();" ></div><br><br>
                                <input type="checkbox" id="Psec" name="Psec" onClick="h4();">
                                <label for="">Sección/Sexo</label><br>
                                <div class="col-xs-4">
                                    <select class="form-control" name="secciones" id="secciones" onchange="h3(event);">
                                        <option value="-">Sección</option>
                                        <option value="m">Todas</option>
                                    </select>
                                </div>
                                <div class="col-xs-1">&nbsp;&nbsp;<i class="glyphicon glyphicon-arrow-right" aria-hidden="true"></i></div>
                                <div class="col-xs-4">
                                    <select class="form-control" name="" id="sexE" disabled="disabled">
                                        <option value="-">Sexo</option>
                                        <option value="1">Todos</option>
                                        <option value="2">Masculino</option>
                                        <option value="3">Femenino</option>
                                    </select>
                                </div><br><br><br>
                                <h5 class="panel-title text-primary" style="font-weight: bold; text-decoration: underline;">Específicos:</h5><br>
                                <div class="col-xs-4">
                                    <select class="form-control" name="" id="especif" onClick="h5();">
                                        <option value="-">Seleccione</option>
                                        <option value="1">Ordenanos por edad</option>
                                        <option value="2">Que hacen uso del SAE</option>
                                        <option value="3">Con alguna discapacidad de aprendizaje</option>
                                        <option value="4">Con régimen alimenticio</option>
                                        <option value="5">Con alguna discapacidad física</option>
                                        <option value="6">Que van solos al baño</option>
                                    </select>
                                </div><br><br>
                                <center><button id="bPDF" type="button" class="btn btn-primary">Generar Reporte</button></center>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <a class="accordion-trigger" data-toggle="collapse" href="#collapseTwo">
                              <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title">
                                  Docentes <i class="indicator glyphicon pull-right glyphicon-chevron-down" aria-hidden="true"></i>
                                </h4>
                              </div>
                            </a>
                            <div id="collapseTwo" class="panel-collapse collapse">
                              <div class="panel-body">
                                <input type="checkbox" id="Doc" name="Doc" onClick="hD1();">
                                <label for="">Todos</label><br>
                                <input type="checkbox" id="cedo" name="cedo" onClick="hD2();">
                                <label for="">Por su cédula</label><br>
                                <div class="col-xs-4">
                                <input type="text" class="form-control" id="ceduD" placeholder="Cédula" onKeyup="hD2();"></div><br><br>
                                <center><button id="bPDF2" type="button" class="btn btn-primary">Generar Reporte</button></center>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <a class="accordion-trigger" data-toggle="collapse" href="#collapseThree">
                              <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title">
                                  Representantes <i class="indicator glyphicon pull-right glyphicon-chevron-down" aria-hidden="true"></i> 
                                </h4>
                              </div>
                            </a>
                            <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <div class="form-group">
                                    <input type="checkbox" id="Rep" name="Rep" onClick="hR1();">
                                    <label for="">Todos</label><br>
                                    <input type="checkbox" id="eRep" name="eRep" onClick="hR2();">
                                    <label for="">Por su cédula</label><br>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" id="ceduR" placeholder="Cédula" onKeyup="hR2();">
                                </div><br><br><br>
                                <h5 class="panel-title text-primary" style="font-weight: bold; text-decoration: underline;">Específicos:</h5><br>
                                <div class="col-xs-4">
                                    <select class="form-control" name="" id="e6" onClick="hR3();">
                                        <option value="-">Seleccione</option>
                                        <option value="1">Que trabajan</option>
                                        <option value="2">Con sus tipos de vivienda</option>
                                    </select>
                                </div>
                                    <center><button id="bPDF3" type="button" class="btn btn-primary">Generar Reporte</button></center>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--S<div class="panel panel-default">
                            <a class="accordion-trigger" data-toggle="collapse" href="#collapseFour">
                              <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title">
                                  Secciones <i class="indicator glyphicon pull-right glyphicon-chevron-down" aria-hidden="true"></i> 
                                </h4>
                              </div>
                            </a>
                            <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <input type="checkbox" id="Sec" name="Sec" onClick="hSec();">
                                <label for="">Secciones</label>
                                <center><button id="bPDF4" type="button" class="btn btn-primary">Generar Reporte</button></center>
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <a class="accordion-trigger" data-toggle="collapse" href="#collapseFive">
                              <div class="panel-heading" data-parent="#accordion">
                                <h4 class="panel-title">
                                  Participaciones <i class="indicator glyphicon pull-right glyphicon-chevron-down" aria-hidden="true"></i> 
                                </h4>
                              </div>
                            </a>
                            <div id="collapseThree" class="panel-collapse collapse">
                              <div class="panel-body">
                                <input type="checkbox" id="Par" name="Par" onClick="hPar();">
                                <label for="">Participaciones (*)</label>
                                <center><button id="bPDF5" type="button" class="btn btn-primary">Generar Reporte</button></center>
                              </div>
                            </div>-->
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
        <footer class="navbar navbar-default navbar-fixed-bottom panel-footer">
          <div class="container">
            <center><span>Derechos reservados.</span></center>
          </div>
        </footer>
@endsection