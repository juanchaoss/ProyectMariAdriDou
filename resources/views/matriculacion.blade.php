@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center text-primary">Listado de Estudiantes - Matriculación</h4></div>
                    <div class="panel-body">
                        <table class="table" id="tableMatri">
                            <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Sexo</th>
                                    <th>Edad</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Sección</th>
                                    <th>Estatus</th>
                                    <th>Operaciones</th>
                                </tr>
                            </thead>
                            <tbody id="tblMatri">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--modelUp-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Inscripción</h4>
                </div>
                <div class="modal-body">
                    <span><strong>Nombre:</strong></span> <span id="name"></span>&nbsp;&nbsp;
                    <span><strong>Apellido:</strong></span> <span id="lastn"></span>&nbsp;&nbsp;
                    <span><strong>Edad:</strong></span> <span id="age"></span>&nbsp;&nbsp;
                    <span><strong>Fecha  Nacimiento:</strong></span> <span id="born"></span>
                    <hr>
                    <form>
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idEstu">
                        <label for="">Secciones</label>
                        <select name="" id="secciones" class="form-control">

                        </select>
                        <label for="">Periodo</label>
                        <input type="text" id="peri" class="form-control" disabled="true">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendInsc" class="btn btn-primary">Inscribir <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
<footer class="navbar navbar-default navbar-fixed-bottom panel-footer">
          <div class="container">
            <center><span class="text-muted">Derechos reservados.</span></center>
          </div>
        </footer>
@endsection