@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Registro de Actividades</h3></div>
                    <div class="panel-body">
                        <form id="formAct">
                            <div class="form-group col-md-12">
                                <label for="">Título</label>
                                <input type="text" class="form-control" id="tituloA" placeholder="Título">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Fecha</label>
                                <div class="input-group date" data-provide="datepicker">
                                        <input type="text" id="fechaA"] class="form-control">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Duración</label>
                                <input type="text" class="form-control" id="duraA" placeholder="Duración">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Lugar</label>
                                <input type="text" class="form-control" id="lugarA" placeholder="Lugar">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Descripción</label>
                                <textarea name="" id="descriA" class="form-control" cols="5" placeholder="Descripción" rows="5"></textarea>
                            </div>
                            <div class="col-md-12">
                                <center><button type="submit" class="btn btn-primary">Guardar <i class="fa fa-floppy-o"></i></button>
                                <button type="reset" class="btn btn-primary">Cancelar</button></center>
                            </div>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Actividades</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblActividad">
                            <thead>
                            <tr>
                                <td>Título</td>
                                <td>Fecha</td>
                                <td>Duración</td>
                                <td>Lugar</td>
                                <td>Descripción</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody id="regisAct">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
    <div class="modal fade" id="modalAct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización de Datos</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" id="idAct">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Título</label>
                            <input type="text" class="form-control" id="tituloAUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Fecha</label>
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" id="fechaAUp"] class="form-control">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Duración</label>
                            <input type="text" class="form-control" id="duraAUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Lugar</label>
                            <input type="text" class="form-control" id="lugarAUp">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Descripción</label>
                            <textarea class="form-control" id="descriAUp"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana<i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendActiUp" class="btn btn-primary">Actualizar<i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
 @include('footer')
@endsection
