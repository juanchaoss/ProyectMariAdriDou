@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Reportes Gráficos</h3></div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div id="Edades"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="Participacion"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="DiscapaciFisica"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="Viviendas"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container1"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container2"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container3"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container4"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="container5"></div>
                        </div>
                        <!--div id="container1" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('bower_components/highcharts/highcharts.js') }}"></script>
    <script src="{{ asset('bower_components/highcharts/js/modules/exporting.js') }}"></script>
    @include('footer')
@endsection