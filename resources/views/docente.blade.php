@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Registro de Docentes</h3></div>
                    <div class="panel-body">
                        <form id="formDocen">
                            <label class="col-md-12">Cédula</label><br>
                            <div class="col-md-3">
                            <select name="" class="form-control" id="nac_d" onchange="habilitarCedulaD(event);">
                                    <option value="-">Nac.</option>
                                    <option value="1">V</option>
                                    <option value="2">E</option>
                                </select></div>
                            <div class="form-group col-md-9">
                                <input type="text" class="form-control" id="cedu" name="cedu" placeholder="Cédula" maxlength="8" onchange="tam_ced(event);" disabled="disabled" onkeypress="validarNumeros(event);">
                            </div><br>
                            <div class="form-group col-md-12">
                                <label for="">Apellidos</label>
                                <input type="text" class="form-control" id="ape" placeholder="Apellido" onkeypress="validarLetras(event);">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Nombres</label>
                                <input type="text" class="form-control" id="nom" placeholder="Nombres" onkeypress="validarLetras(event);">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Nivel académico</label>
                                <select name="" class="form-control" id="nivel">
                                    <option value="-">Seleccione</option>
                                    <option value="1">Universitario</option>
                                    <option value="2">TSU</option>
                                    <option value="3">Bachiller</option>
                                    <option value="4">Nada</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Dirección</label>
                                <textarea name="" id="direc" class="form-control" cols="5" placeholder="Dirección" rows="5"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Teléfono</label>
                                <input type="text" class="form-control" id="tele" placeholder="Teléfono" onkeypress="validarNumeros(event);" maxlength="14">
                            </div>
                            <center><button type="submit" class="btn btn-primary">Guardar<i class="fa fa-floppy-o"></i></button>
                            <button type="reset" class="btn btn-primary">Cancelar</button></center>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Docentes</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblDocente">
                            <thead>
                            <tr>
                                <td>Nac.</td>
                                <td>Cédula</td>
                                <td>Apellidos</td>
                                <td>Nombres</td>
                                <td>Nivel Académico</td>
                                <td>Dirección</td>
                                <td>Teléfono</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody id="regisDo">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización de Datos</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <label for="">Nacionalidad</label>
                        <select name="" class="form-control" id="nac_dUp">
                            <option value="-">Seleccione</option>
                            <option value="1">V</option>
                            <option value="2">E</option>
                        </select>
                        <input type="hidden" id="id">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Cédula</label>
                            <input type="text" class="form-control" id="ceduUp" onChange="tam_ced();">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Apellidos</label>
                            <input type="text" class="form-control" id="apeUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nombres</label>
                            <input type="text" class="form-control" id="nomUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nivel académico</label>
                            <select name="" class="form-control" id="niUp">
                                <option value="-">Seleccione</option>
                                <option value="1">Universitario</option>
                                <option value="2">TSU</option>
                                <option value="3">Bachiller</option>
                                <option value="4">Nada</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Dirección</label>
                            <textarea class="form-control" id="diUp"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Teléfono</label>
                            <input type="text" class="form-control" id="teUp">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana<i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendUp" class="btn btn-primary">Actualizar<i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
 @include('footer')
@endsection
