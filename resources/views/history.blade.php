@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4 class="text-center text-primary">Historial</h4></div>
                    <br><br>
                    <div class="col-xs-5">
                        <label for="">Periodo Académico</label>
                    </div>
                    <div class="col-xs-5">
                        <label for="">Sección</label>
                    </div>
                    <div class="col-xs-5">
                        <select class="form-control" name="aaa" id="aaa">
                            <option value="-">Seleccione</option>
                        </select>
                    </div>
                    <div class="col-xs-5">
                        <select class="form-control" name="" id="seccionh">
                            <option value="-">Seleccione</option>
                        </select>
                    </div>
                    <div class="col-xs-2">
                        <center><button id="history" type="button" class="btn btn-primary">Consultar</button></center>
                    </div><br><br><br><br>
                    <h4 class="text-center text-primary">Resultados</h4>
                    <div class="text-primary col-xs-12"><strong>Datos del docente</strong></div><br><br>
                    <div class="col-xs-5">
                        <span><strong>Nombre:</strong></span> <span id="name"></span>
                    </div>
                    <div class="col-xs-5">
                        <span><strong>Apellido:</strong></span> <span id="lastn"></span>
                    </div><br><br>
                    <div class="text-primary col-xs-12"><strong>Datos del estudiante</strong></div><br>
                    <div class="panel-body">
                        <table class="table" id="tableHistory">
                            <thead>
                                <tr>
                                    <th>Cédula</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Edad</th>
                                    <th>Sexo</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tblHistory">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="navbar navbar-default navbar-fixed-bottom panel-footer">
          <div class="container">
            <center><span class="text-muted">Derechos reservados.</span></center>
          </div>
        </footer>
    
@endsection