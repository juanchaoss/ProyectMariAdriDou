@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Gestión de usuarios</h3></div>
                    <div class="panel-body">
                        <form id="formRepre">
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" id="name" class="form-control" placeholder="Name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="">Clave</label>
                                <input type="password" class="form-control" id="pass" placeholder="Clave">
                            </div>

                            <center>
                                <button type="button" id="saveUsers" class="btn btn-primary">Guardar <i class="fa fa-floppy-o"></i></button>
                                <button type="reset" class="btn btn-primary">Cancelar </button>
                            </center>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Usuarios</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblUsers">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody id="listUsers">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--modelUp-->
    <div class="modal fade" id="modalUsers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización los Datos</h4>
                </div>
                <div class="modal-body">
                    <form id="formRepre">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" id="nameUp">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" class="form-control" id="emailUp">
                        </div>
                        <div class="form-group">
                            <label for="">Clave</label>
                            <input type="password" class="form-control" id="claveUp">
                        </div>
                        <input type="hidden" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" id="idU">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendUpUser" class="btn btn-primary">Actualizar <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
@endsection