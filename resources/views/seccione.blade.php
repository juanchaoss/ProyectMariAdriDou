@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Registro de Secciones</h3></div>
                    <div class="panel-body">
                        <form id="formSecc">
                            <div class="form-group col-md-12">
                                <label for="">Identificador de la sección</label>
                                <input type="text" class="form-control" id="idsecc" placeholder="Identificador de la sección">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Turno</label>
                                <select name="" class="form-control" id="turnosecc">
                                    <option value="-">Seleccione</option>
                                    <option value="1">Mañana</option>
                                    <option value="2">Tarde</option>
                                </select>
                            </div>
                            <label class="col-md-12">Edades Comprendidas</label>
                            
                            <div class="form-group col-md-6">
                                <label>Desde</label>
                                <select name="" class="form-control" id="edaddesde">
                                    <option value="-">Seleccione</option>
                                    <option value="1">3</option>
                                    <option value="2">4</option>
                                    <option value="3">5</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Hasta</label>
                                <select name="" class="form-control" id="edadhasta">
                                    <option value="-">Seleccione</option>
                                    <option value="1">3</option>
                                    <option value="2">4</option>
                                    <option value="3">5</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="perido_id">Periodo</label>
                                <input type="text" readonly class="form-control" name="perido_id" id="periId">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Observaciones</label>
                                <textarea name="" id="obser" class="form-control" cols="5" placeholder="Obcervaciones" rows="5"></textarea>
                            </div>
                            <div class="col-md-12">
                                <center><button type="submit" class="btn btn-primary">Guardar<i class="fa fa-floppy-o"></i></button>
                                <button type="reset" class="btn btn-primary">Cancelar</button></center>
                            </div>
                        </form>
                    </div>
                </div>
                <h3 class="text-center text-primary">Listado de Secciones</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblSeccione">
                            <thead>
                            <tr>
                                <td>Identificador</td>
                                <td>Turno</td>
                                <td>Desde</td>
                                <td>Hasta</td>
                                <td>Obcervaciones</td>
                                <td>Acciones</td>
                            </tr>
                            </thead>
                            <tbody id="regisSecc">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
    <div class="modal fade" id="modalSec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualización de Datos</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" id="id">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Identificador</label>
                            <input type="text" class="form-control" id="idseccUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Turno</label>
                            <select name="" class="form-control" id="turnoseccUp">
                                <option value="-">Seleccione</option>
                                <option value="1">Mañana</option>
                                <option value="2">Tarde</option>
                            </select>
                        </div>
                        <input type="hidden" id="token" value = {{ csrf_token() }}>
                        <input type="hidden" id="idSec">
                        <div><label for="recipient-name" class="control-label">Edades comprendidas:</label></div>
                        <div class="form-group col-md-6">
                            <label for="recipient-name" class="control-label">Desde</label>
                            <select name="" class="form-control" id="edaddesdeUp">
                                <option value="-">Seleccione</option>
                                <option value="1">3</option>
                                <option value="2">4</option>
                                <option value="3">5</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="recipient-name" class="control-label">Hasta</label>
                            <select name="" class="form-control" id="edadhastaUp">
                                <option value="-">Seleccione</option>
                                <option value="1">3</option>
                                <option value="2">4</option>
                                <option value="3">5</option>
                            </select>
                        </div><br>
                        <div class="form-group" >
                            <label for="recipient-name" class="control-label">Observaciones</label>
                            <textarea class="form-control" id="obserUp"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="sendSecUp" class="btn btn-primary">Actualizar <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
@include('footer')
@endsection