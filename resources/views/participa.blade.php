@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center text-primary">Registro de Participaciones</h3></div>
                    <div class="panel-body">
                        <form id="formParti">
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                                <div id="aa">
                                    <div class="col-md-12">
                                        <label>Actividad:</label>
                                        <select id="listAct" class="form-control">
                                            <option value="-">Seleccione</option>
                                        </select>
                                    </div><br><br>
                                <div class="col-md-12">
                                    <input type="checkbox" id="allEst" name="AllEst">
                                    <label for="">Toda la institución</label>
                                </div>
                                <div class="col-md-12">
                                    <label for="">Sección</label>
                                    <select name="" id="secAct" class="form-control">
                                        <option value="-">Seleccione</option>
                                    </select>
                                </div><br>
                                <div class="col-md-12">
                                    <label for="">Docente</label>
                                    <select name="" id="doAct" class="form-control">
                                        <option value="-">Seleccione</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="">Estudiantes</label>
                                    <select id="estuAct" multiple="multiple">
                                    </select>
                                </div><br>
                                
                                <div class="col-md-12">
                                    <label>Resultado de la participación:</label>
                                    <select id="valor" class="form-control">
                                        <option value="-">Seleccione</option>
                                        <option value="1">Fue exitosa</option>
                                        <option value="2">No fue exitosa</option>
                                    </select>
                                </div>
                                <br><br><br>
                            <div class="col-md-12"><center><br><button type="button" id="sendPar" class="btn btn-primary">Guardar <i class="fa fa-floppy-o"></i></button>
                            <button type="reset" class="btn btn-primary">Cancelar</button><br></center></div>
                                </div>
                        </form>
                        <br><h3 class="text-center text-primary">Listado de Participaciones</h3>
                        <table class="table" id="tblParticipaciones">
                            <thead>
                                <tr>
                                    <td>Actividad</td>
                                    <td>Sección</td>
                                    <td>Docente</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody id="tblDataPar">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>

    <!--modelUp-->
    <div class="modal fade" id="modalParti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Información de la Participación</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="tblHeadModalPar">
                        <thead>
                            <tr>
                                <td>Actividad</td>
                                <td>Seccion</td>
                                <td>Docente</td>
                                <td>Estudiantes</td>
                            </tr>
                        </thead>
                        <tbody id="tblModalPar">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                </div>
            </div>
        </div>
    </div>
@include('footer')
@endsection