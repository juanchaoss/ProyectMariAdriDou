@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registro Estudiante</div>
                                
                            </div>
                            <form id="formEstu">
                           <div class=<div class="panel-body" id='form1'>
                            <div class="form-group">
                                <label for="">Cedula Estudiantil</label>
                                <input type="text" class="form-control" id="cedulaE" placeholder="Cedula Estudiantil">
                            </div>
                            <div class="form-group">
                                <label for="">Apellidos</label>
                                <input type="text" class="form-control" id="apellidosE" placeholder="Apellidos">
                            </div>
                            <div class="form-group">
                                <label for="">Nombres</label>
                                <input type="text" class="form-control" id="nombresE" placeholder="Nombres">
                            </div>
                            <div class="form-group">
                                <label for="">Fecha de Nacimiento</label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" id="fechaE"] class="form-control">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">¿Estudió en otro Plantel?</label>
                                <select name="" class="form-control" id="plantel">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="plantelE">
                            </div>
                            <div class="form-group">
                                <label for="">Nacionalidad</label>
                                <select name="" class="form-control" id="nacionalidadE">
                                    <option value="1">Venezolano</option>
                                    <option value="2">Extranjero</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Edad</label>
                                <input type="text" class="form-control" id="edadE" placeholder="EdadE">
                            </div>
                            <div class="form-group">
                                <label for="">Sexo</label>
                                <input type="text" class="form-control" id="sexoE" placeholder="SexoE">
                            </div>
                            <button type="submit" class="btn btn-primary" id="btn1">Guardar <i class="fa fa-floppy-o"></i></button>
                  </div>
                    <div class="panel-body" id='form2'>
                    	<div class="panel-heading">Condiciones Medicas</div>
                        	<div class="form-group">
                                <label for="">¿Padece alguna enfermedad?</label>
                                <select name="" class="form-control" id="enfer">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="enfermedad">
                            </div>
                            <div class="form-group">
                                <label for="">¿En caso de fiebre que medicamento toma?</label>
                                <input type="text" class="form-control" id="fiebre">
                            </div>
                            <div class="form-group">
                                <label for="">¿Padece alguna condición física?</label>
                                <select name="" class="form-control" id="condi">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="condicionf">
                            </div>
                            <div class="form-group">
                                <label for="">¿Sufre retraso de aprendizaje?</label>
                                <select name="" class="form-control" id="retraso">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="retrasoa">
                            </div>
                            <div class="form-group">
                                <label for="">¿Padece alguna alergia?</label>
                                <select name="" class="form-control" id="alerg">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="alergia">
                            </div>
                            <div class="form-group">
                                <label for="">¿Posee un regimen alimenticio?</label>
                                <select name="" class="form-control" id="regi">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                                <input type="text" class="form-control" id="regimen">
                            </div>
                            <div class="form-group">
                                <label for="">¿Hará uso del S.A.E?</label>
                                <select name="" class="form-control" id="sae">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-primary" id="btn2">Guardar <i class="fa fa-floppy-o"></i></button>
              </div>
                <div class="panel-body" id='form3'>
                    	<div class="panel-heading">Habitos</div>
                        	<div class="form-group">
                                <label for="">¿Come solo?</label>
                                <select name="" class="form-control" id="come">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                 <label for="">¿Desayuna antes de ir a la escuela?</label>
                                <select name="" class="form-control" id="desayuna">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                 <label for="">¿Tiene buen apetito?</label>
                                <select name="" class="form-control" id="apetito">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                 <label for="">¿Como es el sueño del niño?</label>
                                <select name="" class="form-control" id="sueno">
                                    <option value="1">Bueno</option>
                                    <option value="2">Regular</option>
                                    <option value="2">Malo</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">¿Hora de dormir del niño?</label>
                                <input type="text" class="form-control" id="hora">
                            </div>
                            <div class="form-group">
                                 <label for="">¿Se orina?</label>
                                <select name="" class="form-control" id="orina">
                                    <option value="1">Si, de dia</option>
                                    <option value="2">Si, de noche</option>
                                    <option value="3">Si, siempre</option>
                                    <option value="4">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                 <label for="">¿A qué edad dejó los pañales?</label>
                                <select name="" class="form-control" id="panales">
                                    <option value="1">1 año</option>
                                    <option value="2">2 años</option>
                                    <option value="3">3 años</option>
                                    <option value="4">4 años</option>
                                    <option value="5">5 años</option>
                                </select>
                            </div>
                            <div class="form-group">
                                 <label for="">¿Puede ir al baño solo?</label>
                                <select name="" class="form-control" id="bano">
                                    <option value="1">Si</option>
                                    <option value="2">No</option>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-primary" id="btn3">Guardar <i class="fa fa-floppy-o"></i></button>
                        
                    </div>
                    </form>
                <h3 class="text-center text-primary">Listado de Estudiante</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table" id="tblEstudiante">
                            <thead>
                            <tr>
                                <td>Representante</td>
                                <td>Cedula Estudiantil</td>
                                <td>Apellidos</td>
                                <td>Nombres </td>
                                <td>Fecha de Nacimiento</td>
                                <td>Antiguo Plantel</td>
                                <td>Nacionalidad</td>
                                <td>Edad</td>
                                <td>Sexo</td>
                            </tr>
                            </thead>
                            <tbody id="regisEstu">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    <!--modelUp-->
   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="exampleModalLabel">Actualizacion los Datos</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" id="id">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Representante</label>
                            <input type="text" class="form-control" id="representanteEUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Cedula Estudiantil</label>
                            <input type="text" class="form-control" id="cedulaEUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Apellidos</label>
                            <input type="text" class="form-control" id="apellidosEUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nombres</label>
                            <input type="text" class="form-control" id="nombresEUp">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Fecha de Nacimiento</label>
                            <div class="input-group date" data-provide="datepicker">
                                        <input type="text" id="fechaEUp"] class="form-control">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Antiguo Plantel</label>
                            <textarea class="form-control" id="plantelEUp"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nacionalidad</label>
                            <select name="" class="form-control" id="nacionalidadEUp">
                                <option value="-">...</option>
                                <option value="1">Venezolano</option>
                                <option value="2">Extranjero</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Edad</label>
                            <textarea class="form-control" id="edadEUp"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Sexo</label>
                            <input type="text" class="form-control" id="sexoEUp">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana <i class="fa fa-times-circle"></i></button>
                    <button type="button" id="senndUp" class="btn btn-primary">Actualizar <i class="fa fa-pencil-square"></i></button>
                </div>
            </div>
        </div>
    </div>
  
    <!--fin-->
@endsection
