var url = "http://localhost:8000/";

//var collect = require('collect.js');

$(document).on("click","i[id^='reti-']",function(){
    var id = $(this).attr('id').split('-').pop();
    swal({
        title: 'Alerta?',
        text: "¿Está seguro de retirar a este estudiante?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, retirar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        cambiarStatus(id)
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    })
});

$(document).on("click","i[id^='inscri-']",function(){
    var id = $(this).attr('id').split('-').pop();
    $("#idEstu").val(id);
    $("#exampleModal").modal("show");
    getEstudianteEsM(id);
    getPeriodoActivo();
    getSecciones();
});

$("#sendInsc").click(function(){
    var data = {
        "estudiante_id":$("#idEstu").val(),
        "seccion_id"   :$("#secciones").val(),
        "_token"       :$("#token").val(),
        "periodo"      :$("#peri").val()
    };

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de registrar esta inscripción?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        saveInscription(data);
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    })
});

function saveInscription(data){
    $.ajax({
        url:url+"saveInscription",
        type:"POST",
        dataType:"json",
        data:data,
        success:function(data){
            if(data){
                swal("Éxito","Inscripción registrada exitosamente","success");
                getAllStudentMatri();
            }else{
                swal("Error","Ha ocurrido un error, intente nuevamente","error");
                getAllStudentMatri();
            }
        },
        error:function(data){
            console.log("error");
        }
    });
}

function getEstudianteEsM(id){
    $.ajax({
        url:url+"obtenerEstu",
        type:"GET",
        dataType:"json",
        async:false,
        data:{id:id},
        success:function(data){
            if(data){
                $("#name").text(data['nombres']);
                $("#lastn").text(data['apellidos']);
                $("#born").text(data['fechaNa']);
                $("#age").text(data['edad']);
                $("#idEstu").val(data['id']);
                return;
            }
        },
        error:function(data){
            console.log("error");
        }
    });
}

function getPeriodoActivo(){
    $.ajax({
        url:url+"getPeriodoAct",
        type:"GET",
        dataType:"json",
        async:false,
        success:function(data){
            if(data){
                var anioI   = data['fechaI'].split('-') ;
                var anioF   = data['fechaF'].split('-');
                var periodo = anioI[0]+"-"+anioF[0];
                $("#peri").val(periodo);
                return;
            }
        },
        error:function(data){
            console.log("error");
        }
    });
}

function getSecciones(){
    $.ajax({
        url:url+"obtenerSeccione",
        type:"GET",
        dataType:"json",
        async:false,
        success:function(data){
            $("#secciones").html('');
            if(data.length >= 1){
                var str;
                for(var i = 0; i < data.length; i++){
                    str = "<option value="+data[i]['id']+">"+data[i]['idsecc']+"</option>";
                    $("#secciones").append(str);
                }
                return;
            }
        },
        error:function(data){
            console.log("error");
        }
    });
}

function cambiarStatus(idStu){
    $.ajax({
        url:url+"cambiarStatus",
        data:{id:idStu},
        dataType:"json",
        type:"GET",
        success:function(data){
            if(data){
                swal('Éxito','El estudiante ha sido retirado exitosamente','success');
                getAllStudentMatri();
            }else
                swal('Error','Ha ocurrido un error, intente luego','error');
        },
        error:function(){
            console.log("error");
        }
    });
}

function getAllStudentMatri(){
    $.ajax({
        url:url+"getStudiantesM",
        type:"GET",
        dataType:"json",
        success:function(data){
            if(data.length >= 1){
                $("#tblMatri").html('');
                for(var i = 0,j = data.length; i < j; i++){
                    var seccion;
                    if(data[i]['seccion'].length >= 1){
                        var index = parseInt(data[i]['seccion'].length)-1;
                        console.log(index,data[i]['seccion'],data[i]['seccion'][index]['idsecc']);
                        seccion = data[i]['seccion'][index]['idsecc'];
                    }else{
                        seccion = "No inscrito";
                    }
                    var sexo  = data[i]['sexo'] == 1 ? "Masculino" : "Femenino";
                    var status  = data[i]['status'] == 1 ? "Activo" : "Retirado";
                    var inscribir = "<i id=inscri-"+data[i]["id"]+" class=\"fa fa-file-text-o\ fa-2x\"></i>";
                    var remover   = "<i id=reti-"+data[i]["id"]+" class=\"fa fa-ban\ fa-2x\"></i>";
                    //var color = data[i]['seccion'].length >= 1 ? "tiene seccion" : class='text-danger';
                    var regis = "<tr>" +
                                    "<td>"+data[i]['nombres']+"</td>"+
                                    "<td>"+data[i]['apellidos']+"</td>"+
                                    "<td>"+sexo+"</td>"+
                                    "<td>"+data[i]['edad']+"</td>"+
                                    "<td>"+data[i]['fechaNa']+"</td>"+
                                    "<td>"+seccion+"</td>"+
                                    "<td>"+status+"</td>"+
                                    "<td>"+inscribir+" "+remover+"</td>"+
                               "</tr>";
                    $("#tblMatri").append(regis);
                }
                $("#tableMatri").DataTable();
            }
        },
        error:function(data){
            console.log("error");
        }
    });
    }

