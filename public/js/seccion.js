var url = "http://localhost:8000";

function registrarSeccione(){

    var idsec   = $("#idsecc").val();
    var turno   = $("#turnosecc").val();
    var edadd   = $("#edaddesde").val();
    var edadh   = $("#edadhasta").val();
    var obs     = $("#obser").val();
    var idPeriod = $("#periId").attr('data-input');

    if($.trim(idsec) == '' || $.trim(turno) == '' || $.trim(edadd) == '' || $.trim(edadh) == '' || $.trim(obs) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else if(edadd>edadh){
        swal("","El rango de edades introducido no es correcto","warning");
        return;
    }else{
    $.ajax({
        type: "POST",
        url: url+"/guardarSeccione",
        data:{
            _token:$("#_token").val(),
            idsecc:idsec,
            turnosecc:turno,
            edaddesde:edadd,
            edadhasta:edadh,
            obser:obs,
            periodo_id:idPeriod
        },
        dataType: "json",
        success: function(data){
            swal("Éxito","Registro realizado exitosamente","success");
            cleanFormSec();
            obtenerSeccione();
        },
        error: function(error){
            console.log(error);
        }
    });
    }
}

function cleanFormSec(){
    $("#idsecc").val('');
    $("#turnosecc").val('');
    $("#edaddesde").val('');
    $("#edadhasta").val('-');
    $("#obser").val('');
}

function obtenerSeccione(){

    $.ajax({
        type: "POST",
        url: url+"/obtenerSeccione",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        success: function(data){

            if(data){
                $("#regisSecc").html("");
                for(var i = 0, j = data.length; i < j; i++){
                    
                    var up  = "upS-"+data[i]['id'];
                    var del = "deS-"+data[i]['id'];
                    
                    if(data[i]['turnosecc'] == 1){
                        turno = "Mañana";
                    }else{
                        turno = "nada";
                    }

                    if(data[i]['edaddesde'] == 1){
                        desde = "3";
                    }else if(data[i]['edaddesde'] == 2){
                        desde = "4";
                    }else{
                        desde = "5";
                    }

                    if(data[i]['edadhasta'] == 1){
                        hasta = "3";
                    }else if(data[i]['edadhasta'] == 2){
                        hasta = "4";
                    }else{
                        hasta = "5";
                    }

                    registro = "<tr><td>"+data[i]['idsecc']+"</td><td>"+turno+"</td><td>"+desde+" años</td>" + "<td>"+hasta+" años</td>" +
                        "<td>"+data[i]['obser']+"</td>" +
                        "<td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;<li id="+del+" class='fa fa-trash-o fa-2x'></li></td></tr>";
                    $("#regisSecc").append(registro);
                }
                cleanFormSec();
                $('#tblSeccione').DataTable();
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarSeccione(id){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de eliminar esta sección?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarSeccione",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Sección eliminada exitosamente","success");
                    obtenerSeccione();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operador cancelada por el usuario',
                'error'
            )
        }
    });
}

function obtenerSeccioneEspecifico(id){
    $.ajax({
        type: "POST",
        url: url+"/obtenerSeccioneEsp",
        data:{
            _token:$("#_token").val(),
            id:id
        },
        dataType: "json",
        success: function(data){
            if(data){

                $("#idseccUp").val(data['idsecc']);
                $("#turnoseccUp").val(data['turnosecc']);
                $("#edaddesdeUp").val(data['edaddesde']);
                $("#edadhastaUp").val(data['edadhasta']);
                $("#obserUp").val(data['obser']);
                $("#idSec").val(data['id']);
                return;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarSeccione(){

    if(confirm("¿Está seguro de actualizar la sección?")){

        var idsecc    = $("#idseccUp").val();
        var turnosecc = $("#turnoseccUp").val();
        var edaddesde = $("#edaddesdeUp").val();
        var edadhasta = $("#edadhastaUp").val();
        var obser     = $("#obserUp").val();
        var id        = $("#idSec").val();

        if($.trim(idsecc) == '' || $.trim(turnosecc) == '-' || $.trim(edaddesde) == '-' || $.trim(edadhasta) == '-' || $.trim(obser) == '') {
            swal("Error","Existen campos vacíos","error");
            return;
        }
        $.ajax({
            type: "POST",
            url: url+"/actualizaSeccione",
            data:{
                _token:$("#_token").val(),
                idsecc:idsecc,
                turnosecc:turnosecc,
                edaddesde:edaddesde,
                edadhasta:edadhasta,
                obser:obser,
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Sección actualizada exitosamente","success");
                    $('#modalSec').modal('hide');
                    obtenerSeccione();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }else
        return false;
}
