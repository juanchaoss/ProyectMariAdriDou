/*$(document).on("click","#sendPar",function(){

});*/

function getActivities(){
    $.ajax({
        type: "GET",
        url: url+"/getActivities",
        dataType: "json",
        success: function(data){
            if(data){
                var activity;
                for(var i = 0; i < data.length; i++){
                    activity = "<option value = "+data[i]['id']+">"+data[i]['tituloA']+"</option>";
                    $("#listAct").append(activity);
                }
            }else if(data.length === 0){
                //alert("No existen actividades");
            }
        }
    });
}

function saveParticipacion(todo,docente,estudiantes,actividad,seccion,valor){
    $.ajax({
        type: "POST",
        url: url+"/saveParticipa",
        data:{_token:$("#_token").val(),
            todo:todo,
            docente:docente,
            estudiantes:estudiantes,
            actividad:actividad,
            valor:valor,
            seccion:seccion
        },
        dataType: "json",
        success: function(data){
            if(data){
                swal("Éxito","Participación registrada exitosamente","success");
                cleanFormPar();
                getAllParti();
            }else{
                swal("Error","Ya existe una participacion registrada con esa actividad","error");
            }
        }
    });
}

function cleanFormPar(){
    $("#listAct").val('');
    //$("#secAct").val('');
    $("#doAct").val('');
    $("#estuAct").val('-');
    $("#allEst").val('');
    $("#valor").val('-');
}

function getSection(){
    $.ajax({
        type: "POST",
        url: url+"/obtenerSeccione",
        data:{_token:$("#_token").val()},
        dataType: "json",
        success: function(data){
            if(data.length >= 1){
                for(var i = 0; i < data.length; i++){
                    var elem = "<option value='"+data[i]['id']+"'>"+data[i]['idsecc']+"</option>";
                    $("#secAct").append(elem);
                }
            }else if(data.length === 0){
                //alert("No existen secciones");
            }
        }
    });
}

function getEstudiantes(seccion_id){
    $.ajax({
        type: "GET",
        url: url+"/estuSec/"+seccion_id,
        data:{_token:$("#_token").val()},
        dataType: "json",
        success: function(data){
            $('#estuAct').html('');
            if(data){
                for(var i = 0; i < data['estudiantes'].length; i++){
                    var fullNameId = data['estudiantes'][i]['nombres']+" "+data['estudiantes'][i]['apellidos']+"-"+data['estudiantes'][i]['id'];
                    var elem = "<option value='"+data['estudiantes'][i]['id']+"'>"+fullNameId+"</option>";
                    $("#estuAct").append(elem);
                }
                $('#estuAct').multiselect();
            }else if(data.length === 0){
                //alert("No existen Estudiantes");
            }
        }
    });
}

function getPartiTodos(){
    $.ajax({
        type: "GET",
        url: url+"/getParTodos",
        dataType: "json",
        success: function(data){
            $('#tblDataPar').html('');
            if(data){
                //alert(data.length);
                for(var i = 0; i < data.length; i++){
                    var cell = "<tr>" +
                        "<td>"+data[i]['tituloA']+"</td>"+
                        "<td>Todos</td>"+
                        "<td>Todos</td>"+
                        "<td>&nbsp;&nbsp;<i id='delP-"+data[i]['id']+"' class='fa fa-trash-o fa-2x'></i></td>"+
                        "</tr>";
                    $("#tblDataPar").append(cell);
                }
                $("#tblParticipaciones").DataTable();
            }
        }
    });
}

function getAllParti(){
    getPartiTodos();
    $.ajax({
        type: "GET",
        url: url+"/getInfParti",
        dataType: "json",
        success: function(data){
            if(data){
                for(var i = 0; i < data.length; i++){
                    var cell = "<tr>" +
                                    "<td>"+data[i]['actividad']['tituloA']+"</td>"+
                                    "<td>"+data[i]['seccion']['idsecc']+"</td>"+
                                    "<td>"+data[i]['docente']['nombre']+"</td>"+
                                    "<td><i id='showP-"+data[i]['actividad']['id']+"' class='fa fa-window-restore fa-2x'></i>&nbsp;&nbsp<i id='delP-"+data[i]['actividad']['id']+"' class='fa fa-trash-o fa-2x'></i></td>"+
                                "</tr>";
                    $("#tblDataPar").append(cell);
                }
                cleanFormPar();
                $("#tblParticipaciones").DataTable();
            }
        }
    });
}

function getParEsp(id){
    $.ajax({
        type: "GET",
        url: url+"/getParEsp",
        dataType: "json",
        data:{actId:id},
        success: function(data){
            $('#tblModalPar').html('');
            if(data){
                $("#modalParti").modal('show');
                var list = "<ul>";
                for(var i = 0; i < data['estudiantes'].length; i++){
                    list = list+"<li>"+data['estudiantes'][i]['nombres']+"</li>";
                }
                list = list+"</ul>";
                var cell = "<tr>" +
                            "<td>"+data['actividad']['tituloA']+"</td>"+
                            "<td>"+data['seccion']['idsecc']+"</td>"+
                            "<td>"+data['docente']['nombre']+"</td>"+
                            "<td>"+list+"</td>"+
                           "</tr>";
                $("#tblModalPar").append(cell);
            }else if(data.length === 0){
            }
        }
    });
}

function deletesParti(id){
    swal({
        title: 'Alerta!',
        text: "¿Está seguro de eliminar esta participación?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "get",
            url: url+"deleteParti",
            dataType: "json",
            data:{idAct:id},
            success: function(data){
                swal('Éxito','Información de la participación ha sido eliminada','success');
                getAllParti();
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operacion cancelada por el usuario',
                'error'
            )
        }
    });
}

function getDocentes(seccion_id){
    $.ajax({
        type: "GET",
        url: url+"/docsec/"+seccion_id,
        data:{_token:$("#_token").val(),seccion_id:seccion_id},
        dataType: "json",
        success: function(data){
            if(data){
                var fullNameId = data['nombre']+" "+data['apellido'];
                var elem = "<option value='"+data['id']+"'>"+fullNameId+"</option>";
                $("#doAct").append(elem);
            }
        }
    });
}

$("#allEst").click(function(){
    if(!$(this).prop('checked')){
        $("#secAct").prop('disabled',false);
        $("#doAct").prop('disabled',false);
        $("#estuAct").multiselect('enable');
    }else{
        $("#estuAct").multiselect('disable');
        $("#secAct").prop('disabled',true);
        $("#doAct").prop('disabled',true);

    }
});
