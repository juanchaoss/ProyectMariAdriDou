function validEmail(email,ruta) {
    if($.trim(email) === ''){
        $("#infE").hide();
        return false;
    }

    $.ajax({
        type: "GET",
        url: url+"/"+ruta,
        data:{
            _token:$("#_token").val(),
            email:email
        },
        dataType: "json",
        success: function(data){
           if(data.length === 0){
               $('#emailR').attr('style','');
               $("#infE").show().removeClass('fa fa-times').addClass('fa fa-check');
           }
           else if(data[0]['email'] === email){
               $("#infE").show().removeClass('fa fa-check').addClass('fa fa-times');
               $('#emailR').css('border-color','red');
           }

        },error: function(error){
            console.log(error);
        }
    });
}