var  url = "http://localhost:8000/";

$(document).ready(function(){
    getAllUsers();
    $('#saveUsers').click(function(){
        saveUsers();
    });

    $(document).on('click',"i[id^='upU-']",function(){
        var id = $(this).attr('id').split('-').pop();
        $("#modalUsers").modal('show');
        getUserSpec(id);
    });

    $(document).on('click',"i[id^='delU-']",function(){
        var id = $(this).attr('id').split('-').pop();
        deleteUser(id);
    });

    $("#sendUpUser").click(function(){
        updateUser();
    });
});

function saveUsers(){
    var name  = $("#name").val();
    var email = $("#email").val();
    var pass  = $("#pass").val();

    if($.trim(name) === '' || $.trim(email) === '' || $.trim(pass) === ""){
        swal('ERROR',"Existen campos vacios",'warning');
        return;
    }

    $.ajax({
        url:url+'saveUsers',
        type:'POST',
        data:{
            name:name,
            email:email,
            password:pass,
            _token:$('#token').val()
        },
        success:function(data){
            if(data){
                swal('Existo','Usuario registrado exitosamente','success');
                getAllUsers();
            }else{
                swal('ERROR','Ha ocurrido un problema','warning');
            }
        },
        error:function(){
            swal('ERROR','Ha ocurrido un problema','warning');
        }
    });
}

function getAllUsers(){
    $.ajax({
        url:url+'getAllUsers',
        type:'GET',
        success:function(data){
            if(data.length >= 1){
                $("#listUsers").html('');
                for(var i = 0; i < data.length; i++){
                    var cell = "<tr><td>"+data[i]['name']+"</td><td>"+data[i]['email']+"</td><td><i id='upU-"+data[i]['id']+"' class='fa fa-pencil-square-o fa-2x'></i><i id='delU-"+data[i]['id']+"' class='fa fa-trash-o\ fa-2x'></i></td></tr>";
                    $("#listUsers").append(cell);
                }
                $("#tblUsers").DataTable();
            }
        },
        error:function(){
            swal('ERROR','Ha ocurrido un problema','warning');
        }
    });
}

function getUserSpec(id){
    $.ajax({
        url:url+'getUserSpec',
        type:'GET',
        data:{id:id},
        success:function(data){
            if(data){
                $('#nameUp').val(data['name']);
                $('#emailUp').val(data['email']);
                $('#claveUp').val('****');
                $("#idU").val(data['id']);
            }
        },
        error:function(){
            swal('ERROR','Ha ocurrido un problema','warning');
        }
    });
}

function updateUser(){
    var name  = $("#nameUp").val();
    var email = $("#emailUp").val();
    var pass  = $("#claveUp").val();
    var id    = $("#idU").val();
    if($.trim(name) === '' || $.trim(email) === '' || $.trim(pass) === "****"){
        swal('ERROR',"Existen campos vacios",'warning');
        return;
    }

    $.ajax({
        url:url+'updateUser',
        type:'POST',
        data:{
            name:name,
            email:email,
            password:pass,
            id:id,
            _token:$('#token').val()
        },
        success:function(data){
            if(data){
                swal('Existo','Usuario Actualizado exitosamente','success');
                $("#modalUsers").modal('hide');
                getAllUsers();
            }else{
                swal('ERROR','Ha ocurrido un problema','warning');
            }
        },
        error:function(){
            swal('ERROR','Ha ocurrido un problema','warning');
        }
    });
}

function deleteUser(id){
    swal({
        title: 'Advertencia!',
        text: "Estas seguro de eliminar a este usuario?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminarlo!',
        cancelButtonText: 'No, Cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            url:url+'deleteUser',
            type:'POST',
            data:{
                id:id,
                _token:$('#token').val()
            },
            success:function(data){
                if(data){
                    swal('Existo','Usuario Eliminado exitosamente','success');
                    $("#modalUsers").modal('hide');
                    getAllUsers();
                }else{
                    swal('ERROR','Ha ocurrido un problema','warning');
                }
            },
            error:function(){
                swal('ERROR','Ha ocurrido un problema','warning');
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operacion cancelada por el usuario',
                'error'
            );
        }
    });
}