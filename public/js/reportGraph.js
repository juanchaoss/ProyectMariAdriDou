
$(document).ready(function () {

    // Build the chart
    var edades = reportGraphEd();
    var tipoVi = reportGraphViviendas();
    var fisi = reportGraphDiscapacidadf();
    var gb  = reportGraphGB();
    var sea = reportGraphSEA();
    var trabajan = reportGraphRepreTrabaja();
    var banoSolo = reportGraphSoloBano();
    var retraso  = reportGraphRetraso();
    var regimen  = reportGraphRegimenAli();
    var partici  = reportGraphParticipa();


    Highcharts.chart('Edades', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Edades de los niños'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: '3 años',
                y: edades['tres']
            }, {
                name: '4 años',
                y: edades['cua'],
                sliced: true,
                selected: true
            }, {
                name: '5 años',
                y: edades['cin'],
                sliced: true,
                selected: true
            }]
        }]
    });
    Highcharts.chart('Participacion', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Muestra de participación en actividades'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Exitosa',
                y: partici['si']
            }, {
                name: 'No exitosa',
                y: partici['no'],
                sliced: true,
                selected: true
            }]
        }]
    });

      Highcharts.chart('Viviendas', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Tipos de viviendas'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Rancho',
                y: tipoVi['rancho']
            }, {
                name: 'Apartamentos en quintas o casas',
                y: tipoVi['apaquin'],
                sliced: true,
                selected: true
            }, {
                name: 'Apartamentos en edificios',
                y: tipoVi['apaedi'],
                sliced: true,
                selected: true
            }, {
                name: 'Casas',
                y: tipoVi['casas'],
                sliced: true,
                selected: true
            }, {
                name: 'Quintas',
                y: tipoVi['quintas'],
                sliced: true,
                selected: true
            }]
        }]
    });
      Highcharts.chart('DiscapaciFisica', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Niños con algúna discapacidad física'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Si',
                y: fisi['discapacidad']
            }, {
                name: 'No',
                y: fisi['discapacidadno'],
                sliced: true,
                selected: true
            }]
        }]
    });
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Niños que hacen uso del SAE'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Si',
                y: sea['sea']
            }, {
                name: 'No',
                y: sea['seaNo'],
                sliced: true,
                selected: true
            }]
        }]
    });
    //********************************************************************************************
    Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Cantidad de niños y niñas'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Niños',
                y: gb['boys']
            }, {
                name: 'Niñas',
                y: gb['girls'],
                sliced: true,
                selected: true
            }]
        }]
    });

    Highcharts.chart('container2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Representantes que Trabajan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Procentaje',
            colorByPoint: true,
            data: [{
                name: 'Trabajan',
                y: trabajan['si']
            }, {
                name: 'No Trabajan',
                y: trabajan['no'],
                sliced: true,
                selected: true
            }]
        }]
    });

    Highcharts.chart('container3', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estudiantes que van solos al baño'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            colorByPoint: true,
            data: [{
                name: 'Solo',
                y: banoSolo['solo']
            }, {
                name: 'Acompa#ado',
                y: banoSolo['noSolo'],
                sliced: true,
                selected: true
            }]
        }]
    });

    Highcharts.chart('container4', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estudiantes con discapacidad de aprendizaje'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            colorByPoint: true,
            data: [{
                name: 'Con Retraso',
                y: retraso['retraso']
            }, {
                name: 'Sin Retraso',
                y: retraso['noRetraso'],
                sliced: true,
                selected: true
            }]
        }]
    });

    Highcharts.chart('container5', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Estudiantes con régimen alimenticio'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Porcentaje',
            colorByPoint: true,
            data: [{
                name: 'Con regimen',
                y: regimen['regimen']
            }, {
                name: 'Sin regimen',
                y: regimen['sinRegimen'],
                sliced: true,
                selected: true
            }]
        }]
    });
});

function reportGraphEd(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphEd",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}
function reportGraphParticipa(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphParticipa",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}
function reportGraphViviendas(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphViviendas",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}
function reportGraphDiscapacidadf(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphDiscapacidadf",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}
function reportGraphGB(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphGB",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}

function reportGraphSEA(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphCountSEA",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}

function reportGraphRepreTrabaja(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphTrabaja",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}

function reportGraphSoloBano(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphSoloBano",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}

function reportGraphRetraso(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphRetraso",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}

function reportGraphRegimenAli(){
    var result = '';
    $.ajax({
        type: "GET",
        url: url+"/reportGraphRegimenAlimenticio",
        dataType: "json",
        async:false,
        success: function(data){
            if(data){
                result = data;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    return result;
}