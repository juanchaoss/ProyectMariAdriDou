var url = "http://localhost:8000";

function validYears(year1,year2){
    var yInit = year1.split('/');
    var yEnd  = year2.split('/');

    if(yInit[2] === yEnd[2]){
        return false;
    }else if(yEnd[2] < yInit[2]){
        return false;
    }
    return true;
}

function registrarPeriodo(){

    var fechaI   = $("#fechaI").val();
    var fechaF   = $("#fechaF").val();
    var descriP   = $("#descriP").val();

    if(!validYears(fechaI,fechaF)){
        swal("ERROR","Fecha final invalida","error");
        return;
    }

     if($.trim(fechaI) == '' || $.trim(fechaF) == '' || $.trim(descriP) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
        $.ajax({
        type: "POST",
        url: url+"/guardarPeriodo",
        data:{
            _token:$("#_token").val(),
            fechaI:fechaI,
            fechaF:fechaF,
            descriP:descriP
        },
        dataType: "json",
        success: function(data){
            if(data){
                if(data === 2){
                    swal("Error","Ya existe un periodo activo","error");
                    return;
                }else if(data === 3){
                    swal("Error","Fechas inválidas","error");
                    return;
                }else{
                    swal("Éxito","Registro realizado exitosamente","success");
                    cleanFormPero();
                    obtenerPeriodo();
                }
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    }
}

function cleanFormPero(){
    $("#fechaI").val('');
    $("#fechaF").val('');
    $("#descriP").val('');
}

function obtenerPeriodo(){

    $.ajax({
        type: "POST",
        url: url+"/obtenerPeriodo",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        success: function(data){
            $("#regisPeri").html('');
            if(data){
                var registro = "";
                for(var i = 0, j = data.length; i < j; i++){
                    var up = "upP-"+data[i]['id'];
                    var del = "delP-"+data[i]['id'];

                    if (data[i]['status'] == 1){
                        status= "Activo";
                    }else{
                        status= "Pasado";
                    }

                    registro = "<tr><td>"+data[i]['fechaI']+"</td><td>"+data[i]['fechaF']+"</td><td>"+data[i]['descriP']+"</td><td>"+status+
                        "</td><td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;</td></tr>";
                    
                    $("#regisPeri").append(registro);
                }
                cleanFormPero();
                $('#tblPeriodo').DataTable();
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarPeriodo(id){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de eliminar este periodo?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarPeriodo",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("","Periodo eliminado exitosamente",'success');
                    obtenerPeriodo();
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function obtenerPeriodoEspecifico(id){
    $.ajax({
        type: "POST",
        url: url+"/obtenerPeriodoEsp",
        data:{
            _token:$("#_token").val(),
            id:id
        },
        dataType: "json",
        success: function(data){
            if(data){

                $("#fechaIUp").val(data['fechaI']);
                $("#fechaFUp").val(data['fechaF']);
                $("#descriPUp").val(data['descriP']);
                $("#id").val(data['id']);
                return;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function getPeriodActive(){
    $.ajax({
        type: "GET",
        url: url+"/getPeriodActive",
        dataType: "json",
        success: function(data){
            if(data){
                if(data['fechaI'] && data['fechaF']){
                    var fechai = data['fechaI'].split(' ');
                    var fechaf = data['fechaF'].split(' ');
                    $("#periId").attr('data-input',data['id']).val(fechai[0]+' - '+fechaf[0]);
                }
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function getPeriodTeacherSeccion(){
    $.ajax({
        type: "GET",
        url: url+"/getPeriodTeacherSeccion",
        dataType: "json",
        success: function(data){
            if(data){
                if(data['docente'].length >= 1){
                    for(var i = 0; i < data['docente'].length; i++){
                        var nameTemp = data['docente'][i]['nombre'].split(' ');
                        var lastNameTemp = data['docente'][i]['apellido'].split(' ');
                        var optionTag = "<option value="+data['docente'][i]['id']+">"+nameTemp[0]+" "+lastNameTemp[0]+"</option>";
                        $("#docenteList").append(optionTag);
                    }
                }
                if(data['secciones'].length >= 1){
                        for(var j = 0; j < data['secciones'].length; j++){
                         var option = "<option value="+data['secciones'][j]['id']+">"+data['secciones'][j]['idsecc']+"</option>";
                            $("#secciones").append(option);
                        }
                    }

                if(data['periodo']){
                    var fechai = data['periodo']['fechaI'].split(' ');
                    var fechaf = data['periodo']['fechaF'].split(' ');
                    var fecha = fechai[0]+' - '+fechaf[0];
                    $("#actPeriod").val(fecha);
                    $("#actPeriod").attr({'data-input':data['periodo']['id'],disabled:true})
                }

                if(data['periodos'].length >= 1){
                    for(var i = 0; i < data['periodos'].length; i++){
                        var option = "<option value="+data['periodos'][i]['id']+">"+data['periodos'][i]['fechaI']+"/"+data['periodos'][i]['fechaF']+"</option>";
                        $("#aaa").append(option);
                    }
                }

                if(data['secciones'].length >= 1){
                    for(var j = 0; j < data['docente'].length; j++){
                        var option = "<option value="+data['secciones'][j]['id']+">"+data['secciones'][j]['idsecc']+"</option>";
                        $("#seccionesList").append(option);
                    }
                }
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function saveAttach(){
    var docente = $("#docenteList").val();
    var seccion = $("#seccionesList").val();
    var periodo = $("#actPeriod").val();

    if(docente === '-' || seccion === '-' || $.trim(periodo) === ''){
        swal("ERROR","Existen campos vacios","warning");
        return;
    }

    $.ajax({
        type: "POST",
        url: url+"/saveAttach",
        data:{
            _token:$("#_token").val(),
            docente_id:docente,
            seccion_id:seccion,
            periodo:periodo
        },
        dataType: "json",
        success: function(data){
            if(data){
                swal("Éxito","Operación realizada exitosamente","success");
                $("#docenteList").val('-');
                $("#seccionesList").val('-');
            }else{
                swal("Error","Ha ocurrido un error, posiblemente sea que el docente ya pertenece a una sección","warning");
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarPeriodo(){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de actualizar el periodo?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function (){

        var fechaI   = $("#fechaIUp").val();
        var fechaF   = $("#fechaFUp").val();
        var descriP  = $("#descriPUp").val();
        var id       = $("#id").val();
        var status   = $("#status").val();

        $.ajax({
            type: "POST",
            url: url+"/actualizarPeriodo",
            data:{
                _token:$("#_token").val(),
                fechaI:fechaI,
                fechaF:fechaF,
                descriP:descriP,
                id:id,
                status:status
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Periodo actualizado exitosamente","success");
                    $('#modalPeri').modal('hide');
                    obtenerPeriodo();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}