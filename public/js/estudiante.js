var url  = "http://localhost:8000";
var step = 0;

function registrarEstudiante(bool){

    var idRepre   = $("#idRe").val();
    var nombres   = $("#nombresE").val();
    var apellidos = $("#apellidosE").val();
    var fecha     = $("#fechaE").val();
    //var plantel   = $("#plantel").val();
    //var plantele  = $("#plantelE").val();
    var nac       = $("#nacionalidadE").val();
    var sexo      = $("#sexoE").val();
    //valid date
    var fechaTemp = fecha.split('/');
    var comDate = new Date().getFullYear();
    var less    = parseInt(comDate) - parseInt(fechaTemp[2]);
    var idEstu  = $("#idEstudi").val();

    if(parseInt(less) <= 0){
        console.log('entro swal');
        swal("", "Fecha de nacimiento no admitida","warning");
        return;
    }


  if($.trim(idRepre) == '' || $.trim(apellidos) == '' || $.trim(nombres) == '' || $.trim(fecha) == ''|| $.trim(nac) == '' || $.trim(sexo) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
        if(bool){
            $.ajax({
                type: "POST",
                url: url+"/guardarEstudiante",
                data:{
                    _token:$("#token").val(),
                    representante_id:idRepre,
                    nombres:nombres,
                    apellidos:apellidos,
                    fechaNa:fecha,
                    //plantel:plantel,
                    //plantelE:plantele,
                    nacionalidad:nac,
                    sexo:sexo,
                    idEstu:idEstu
                },
                dataType: "json",
                success: function(data){
                    if(data){
                        swal("Éxito","Registro realizado exitosamente (1/3)","success");
                        $("#id").val(data['id']);
                        $( "a[aria-controls|='profile']" ).tab('show');
                        $("#btn1").remove();
                        $("#groupBo").append("<button class='btn btn-primary' id='btn2'>Guardar <i class='class='fa fa-floppy-o'></i></button>");
                        $("#idEstudi").val(data['id']);
                        step = 1;
                    }else{
                        swal("Error","Ha ocurrido un error","warning");
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        }
    }
}

function registroHabito(){

    var come    = $("#come").val();
    var desayu  = $("#desayuna").val();
    var apetitu = $("#apetito").val();
    var sueno   = $("#sueno").val();
    var hora    = $("#hora").val();
    var orina   = $("#orina").val();
    var panales = $("#panales").val();
    var bano    = $("#bano").val();

   if($.trim(come) == '' || $.trim(desayu) == '' || $.trim(apetitu) == '' || $.trim(sueno) == '' || $.trim(hora) == '' || $.trim(orina) == ''|| $.trim(panales) == '' || $.trim(bano) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
    $.ajax({
        type: "POST",
        url: url+"/registrarHabito",
        data:{
            estudiante_id:$('#id').val(),
            come:come,
            desayuna:desayu,
            apetito:apetitu,
            suenio:sueno,
            dormir:hora,
            orina:orina,
            panales:panales,
            banio_so:bano,
            _token:$("#token").val()
        },
        dataType: "json",
        success: function(data){
            swal("Éxito","Registro realizado exitosamente (3/3)","success");
            step = 0;
            location.reload();
            limpiarForm();
            obtenerEstudiantes();
        },
        error: function(error){
            console.log(error);
        }
    });
 }
}

function registroCondiMedico(){

    var enfermedad = $("#enfer").val();
    var enferDesc  = $("#enfermedad").val();
    var fiebre     = $("#fiebre").val();
    var condiFi    = $("#condi").val();
    var condiDes   = $("#condicionf").val();
    var retraso    = $("#retraso").val();
    var retrosoDesc = $("#retrasoa").val();
    var alergia     = $("#alerg").val();
    var alegiaDesc  = $("#alergia").val();
    var regimen     = $("#regi").val();
    var regimenDes  = $("#regimen").val();
    var sae         = $("#sae").val();

   if($.trim(enfermedad) === '-' || $.trim(enferDesc) === '' || $.trim(fiebre) === ''
       || $.trim(condiFi) === '-' || $.trim(condiDes) === '' || $.trim(retraso) === '-'|| $.trim(retrosoDesc) === ''
       || $.trim(alergia) === '-'|| $.trim(alegiaDesc) === '' || $.trim(regimen) === '-'|| $.trim(regimenDes) === ''
       || $.trim(sae) === ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
    /*if(enfermedad === 2){
        enferDesc = "N/A";
    }

    if(condiFi === 2){
        condiDes = "N/A";
    }

    if(retraso === 2){
        retrosoDesc === "N/A";
    }

    if(alergia === 2){
        alegiaDesc === "N/A";
    }

    if(regimen === 2){
        regimenDes === "N/A";
    }*/

    $.ajax({
        type: "POST",
        url: url+"/registrarEnfermedad",
        data:{
            estudiante_id:$("#id").val(),
            enfermedad:enferDesc,
            medicamentoF:fiebre,
            condiFisica:condiDes,
            retraso:retrosoDesc,
            alergia:alegiaDesc,
            regimen:regimenDes,
            sea:sae,
            _token:$("#token").val()
        },
        dataType: "json",
        success: function(data){
            swal("Éxito","Registro realizado exitosamente (2/3)","success");
            $("a[aria-controls|='messages']" ).tab('show');
            $("#btn2").remove();
            $("#groupBo").append("<button class='btn btn-primary' id='btn3'>Guardar <i class='class='fa fa-floppy-o'></i></button>");
            step = 2;
        },
        error: function(error){
            console.log(error);
        }
    });
    }
}

function limpiarForm(){
    $("#representanteE").val('');
    $("#cedulaE").val('');
    $("#nombresE").val('');
    $("#apellidosE").val('');
    $("#fechaE").val('');
    //$("#plantel").val('');
    //$("#plantelE").val('');
    $("#nacionalidadE").val('');
    $("#edadE").val('');
    $("#sexoE").val('');
}

function obtenerEstudiantes(){
    var registro = '';
    var nacio = '';
    var sexo='';

    $.ajax({
        type: "POST",
        url: url+"/obtenerEstudiantes",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        success: function(data){
            if(data){
                $("#regisEstu").html('');

                for(var i = 0, j = data.length; i < j; i++){
                    
                    var up  = "upE-"+data[i]['id'];
                    var del = "deE-"+data[i]['id'];
                    
                    if(data[i]['nacionalidad'] == 2){
                        nacio = "E";
                    }else{
                        nacio = "V";
                    }

                    if(data[i]['sexo'] == 2){
                        sexo = "F";
                    }else{
                        sexo = "M";
                    }
                    if(data[i]['enfermedad'] === null || data[i]['habito'] === null){
                        registro = "<tr class='danger'><td>"+data[i]['cedula']+"</td><td>"+data[i]['nombres']+"</td><td>"+data[i]['apellidos']+"</td><td>"+data[i]['fechaNa']/*+"</td><td>"+data[i]['plantele']*/+"</td><td>"+nacio+"</td><td>"+data[i]['edad']+"</td><td>"+sexo+"</td><td><i class='fa fa-times'></i></td>"+
                            "<td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;<li id="+del+" class='fa fa-trash-o fa-2x'></li></td></tr>";
                    }else{
                        registro = "<tr><td>"+data[i]['cedula']+"</td><td>"+data[i]['nombres']+"</td><td>"+data[i]['apellidos']+"</td><td>"+data[i]['fechaNa']/*+"</td><td>"+data[i]['plantele']*/+"</td><td>"+nacio+"</td><td>"+data[i]['edad']+"</td><td>"+sexo+"</td><td><i class='fa fa-check'></i></td>"+
                            "<td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;<li id="+del+" class='fa fa-trash-o fa-2x'></li></td></tr>";
                    }

                    $("#regisEstu").append(registro);
                }
                limpiarForm();
                $("#tblEstudiante").DataTable();
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarEstudiante(id){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de eliminar este estudiante?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarEstudiante",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Estudiante eliminado exitosamente","success");
                    obtenerEstudiantes();
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function obtenerEstudianteEspecifico(id){
    $.ajax({
        type: "POST",
        url: url+"/obtenerEstudianteEsp",
        data:{
            _token:$("#_token").val(),
            id:id
        },
        dataType: "json",
        success: function(data){
            if(data){

              //  $("#cedulaEUp").val(data['cedula']);
                $("#nombresEUp").val(data['estudiante']['nombres']);
                $("#apellidosEUp").val(data['estudiante']['apellidos']);
                $("#fechaEUp").val(data['estudiante']['fechaNa']);
                //$("#plantelUp").val(data['plantel']);
                //$("#plantelEUp").val(data['plantele']);
                $("#nacionalidadEUp").val(data['estudiante']['nacionalidad']);
                $("#sexoEUp").val(data['estudiante']['sexo']);
                $("#id").val(id);
                //++++++++++++++++Enfermedad
                $("#enferUp").val(data['enfermedad']['nacionalidad']);
                $("#enfermedadUp").val(data['enfermedad']['enfermedad']);
                $("#fiebreUp").val(data['enfermedad']['medicamentoF']);
                $("#condiUp").val(data['enfermedad']['nacionalidad']);
                $("#condicionfUp").val(data['enfermedad']['condiFisica']);
                $("#retrasoUp").val(data['enfermedad']['retraso']);
                $("#retrasoaUp").val(data['enfermedad']['retraso']);
                $("#alergUp").val(data['enfermedad']['alergia']);
                $("#alergiaUps").val(data['enfermedad']['alergia']);
                $("#regiUp").val(data['enfermedad']['regimen']);
                $("#regimenUp").val(data['enfermedad']['regimen']);
                $("#saeUp").val(data['enfermedad']['sea']);
                //######################Habito
                $("#comeUp").val(data['habito']['come']);
                $("#desayunaUp").val(data['habito']['desayuna']);
                $("#apetitoUp").val(data['habito']['apetito']);
                $("#suenoUp").val(data['habito']['suenio']);
                $("#horaUp").val(data['habito']['dormir']);
                $("#orinaUp").val(data['habito']['orina']);
                $("#panalesUp").val(data['habito']['panales']);
                $("#banoUp").val(data['habito']['banio_so']);

                return;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarEstudiante(){

    if(confirm("¿Está seguro de actualizar al estudiante?")){

        //var repre    = $("#representanteEUp").val();
        //var ced    = $("#cedulaEUp").val();
        var nom    = $("#nombresEUp").val();
        var apell    = $("#apellidosEUp").val();
        var fech    = $("#fechaEUp").val();
        //var plan    = $("#plantelUp").val();
       // var plant    = $("#plantelEUp").val();
        var nac    = $("#nacionalidadEUp").val();
        var sex    = $("#sexoEUp").val();
        var id     =$("#id").val();
        //Habito
        var come    = $("#comeUp").val();
        var desayuna  = $("#desayunaUp").val();
        var apetito = $("#apetitoUp").val();
        var suenio   = $("#suenoUp").val();
        var dormir    = $("#horaUp").val();
        var orina   = $("#orinaUp").val();
        var panales = $("#panalesUp").val();
        var banio_so    = $("#banoUp").val();
        //Enfermedad
        var enfermedad = $("#enferUp").val();
        var enferDesc  = $("#enfermedadUp").val();
        var fiebre     = $("#fiebreUp").val();
        var condiFi    = $("#condiUp").val();
        var condiDes   = $("#condicionfUp").val();
        var retraso    = $("#retrasoUp").val();
        var retrosoDesc = $("#retrasoaUp").val();
        var alergia     = $("#alergUp").val();
        var alegiaDesc  = $("#alergiaUps").val();
        var regimen     = $("#regiUp").val();
        var regimenDes  = $("#regimenUp").val();
        var sae         = $("#saeUp").val();

        if ($.trim(nom) == '' || $.trim(apell) == '' || $.trim(fech) == ''|| $.trim(nac) == ''|| $.trim(sex) == '') {
            swal("Error","Existen campos vaíos","error");
            return;
        }

        $.ajax({
            type: "POST",
            url: url+"/actualizarEstudiante",
            data:{
                _token:$("#_token").val(),
                id:id,
                estudiante:{
                    nombres:nom,
                    apellidos:apell,
                    fechaNa:fech,
                    //plantel:plan,
                    //plantele:plant,
                    nacionalidad:nac,
                    sexo:sex
                },enfermedad:{
                    enfermedad:enfermedad,
                    enferDesc:enferDesc,
                    fiebre:fiebre,
                    condiFi:condiFi,
                    condiDes:condiDes,
                    retraso:retraso,
                    retrosoDesc:retrosoDesc,
                    alergia:alergia,
                    alegiaDesc:alegiaDesc,
                    regimen:regimen,
                    regimenDes:regimenDes,
                    sae:sae
                },habito:{
                    come:come,
                    desayuna:desayuna,
                    apetito:apetito,
                    suenio:suenio,
                    dormir:dormir,
                    orina:orina,
                    panales:panales,
                    banio_so:banio_so
                }


            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Estudiante actualizado exitosamente","success");
                    $('#modalEstu').modal('hide');
                    obtenerEstudiantes();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }else
        return false;
}

/*function habilitarPlantel()
{
 var plantel=document.getElementById('plantel').value; 
 var nada="N/A";
 if(plantel=='2')
 {
  document.getElementById('plantelE').disabled=true;
  document.getElementById('plantelE').value=nada;
 }
 else
    {
     document.getElementById('plantelE').disabled=false;
    }
}*/

function validarLetras(e){  //funcion que valida solo letras
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor>=48 && valor<=57) 
        e.preventDefault()
    }
}

function habilitarEnfermedad()
{
 var enfermedad=document.getElementById('enfer').value; 
 var nada="N/A";
 if(enfermedad=='2')
 {
  document.getElementById('enfermedad').disabled=true;
  document.getElementById('enfermedad').value=nada;
 }
 else
    {
     document.getElementById('enfermedad').disabled=false;
    }
}

function habilitarCondicion()
{
 var condicion=document.getElementById('condi').value; 
 var nada="N/A";
 if(condicion=='2')
 {
  document.getElementById('condicionf').disabled=true;
  document.getElementById('condicionf').value=nada;
 }
 else
    {
     document.getElementById('condicionf').disabled=false;
    }
}

function habilitarRetraso()
{
 var retraso=document.getElementById('retraso').value; 
 var nada="N/A";
 if(retraso=='2')
 {
  document.getElementById('retrasoa').disabled=true;
  document.getElementById('retrasoa').value=nada;
 }
 else
    {
     document.getElementById('retrasoa').disabled=false;
    }
}

function habilitarAlergia()
{
 var alergia=document.getElementById('alerg').value; 
 var nada="N/A";
 if(alergia=='2')
 {
  document.getElementById('alergia').disabled=true;
  document.getElementById('alergia').value=nada;
 }
 else
    {
     document.getElementById('alergia').disabled=false;
    }
}

function habilitarRegimen()
{
 var regimen=document.getElementById('regi').value; 
 var nada="N/A";
 if(regimen=='2')
 {
  document.getElementById('regimen').disabled=true;
  document.getElementById('regimen').value=nada;
 }
 else
    {
     document.getElementById('regimen').disabled=false;
    }
}

function habilitarEnfermedadup()
{
    var enfermedad=document.getElementById('enferUp').value;
    var nada="N/A";
    if(enfermedad=='2')
    {
        document.getElementById('enfermedadUp').disabled=true;
        document.getElementById('enfermedadUp').value=nada;
    }
    else
    {
        document.getElementById('enfermedadUp').disabled=false;
    }
}

function habilitarCondicionup()
{
    var condicion=document.getElementById('condiUp').value;
    var nada="N/A";
    if(condicion=='2')
    {
        document.getElementById('condicionfUp').disabled=true;
        document.getElementById('condicionfUp').value=nada;
    }
    else
    {
        document.getElementById('condicionfUp').disabled=false;
    }
}

function habilitarRetrasoup()
{
    var retraso=document.getElementById('retrasoUp').value;
    var nada="N/A";
    if(retraso=='2')
    {
        document.getElementById('retrasoaUp').disabled=true;
        document.getElementById('retrasoaUp').value=nada;
    }
    else
    {
        document.getElementById('retrasoaUp').disabled=false;
    }
}

function habilitarAlergiaup()
{
    var alergia=document.getElementById('alergUp').value;
    var nada="N/A";
    if(alergia=='2')
    {
        document.getElementById('alergiaUp').disabled=true;
        document.getElementById('alergiaUp').value=nada;
    }
    else
    {
        document.getElementById('alergiaUp').disabled=false;
    }
}

function habilitarRegimenup()
{
    var regimen=document.getElementById('regiUp').value;
    var nada="N/A";
    if(regimen=='2')
    {
        document.getElementById('regimenUp').disabled=true;
        document.getElementById('regimenUp').value=nada;
    }
    else
    {
        document.getElementById('regimenUp').disabled=false;
    }
}
