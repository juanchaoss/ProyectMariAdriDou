var url = "http://localhost:8000";

function registrarActividad(){
    var tituloA = $("#tituloA").val();
    var fechaA  = $("#fechaA").val();
    var duraA   = $("#duraA").val();
    var lugarA  = $("#lugarA").val();
    var descriA = $("#descriA").val();

    if($.trim(tituloA) == '' || $.trim(fechaA) == '' || $.trim(duraA) == '' || $.trim(lugarA) == '' || $.trim(descriA) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
    $.ajax({
        type: "POST",
        url: url+"/guardarActividad",
        data:{
            _token:$("#_token").val(),
            tituloA:tituloA,
            fechaA:fechaA,
            duraA:duraA,
            lugarA:lugarA,
            descriA:descriA
        },
        dataType: "json",
        success: function(data){
            swal("Éxito","Registro realizado exitosamente","success");
            cleanFormAct();
            obtenerActividad();
        },
        error: function(error){
            console.log(error);
        }
    });
    }
}

function cleanFormAct(){
    $("#tituloA").val('');
    $("#fechaA").val('');
    $("#duraA").val('');
    $("#lugarA").val('');
    $("#descriA").val('');
}

function obtenerActividad(){

    $.ajax({
        type: "POST",
        url: url+"/obtenerActividad",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        success: function(data){
            if(data){
                $("#regisAct").html('');
                for(var i = 0, j = data.length; i < j; i++){

                    var up = "upA-"+data[i]['id'];
                    var del = "delA-"+data[i]['id'];
                    var registro = "";
                    registro = "<tr><td>"+data[i]['tituloA']+"</td><td>"+data[i]['fechaA']+"</td><td>"+data[i]['duraA']+"</td><td>"+data[i]['lugarA']+
                        "</td><td>"+data[i]['descriA']+"</td>"+
                       "<td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;<li id="+del+" class='fa fa-trash-o fa-2x'></li></td></tr>";
                    $("#regisAct").append(registro);
                }
                cleanFormAct();
                $('#tblActividad').DataTable();
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarActividad(id){

    swal({
        title: '',
        text: "¿Seguro que desea eliminar esta actividad?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarActividad",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Actividad eliminada exitosamente","success");
                    cleanFormAct();
                    obtenerActividad();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function obtenerActividadEspecifico(id){
    $.ajax({
        type: "POST",
        url: url+"/obtenerActividadEsp",
        data:{
            _token:$("#_token").val(),
            id:id
        },
        dataType: "json",
        success: function(data){
            if(data){

                $("#tituloAUp").val(data['tituloA']);
                $("#fechaAUp").val(data['fechaA']);
                $("#duraAUp").val(data['duraA']);
                $("#lugarAUp").val(data['lugarA']);
                $("#descriAUp").val(data['descriA']);
                $("#idAct").val(data['id']);
                return;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarActividad(){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de actualizar la actividad?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var tituloA    = $("#tituloAUp").val();
        var fechaA = $("#fechaAUp").val();
        var duraA = $("#duraAUp").val();
        var lugarA = $("#lugarAUp").val();
        var descriA     = $("#descriAUp").val();
        var id        = $("#idAct").val();

        $.ajax({
            type: "POST",
            url: url+"/actualizarActividad",
            data:{
                _token:$("#_token").val(),
                tituloA:tituloA,
                fechaA:fechaA,
                duraA:duraA,
                lugarA:lugarA,
                descriA:descriA,
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Actividad actualizada exitosamente","success");
                    $('#modalAct').modal('hide');
                    obtenerActividad();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}