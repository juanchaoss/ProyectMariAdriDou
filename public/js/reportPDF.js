$(document).ready(function(){

	$(document).on('click','#bPDF',function(){

		if($("#Est").is(":checked")){
            var type = "allStu";
            window.open('reportPdf?typeReport='+type);
        }
        if($("#Par").is(":checked")){
            var type = "allPar";
            window.open('reportPdf?typeReport='+type);
        }
        if($("#Psec").is(":checked")||$('#secciones').val() != '-'){
            var sec = document.getElementById('secciones').value;
        	if(sec=='-') {
        		swal("","Debe seleccionar una opción en el apartado 'Sección'","warning");
        	}else if(sec=='m') {
        			var sexo = document.getElementById('sexE').value;
        			if (sexo=='-') {
        			swal("","Debe seleccionar una opción en el apartado 'Sexo'","warning");
        			}else if (sexo=='1'){
        				window.open('reportSex?');
        			}else if (sexo=='2'){
        				var sexo = "1";
        				window.open('reportPdfS?sexo='+sexo);
        			}else{
        				var sexo = "2";
        				window.open('reportPdfS?sexo='+sexo);
        			}
        	}else{
        		var sexo = document.getElementById('sexE').value;
        		if (sexo=='-') {
        			swal("","Debe seleccionar una opción en el apartado 'Sexo'","warning");
        		}else if (sexo=='1') {
        			window.open('reportSoloSec/'+sec);
        		}else if (sexo=='2'){
        			var sexo = "1";
	            	window.open('reportMasSecc/'+sec);
	        	}else{
	        		var sexo = "2";
	        		window.open('reportFemSecc/'+sec);
	        	}

        	}
        }
        if($("#e1").is(":checked")||$('#ceduE').val() != ""){
        	var cedula = document.getElementById('ceduE').value;
        	if(cedula=='') {
        		swal("","Introduzca la cédula del estudiante","warning");
        	}else{
	            window.open('reportPdfe?cedula='+cedula);
        	}
        }
        if($('#especif').val() != "-"){
        	switch ($('#especif').val()) {
            case '1':
            	var type = "estEdad";
            	return window.open('reportPdf?typeReport='+type);
                break;
            case '2':
            	var type = "sae";
            	return window.open('reportPdf?typeReport='+type);
                break;
            case '3':
            	var type = "ret";
            	return window.open('reportPdf?typeReport='+type);
                break;
            case '4':
            	var type = "reg";
            	return window.open('reportPdf?typeReport='+type);
                break;
            case '5':
            	var type = "disc";
            	return window.open('reportPdf?typeReport='+type);
                break;
           default:
           		var type = "ban";
                return window.open('reportPdf?typeReport='+type);
                break;
        	}
        }
	});
}); 
$(document).ready(function(){
	$(document).on('click','#bPDF2',function(){  
        if($("#cedo").is(":checked")||$('#ceduD').val() != ""){
        	var cedula = document.getElementById('ceduD').value;
        	if(cedula=='') {
        		swal("","Introduzca la cédula del docente","warning");
        	}else{
	            window.open('reportPdfd?cedula='+cedula);
        	}
        }    
        if($("#Doc").is(":checked")){
            var type = "allTeach";
            window.open('reportPdf?typeReport='+type);
        }
	});
});
$(document).ready(function(){
	$(document).on('click','#bPDF3',function(){  
		if($("#Rep").is(":checked")){
            var type = "allRep";
            window.open('reportPdf?typeReport='+type);
        }
        if($("#eRep").is(":checked")||$('#ceduR').val() != ""){
        	var cedula = document.getElementById('ceduR').value;
        	if(cedula=='') {
        		swal("","Introduzca la cédula del representante","warning");
        	}else{
	            window.open('reportPdfr?cedula='+cedula);
        	}
        }
        if($('#e6').val() != "-"){
        	switch ($('#e6').val()) {
            case '1':
            	var type = "tra";
            	return window.open('reportPdf?typeReport='+type);
                break;
            default:
           		var type = "viv";
                return window.open('reportPdf?typeReport='+type);
                break;
        	}
        }
	});
});
//Estudiantes
function hEst()
{
	if($("#Est").is(":checked"))
	 {
		document.getElementById('Par').disabled=true;
		document.getElementById('e1').disabled=true;
		document.getElementById('ceduE').disabled=true;
		document.getElementById('Psec').disabled=true;
		document.getElementById('secciones').disabled=true;
		document.getElementById('especif').disabled=true;
	 }else{
	 	document.getElementById('Par').disabled=false;
		document.getElementById('e1').disabled=false;
		document.getElementById('ceduE').disabled=false;
		document.getElementById('Psec').disabled=false;
		document.getElementById('secciones').disabled=false;
		document.getElementById('especif').disabled=false;
	 }
}
function hPar()
{
	if($("#Par").is(":checked"))
	 {
		document.getElementById('Est').disabled=true;
		document.getElementById('e1').disabled=true;
		document.getElementById('ceduE').disabled=true;
		document.getElementById('Psec').disabled=true;
		document.getElementById('secciones').disabled=true;
		document.getElementById('especif').disabled=true;
	 }else{
	 	document.getElementById('Est').disabled=false;
		document.getElementById('e1').disabled=false;
		document.getElementById('ceduE').disabled=false;
		document.getElementById('Psec').disabled=false;
		document.getElementById('secciones').disabled=false;
		document.getElementById('especif').disabled=false;
	 }
}
function h1(){
	if (document.getElementById('e1').checked==true||$('#ceduE').val() != ""){
	  		document.getElementById('Par').disabled=true;
			document.getElementById('Est').disabled=true;
			document.getElementById('Psec').disabled=true;
			document.getElementById('secciones').disabled=true;
			document.getElementById('especif').disabled=true;
	  	}else{
	  		document.getElementById('Par').disabled=false;
			document.getElementById('e1').disabled=false;
			document.getElementById('Est').disabled=false;
			document.getElementById('Psec').disabled=false;
			document.getElementById('secciones').disabled=false;
			document.getElementById('especif').disabled=false;
	  	}
}
function h4(){
	if (document.getElementById('Psec').checked==true||$('#seccionesList').val() != "-"){
	  		document.getElementById('Par').disabled=true;
			document.getElementById('e1').disabled=true;
			document.getElementById('ceduE').disabled=true;
			document.getElementById('Est').disabled=true;
			document.getElementById('especif').disabled=true;
	  	}else{
	  		document.getElementById('Par').disabled=false;
			document.getElementById('e1').disabled=false;
			document.getElementById('ceduE').disabled=false;
			document.getElementById('Est').disabled=false;
			document.getElementById('especif').disabled=false;
	  	}
}
function h3()
{
	 var se=document.getElementById('secciones').value; 
	 if(se=="-")
	 {
	  	document.getElementById('sexE').disabled=true;
	  	document.getElementById('sexE').value="";
	  	document.getElementById('Par').disabled=false;
		document.getElementById('e1').disabled=false;
		document.getElementById('ceduE').disabled=false;
		document.getElementById('Est').disabled=false;
		document.getElementById('especif').disabled=false;
	 }else{
	    document.getElementById('sexE').disabled=false;
	    document.getElementById('Par').disabled=true;
		document.getElementById('e1').disabled=true;
		document.getElementById('ceduE').disabled=true;
		document.getElementById('Est').disabled=true;
		document.getElementById('especif').disabled=true;
	    }
}
function h5()
{
	 if($('#especif').val() != "-")
	 {
	  	document.getElementById('Par').disabled=true;
		document.getElementById('e1').disabled=true;
		document.getElementById('ceduE').disabled=true;
		document.getElementById('Psec').disabled=true;
		document.getElementById('secciones').disabled=true;
		document.getElementById('Est').disabled=true;
	 }else{
	 	document.getElementById('Par').disabled=false;
		document.getElementById('e1').disabled=false;
		document.getElementById('ceduE').disabled=false;
		document.getElementById('Psec').disabled=false;
		document.getElementById('secciones').disabled=false;
		document.getElementById('Est').disabled=false;
	 }
}
//Docentes
function hD1()
{
	 if(document.getElementById('Doc').checked==true)
	 {
	  	document.getElementById('cedo').disabled=true;
		document.getElementById('ceduD').disabled=true;
		document.getElementById('Dsec').disabled=true;
	 }else{
	 	document.getElementById('cedo').disabled=false;
	 	document.getElementById('ceduD').disabled=false;
		document.getElementById('Dsec').disabled=false;
	 }
}
function hD2()
{
	 if(document.getElementById('cedo').checked==true||$('#ceduD').val() != "")
	 {
	  	document.getElementById('Doc').disabled=true;
		document.getElementById('Dsec').disabled=true;
	 }else{
	 	document.getElementById('Doc').disabled=false;
		document.getElementById('Dsec').disabled=false;
	 }
}
function hD3()
{
	 if(document.getElementById('Dsec').checked==true)
	 {
	  	document.getElementById('Doc').disabled=true;
		document.getElementById('cedo').disabled=true;
		document.getElementById('ceduD').disabled=true;
	 }else{
	 	document.getElementById('Doc').disabled=false;
		document.getElementById('cedo').disabled=false;
	 	document.getElementById('ceduD').disabled=false;
	 }
}
//Representantes
function hR1()
{
	 if(document.getElementById('Rep').checked==true)
	 {
	  	document.getElementById('eRep').disabled=true;
		document.getElementById('ceduR').disabled=true;
		document.getElementById('e6').disabled=true;
	 }else{
	 	document.getElementById('eRep').disabled=false;
		document.getElementById('ceduR').disabled=false;
	 	document.getElementById('e6').disabled=false;
	 }
}
function hR2()
{
	 if(document.getElementById('eRep').checked==true||$('#ceduR').val() != "")
	 {
	  	document.getElementById('Rep').disabled=true;
		document.getElementById('e6').disabled=true;
	 }else{
	 	document.getElementById('Rep').disabled=false;
		document.getElementById('e6').disabled=false;
	 }
}
function hR3()
{
	 if($('#e6').val() != "-")
	 {
	  	document.getElementById('eRep').disabled=true;
		document.getElementById('ceduR').disabled=true;
		document.getElementById('Rep').disabled=true;
	 }else{
	 	document.getElementById('eRep').disabled=false;
		document.getElementById('ceduR').disabled=false;
	 	document.getElementById('Rep').disabled=false;
	 }
}