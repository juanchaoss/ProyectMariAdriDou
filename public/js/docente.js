var url = "http://localhost:8000";

function registrarDocente(){
    var nac_d   = $("#nac_d").val();
    var cedula   = $("#cedu").val();
    var apellido = $("#ape").val();
    var nombre   = $("#nom").val();
    var nivel    = $("#nivel").val();
    var direccion = $("#direc").val();
    var telefono  = $("#tele").val();

    if ($("#cedu").val().length<7) {
        swal("","Debe introducir mínimo 7 dígitos","warning");
        return;
    }
    if($.trim(nac_d) == '' ||$.trim(cedula) == '' || $.trim(apellido) == '' || $.trim(nombre) == '' || $.trim(nivel) == '' || $.trim(direccion) == '' || $.trim(telefono) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
        $.ajax({
            type: "POST",
            url: url+"/guardarDocente",
            data:{
                _token:$("#_token").val(),
                nac_d:nac_d,
                cedula:cedula,
                apellido:apellido,
                nombre:nombre,
                nivel:nivel,
                direccion:direccion,
                telefono:telefono
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Se ha registrado un docente exitosamente","success");
                    cleanFormDo();
                    obtenerDocentes();
                }else{
                    swal("Error","No se pudo guardar el docente","error");
                    return;
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
}

function cleanFormDo(){
    $("#nac_d").val('');
    $("#cedu").val('');
    $("#ape").val('');
    $("#nom").val('');
    $("#nivel").val('');
    $("#direc").val('');
    $("#tele").val('');
}

function obtenerDocentes(){
    var registro = '';
    var nivel = "";
    var nacion = "";

    $.ajax({
        type: "POST",
        url: url+"/obtenerDocentes",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        success: function(data){
            $("#regisDo").html('');
            if(data){

                for(var i = 0, j = data.length; i < j; i++){
                    
                    var up  = "up-"+data[i]['id'];
                    var del = "de-"+data[i]['id'];
                    
                    if (data[i]['nac_d'] == 1){
                        nacion= "V";
                    }else{
                        nacion= "E";
                    }

                    if(data[i]['nivel'] == 1){
                        nivel = "Universitario";
                    }else if(data[i]['nivel'] == 2){
                        nivel = "TSU";
                    }else if(data[i]['nivel'] == 3){
                        nivel = "Bachiller";
                    }else{
                        nivel = "nada";
                    }
                    registro = "<tr><td>"+nacion+"</td><td>"+data[i]['cedula']+"</td><td>"+data[i]['apellido']+"</td><td>"+data[i]['nombre']+"</td>" + "<td>"+nivel+"</td>" +
                        "<td>"+data[i]['direccion']+"</td><td>"+data[i]['telefono']+"</td>" +
                        "<td><li id="+up+" class='fa fa-pencil-square-o fa-2x'></li>&nbsp;&nbsp;<li id="+del+" class='fa fa-trash-o fa-2x'></li></td></tr>";
                    $("#regisDo").append(registro);
                }
                $("#tblDocente").DataTable();
                cleanFormDo();
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarDocente(id){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de eliminar al docente?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarDocente",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            beforeSend: function(xhr, settings){
                //alert("Obteniendo datos");
            },
            success: function(data){
                if(data){
                    swal("Éxito","Docente eliminado exitosamente","success");
                    obtenerDocentes();
                    return;
                }else{
                    swal("Error","No se pudo eliminar el docente","error");
                }
            },
            error: function(error){
                swal("ERROR","Ha ocurrido un error","error");
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function obtenerDocenteEspecifico(id){
    $.ajax({
        type: "POST",
        url: url+"/obtenerDocenteEsp",
        data:{
            _token:$("#_token").val(),
            id:id
        },
        dataType: "json",
        beforeSend: function(xhr, settings){
            //alert("Obteniendo datos");
        },
        success: function(data){
            if(data){
                $("#nac_dUp").val(data['nac_d']);
                $("#ceduUp").val(data['cedula']);
                $("#apeUp").val(data['apellido']);
                $("#nomUp").val(data['nombre']);
                $("#niUp").val(data['nivel']);
                $("#diUp").val(data['direccion']);
                $("#teUp").val(data['telefono']);
                $("#id").val(id);
                return;
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarDocente(){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de actualizar el docente?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        var nac  = $("#nac_dUp").val();
        var cedu  = $("#ceduUp").val();
        var ape   = $("#apeUp").val();
        var nom   = $("#nomUp").val();
        var nivel = $("#niUp").val();
        var direc = $("#diUp").val();
        var tele  = $("#teUp").val();
        var id    = $("#id").val();

        if($.trim(nac) == '-' || $.trim(cedu) == '' || $.trim(ape) == '' || $.trim(nom) == '' || $.trim(nivel) == '' || $.trim(direc) == '' || $.trim(tele) == '') {
            swal("Error","Existen campos vacíos","error");
            return;
        }

        $.ajax({
            type: "POST",
            url: url+"/actualizarDocente",
            data:{
                _token:$("#_token").val(),
                nac_d:nac,
                cedula:cedu,
                apellido:ape,
                nombre:nom,
                nivel:nivel,
                direccion:direc,
                telefono:tele,
                id:id
            },
            dataType: "json",
            beforeSend: function(xhr, settings){
                //alert("Obteniendo datos");
            },
            success: function(data){
                if(data){
                    swal("Éxito","Docente actualizado exitosamente","success");
                    $('#exampleModal').modal('hide');
                    obtenerDocentes();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function validarLetras(e){  //funcion que valida solo letras
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor>=48 && valor<=57) 
        e.preventDefault()
    }
}

function validarNumeros(e){ //funcion que valida solo numeros
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor<48 || valor>57) 
        e.preventDefault()  
    }
}
function habilitarCedulaD()
{
 var nac=document.getElementById('nac_d').value; 
 if(nac=="-1")
 {
  document.getElementById('cedu').disabled=true;
  document.getElementById('cedu').value="";
 }
 else
    {
     document.getElementById('cedu').disabled=false;
    }
}
function tam_ced(){
    if ($("#cedu").val().length<7) {
        swal("","Debe introducir mínimo 7 dígitos","warning");
    }
}
