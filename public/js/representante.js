var url = "http://localhost:8000";

function registrarRepresentante(){

    var naciona   = $("#nacR").val();
    var cedula = $("#cedulaR").val();
    var apellido   = $("#apellidoR").val();
    var nombre    = $("#nombresR").val();
    var telefono = $("#telefonoR").val();
    var nivel  = $("#niveliR").val();
    var direcv  = $("#direcRD").val();
    var tipov  = $("#tipovivienda").val();
    var traba  = $("#trabaR").val();
    var direct  = $("#direcRT").val();

    if ($("#cedulaR").val().length<7) {
        swal("","Debe introducir mínimo 7 dígitos","warning");
        return;
    }
    if($.trim(naciona) == '' || $.trim(cedula) == '' || $.trim(apellido) == '' || $.trim(nombre) == '' || $.trim(telefono) == '' || $.trim(nivel) == '' || $.trim(direcv) == '' || $.trim(tipov) == '' || $.trim(traba) == '' || $.trim(direct) == ''){
        swal("Error","Existen campos vacíos","error");
        return;
    }else{
    $.ajax({
        type: "POST",
        url: url+"/guardarRepresentante",
        data:{
            _token:$("#_token").val(),
            nacionalidad:naciona,
            cedula:cedula,
            apellido:apellido,
            nombre:nombre,
            telefono:telefono,
            nivel:nivel,
            direcRD:direcv,
            tipovi:tipov,
            trabaja:traba,
            direcRT:direct
        },
        dataType: "json",
        success: function(data){
            if(data){
                swal("Éxito","Registro realizado exitosamente","success");
                obtenerRepresentantes();
                //obtenerRepresentantes();
            }else{
                swal("Error","No se pudo registrar el representante","error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
    }
}

function cleanFormRe(){
    $("#nacR").val('');
    $("#cedulaR").val('');
    $("#apellidoR").val('');
    $("#nombresR").val('');
    $("#telefonoR").val('');
    $("#niveliR").val('');
    $("#direcRD").val('');
    $("#tipovivienda").val('');
    $("#trabaR").val('');
    $("#direcRT").val('');
}

function obtenerRepresentantes(){

    $.ajax({
        type: "POST",
        url: url+"/obtenerRepresentantes",
        data:{
            _token:$("#_token").val()
        },
        dataType: "json",
        beforeSend: function(xhr, settings){
            //alert("Obteniendo datos");
        },
        success: function(data){
            var registro;
            $("#regisRe").html('');
            if(data){

                for(var i = 0, j = data.length; i < j; i++){

                    var up = "upR-"+data[i]['id'];
                    var de = "delR-"+data[i]['id'];
                    
                    if(data[i]['nacR'] == 1){
                        nac = "V";
                    }else{
                        nac = "E";
                    }

                    if(data[i]['nivel'] == 1){
                        ins = "Ninguno";
                    }else if(data[i]['nivel'] == 2){
                        ins = "Primaria";
                    }else if(data[i]['nivel'] == 3){
                        ins = "Bachillerato";
                    }else if(data[i]['nivel'] == 4){
                        ins = "TSU";
                    }else if(data[i]['nivel'] == 5){
                        ins = "Univeritario";    
                    }else{
                        ins = "Postgrado";
                    }

                    if(data[i]['tipovi'] == 1){
                        viv = "Rancho";
                    }else if(data[i]['tipovi'] == 2){
                        viv = "Apartamentos en quintas o casa";
                    }else if(data[i]['tipovi'] == 3){
                        viv = "Apartamentos en edificios";
                    }else if(data[i]['tipovi'] == 4){
                        viv = "Casas";   
                    }else{
                        viv = "Quintas";
                    }

                    if(data[i]['trabaja'] == 1){
                        traba = "Si";
                    }else{
                        traba = "No";
                    }


                    registro = "<tr><td>"+data[i]['cedula']+"</td><td>"+data[i]['apellido']+"</td><td>"+data[i]['nombre']+"</td><td>"+data[i]['telefono']+"</td><td>"+ins+"</td><td>"+data[i]['direcRD']+"</td><td>"+viv+"</td>" +
                        "<td>"+traba+"</td> <td><label act="+up+" class='fa fa-pencil-square-o fa-2x'></label>&nbsp;&nbsp;<label act="+de+" class='fa fa-trash-o fa-2x'></label></td></tr>";
                    $("#regisRe").append(registro);
                }
                cleanFormRe();

                $('#tblRepresentante').DataTable();
                /*$('#tblRepresentante').DataTable();*/
            }else{
                console.log("Error");
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function eliminarRepresentante(id){

    swal({
        title: 'Alerta',
        text: "¿Está seguro de eliminar a este representante?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'No, cancelar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url+"/eliminarRepresentante",
            data:{
                _token:$("#_token").val(),
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Representante eliminado exitosamente","success");
                    obtenerRepresentantes();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            );
        }
    });
}

function obtenerRepresentanteEspecifico(id,cedula,nac){

    if(nac === "-"){
        swal("Error","Debes elegir la nacionalidad","warning");
        return false;
    }

    $.ajax({
        type: "POST",
        url: url+"/obtenerRepresentanteEsp",
        data:{
            _token:$("#_token").val(),
            id:id,
            cedula:cedula,
            nac:nac
        },
        dataType: "json",
        success: function(data){
            $("#nomR").text('');
            $("#apeR").text('');
            $("#direR").text('');
            $("#idRe").val('');

            if(data){
                if(cedula === null){
                    $("#nacRe").val(data['nacionalidad']);
                    $("#cedulaRe").val(data['cedula']);
                    $("#apellidoRe").val(data['apellido']);
                    $("#nombresRe").val(data['nombre']);
                    $("#telefonoRe").val(data['telefono']);
                    $("#niveliRe").val(data['nivel']);
                    $("#direcRDe").val(data['direcRD']);
                    $("#tipoviviendae").val(data['tipovi']);
                    $("#trabaRe").val(data['trabaja']);
                    $("#direcRTe").val(data['direcRT']);
                    $("#idR").val(data['id']);
                }else{

                    $("#nomR").text(data['nombre']);
                    $("#apeR").text(data['apellido']);
                    $("#direR").text(data['direcRD']);
                    $("#idRe").val(data['id']);
                }
                return;
            }else{
                swal("Error","El representante no existe","warning");
                return;
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}

function actualizarRepresentante(){

    swal({
        title: 'Alerta!',
        text: "¿Está seguro de actualizar el representante?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function (){

        var nacR      = $("#nacRe").val();
        var cedulaR   = $("#cedulaRe").val();
        var apellidoR = $("#apellidoRe").val();
        var nombresR  = $("#nombresRe").val();
        var telefonoR = $("#telefonoRe").val();
        var niveliR   = $("#niveliRe").val();
        var direcRD   = $("#direcRDe").val();
        var tipovivienda  = $("#tipoviviendae").val();
        var traba    = $("#trabaRe").val();
        var direcRT  = $("#direcRTe").val();
        var id       = $("#idR").val();

        $.ajax({
            type: "POST",
            url: url+"/actualizarRepresentante",
            data:{
                _token:$("#_token").val(),
                nacionalidad:nacR,
                cedula:cedulaR,
                apellido:apellidoR,
                nombre:nombresR,
                telefono:telefonoR,
                nivel:niveliR,
                direcRD:direcRD,
                tipovi:tipovivienda,
                trabaja:traba,
                direcRT:direcRT,
                id:id
            },
            dataType: "json",
            success: function(data){
                if(data){
                    swal("Éxito","Representante actualizado exitosamente","success");
                    $('#modalR').modal('hide');
                    obtenerRepresentantes();
                    return;
                }else{
                    console.log("Error");
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelado',
                'Operación cancelada por el usuario',
                'error'
            )
        }
    });
}

function validarLetras(e){  //funcion que valida solo letras
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor>=48 && valor<=57) 
        e.preventDefault()
    }
}

function validarNumeros(e){ //funcion que valida solo numeros
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor<48 || valor>57) 
        e.preventDefault()  
    }
}

function validarTelefonos(e){ //funcion que valida solo numeros
    var valor = e.keyCode | e.which
    if(valor!=9 && valor!=8)
    {
     if(valor<48 || valor>57 && valor!=43) 
        e.preventDefault()  
    }
}

function habilitarCedula()
{
 var cedula=document.getElementById('nacR').value; 
 if(cedula=="-1")
 {
  document.getElementById('cedulaR').disabled=true;
  document.getElementById('cedulaR').value="";
 }
 else
    {
     document.getElementById('cedulaR').disabled=false;
    }
}

function habilitarTrabajo()
{
 var trabajo=document.getElementById('trabaR').value; 
 var nada="N/A";
 if(trabajo=='2')
 {
  document.getElementById('direcRT').disabled=true;
  document.getElementById('direcRT').value=nada;
 }
 else
    {
     document.getElementById('direcRT').disabled=false;
    }
}
function tama_ced(){
    if ($("#cedulaR").val().length<7) {
        swal("","Debe introducir mínimo 7 dígitos","warning");
    }
}