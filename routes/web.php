<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TEST-ROUTES
Route::get('/estuSecc','ParticipaController@getPartici');
Route::get('/testEs','EstudianteController@getAllStudienWithAll');
Route::get('/estuSec/{seccion_id}','EstudianteController@estuSec');
Route::get('/canSavePar/{actividad_id}','ParticipaController@canSaveParti');
Route::get('/getInfParti','ParticipaController@getInfParti');
Route::get('/getParEsp','ParticipaController@getInfPartiEsp');
Route::get('/deleteParti','ParticipaController@deleteInfParti');
Route::get('/infEst/{idSecc}','EstudianteController@studenWithSec');
Route::get('/getParTodos','ParticipaController@getParTodos');
Route::get('/xxx/{idSecc}','ReportPDFController@caseMasSecc');
//historial

Route::get('/historyList','SeccionController@historySeccion');
Route::get('/getSection/{periodo_id}','SeccionController@getSecciones')->middleware('CanDoGo');
//TEST-ROUTES

/*Route::get('testU','EstudianteController@testEstu');

Route::get('test/{id}','EstudianteController@functionPrueba');*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/docentes', function () {
    return view('docente');
})->middleware('CanDoGo');

Route::get('/matriculacion', function () {
    return view('matriculacion');
})->middleware('CanDoGo');

Route::get('/estudiantes', function () {
    return view('estudiante');
})->middleware('CanDoGo');

Route::get('/seccione', function () {
    return view('seccione');
})->middleware('CanDoGo');

Route::get('/representantes', function () {
    return view('representante');
})->middleware('CanDoGo');

Route::get('/periodo', function () {
    return view('periodo');
})->middleware('CanDoGo');

Route::get('/actividad', function () {
    return view('actividad');
})->middleware('CanDoGo');

Route::get('/participa', function () {
    return view('participa');
})->middleware('CanDoGo');

Route::get('/reporGra',function(){
    return view('reportGra');
})->middleware('CanDoGo');

Route::get('/reporPDF',function(){
    return view('reportPDF');
})->middleware('CanDoGo');

Route::get('/usuarios',function(){
    return view('users');
})->middleware('CanDoGo');

Route::get('/history', function () {
    return view('history');
})->middleware('CanDoGo');

/*Route::get('participa',['uses'=>'ParticipaController@participa']);
Route::post('participa',['uses'=>'ParticipaController@postParticipa','as'=>'postParticipa']);*/

//USERS
Route::post('/saveUsers','UserController@createUser')->middleware('CanDoGo');
Route::get('/getAllUsers','UserController@getAllUsers')->middleware('CanDoGo');
Route::get('/getUserSpec','UserController@getUserEsp')->middleware('CanDoGo');
Route::post('/updateUser','UserController@updateUser')->middleware('CanDoGo');
Route::post('/deleteUser','UserController@deleteUser')->middleware('CanDoGo');
//FIN USERS
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/getPartici','ParticipaController@getPartici')->middleware('CanDoGo');

//Docentes
Route::post('/guardarDocente','DocenteController@registrarDocente')->middleware('CanDoGo');
Route::post('/obtenerDocentes','DocenteController@traerTodosLosDocentes')->middleware('CanDoGo');
Route::post('/eliminarDocente','DocenteController@eliminarDocente')->middleware('CanDoGo');
Route::post('/obtenerDocenteEsp','DocenteController@traerUsuarioEspecifico')->middleware('CanDoGo');
Route::post('/actualizarDocente','DocenteController@actualizarDatosDocente')->middleware('CanDoGo');

//Estudiantes
Route::post('/guardarEstudiante','EstudianteController@registrarEstudiante')->middleware('CanDoGo');
Route::post('/obtenerEstudiantes','EstudianteController@traerTodosLosEstudiantes')->middleware('CanDoGo');
Route::post('/eliminarEstudiante','EstudianteController@eliminarEstudiante')->middleware('CanDoGo');
Route::post('/obtenerEstudianteEsp','EstudianteController@traerEstudianteEspecifico')->middleware('CanDoGo');
Route::post('/actualizarEstudiante','EstudianteController@actualizarDatosEstudiante')->middleware('CanDoGo');
Route::get('/estuSec/{seccion_id}','EstudianteController@estuSec');

//enfermedad y habitos
Route::post('/registrarEnfermedad','EstudianteController@registrarEnfermedad')->middleware('CanDoGo');
Route::post('/registrarHabito','EstudianteController@registrarHabito')->middleware('CanDoGo');

//Secciones
Route::post('/guardarSeccione','SeccionController@registrarSeccione')->middleware('CanDoGo');
Route::post('/obtenerSeccione','SeccionController@traerTodasLasSeccione')->middleware('CanDoGo');
Route::post('/eliminarSeccione','SeccionController@eliminarSeccione')->middleware('CanDoGo');
Route::post('/obtenerSeccioneEsp','SeccionController@traerSeccioneEspecifico')->middleware('CanDoGo');
Route::post('/actualizaSeccione','SeccionController@actualizarDatosSeccione')->middleware('CanDoGo');

//Representantes
Route::post('/guardarRepresentante','RepresentanteController@registrarRepresentante')->middleware('CanDoGo');
Route::post('/obtenerRepresentantes','RepresentanteController@traerTodosLosRepresentantes')->middleware('CanDoGo');
Route::post('/eliminarRepresentante','RepresentanteController@eliminarRepresentante')->middleware('CanDoGo');
Route::post('obtenerRepresentanteEsp','RepresentanteController@traerRepresentante')->middleware('CanDoGo');
Route::post('/actualizarRepresentante','RepresentanteController@actualizarDatosRepresentante')->middleware('CanDoGo');

//Periodos
Route::post('/guardarPeriodo','PeriodoController@registrarPeriodo')->middleware('CanDoGo');
Route::post('/obtenerPeriodo','PeriodoController@traerTodosLosPeriodo')->middleware('CanDoGo');
Route::post('/eliminarPeriodo','PeriodoController@eliminarPeriodo')->middleware('CanDoGo');
Route::post('/obtenerPeriodoEsp','PeriodoController@obtenerPeriodoEspecifico')->middleware('CanDoGo');
Route::post('/actualizarPeriodo','PeriodoController@actualizarDatosPeriodo')->middleware('CanDoGo');
Route::get('/getPeriodActive','PeriodoController@getPeriodoActivo')->middleware('CanDoGo');
Route::get('/getPeriodTeacherSeccion','PeriodoController@getPeriodoDocenteSecciones')->middleware('CanDoGo');
Route::post('/saveAttach','PeriodoController@saveAttachDocenteSeccionPeriodo')->middleware('CanDoGo');

//Actividad
Route::post('/guardarActividad','ActividadController@registrarActividad')->middleware('CanDoGo');
Route::post('/obtenerActividad','ActividadController@traerTodosLosActividad')->middleware('CanDoGo');
Route::post('/eliminarActividad','ActividadController@eliminarActividad')->middleware('CanDoGo');
Route::post('/obtenerActividadEsp','ActividadController@obtenerActividadEspecifico')->middleware('CanDoGo');
Route::post('/actualizarActividad','ActividadController@actualizarDatosActividad')->middleware('CanDoGo');
Route::get('/getActivities','ActividadController@getActivities')->middleware('CanDoGo');

//Participa
//Route::post('/select','ParticipaController@select')->middleware('CanDoGo');
Route::post('/obtenerParticipa','ParticipaController@traerTodosLosParticipa')->middleware('CanDoGo');
Route::post('/eliminarParticipa','ParticipaController@eliminarParticipa')->middleware('CanDoGo');
Route::post('/obtenerParticipaEsp','ParticipaController@traerParticipa')->middleware('CanDoGo');
Route::post('/actualizarParticipa','ParticipaController@actualizarDatosParticipa')->middleware('CanDoGo');
Route::get('/docsec/{seccion_id}','DocenteController@docSec');
Route::get('/getInfParti','ParticipaController@getInfParti');
//Route::post('/select','ParticipaController@select')->middleware('CanDoGo');

//cosa nueva
Route::get('/validEmail','ComunesController@validEmail');

Route::get('/verFecha','EstudianteController@verFecha');

//Participa
Route::post('/saveParticipa','ParticipaController@saveParicipacion')->middleware('CanDoGo');

//Matriculacion
Route::get('/getStudiantesM','MatriculacionController@getAllStudent');
Route::get('/cambiarStatus','EstudianteController@cambiarStatus');
Route::get('/obtenerSeccione','SeccionController@traerTodasLasSeccione')->middleware('CanDoGo');
Route::get('/obtenerEstu','EstudianteController@traerEstudianteMat')->middleware('CanDoGo');
Route::get('/getPeriodoAct','PeriodoController@getPeriodoActivo')->middleware('CanDoGo');
Route::post('/saveInscription','MatriculacionController@saveInscription')->middleware('CanDoGo');

//REPORT GRAPH
Route::get('reportGraphEd','EstudianteController@reportGraphEdades')->middleware('CanDoGo');
Route::get('reportGraphViviendas','RepresentanteController@reportGraphViviendas')->middleware('CanDoGo');
Route::get('reportGraphDiscapacidadf','EstudianteController@reportGraphDiscapacidadf')->middleware('CanDoGo');
Route::get('reportGraphGB','EstudianteController@reportGraphCountGirlsAndBoy')->middleware('CanDoGo');
Route::get('reportGraphCountSEA','EstudianteController@reportGraphCountSEA')->middleware('CanDoGo');
Route::get('reportGraphTrabaja','RepresentanteController@reportGraphTrabaja')->middleware('CanDoGo');
Route::get('reportGraphSoloBano','EstudianteController@reportGraphSoloBano')->middleware('CanDoGo');
Route::get('reportGraphRetraso','EstudianteController@reportGraphRetraso')->middleware('CanDoGo');
Route::get('reportGraphRegimenAlimenticio','EstudianteController@reportGraphRegimenAlimenticio')->middleware('CanDoGo');
Route::get('reportGraphParticipa','ParticipaController@reportGraphParticipa')->middleware('CanDoGo');

//ROUTES TO REPORT PDF
Route::get('reportPdf','ReportPDFController@validReportPDF')->middleware('CanDoGo');
Route::get('reportPdfe','ReportPDFController@caseSpece')->middleware('CanDoGo');
Route::get('reportPdfd','ReportPDFController@caseSpeceD')->middleware('CanDoGo');
Route::get('reportPdfS','ReportPDFController@caseSex')->middleware('CanDoGo');
Route::get('reportPdfSe','ReportPDFController@caseSec')->middleware('CanDoGo');
Route::get('reportPdfr','ReportPDFController@caseSpeceR')->middleware('CanDoGo');
Route::get('reportMasSecc/{idSecc}','ReportPDFController@caseMasSecc')->middleware('CanDoGo');
Route::get('reportFemSecc/{idSecc}','ReportPDFController@caseFemSecc')->middleware('CanDoGo');
Route::get('reportSoloSec/{idSecc}','ReportPDFController@caseSoloSec')->middleware('CanDoGo');
Route::get('reportSex','ReportPDFController@caseStuSex')->middleware('CanDoGo');