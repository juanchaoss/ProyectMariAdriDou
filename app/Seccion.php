<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Seccion
 *
 * @property int $id
 * @property string $idsecc
 * @property int $turnosecc
 * @property int $edaddesde
 * @property int $edadhasta
 * @property string $obser
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $periodo_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Docente[] $docentes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Estudiante[] $estudiantes
 * @property-read \App\Periodo $periodos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereEdaddesde($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereEdadhasta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereIdsecc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereObser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion wherePeriodoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereTurnosecc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Seccion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Seccion extends Model
{
    protected $table = "secciones";
    protected $fillable = ['idsecc','turnosecc','edaddesde','edadhasta','obser','periodo_id'];
    protected $dates = ['deleted_at'];

    public function estudiantes(){
        return $this->belongsToMany('App\Estudiante','estu_seccs');
    }

    public function docentes(){
        return $this->belongsToMany('App\Docente','secciones_docentes');
    }

    public function periodos(){
        return $this->hasOne('App\Periodo');
    }
}