<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Inf_Enfermedad
 *
 * @property int $id
 * @property string $enfermedad
 * @property string $medicamentoF
 * @property string $condiFisica
 * @property string $retraso
 * @property string $alergia
 * @property string $regimen
 * @property int $sea
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $estudiante_id
 * @property-read \App\Estudiante $estudiantes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereAlergia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereCondiFisica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereEnfermedad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereEstudianteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereMedicamentoF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereRegimen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereRetraso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereSea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Inf_Enfermedad whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Inf_Enfermedad extends Model
{
    protected $table = "inf__enfermedads";
    protected $fillable = ['enfermedad','medicamentoF','condiFisica','retraso','alergia','regimen','sea','estudiante_id'];

    public function estudiante(){
        return $this->belongsTo('App\Estudiante','estudiante_id');
    }
}
