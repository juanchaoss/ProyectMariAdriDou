<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Habito
 *
 * @property int $id
 * @property int $estudiante_id
 * @property int $come
 * @property int $desayuna
 * @property int $apetito
 * @property int $suenio
 * @property string $dormir
 * @property int $orina
 * @property int $panales
 * @property int $banio_so
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Estudiante $estudiantes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereApetito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereBanioSo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereCome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereDesayuna($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereDormir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereEstudianteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereOrina($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito wherePanales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereSuenio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Habito whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Habito extends Model
{
    protected $table = "habitos";
    protected $fillable = ['estudiante_id','come','desayuna','apetito','suenio','dormir','orina','panales','banio_so'];

    public function estudiantes(){
        return $this->belongsTo('App\Estudiante','estudiante_id');
    }
}
