<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Estu_secc
 *
 * @property int $id
 * @property int|null $estudiante_id
 * @property int|null $seccion_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $periodo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc whereEstudianteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc wherePeriodo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc whereSeccionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estu_secc whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Estu_secc extends Model
{
    protected $fillable = ['estudiante_id','seccion_id','periodo'];
}
