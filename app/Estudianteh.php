<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Estudianteh
 *
 * @mixin \Eloquent
 */
class Estudianteh extends Model
{
    protected $table = "estudiantesh";
    protected $fillable = ['come','desayuna','apetito','sueno','hora','orina','panales','bano'];
}
