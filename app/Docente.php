<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Docente
 *
 * @property int $id
 * @property int $nac_d
 * @property string $cedula
 * @property string $apellido
 * @property string $nombre
 * @property int $nivel
 * @property string $direccion
 * @property string $telefono
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Participa[] $participacion
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Seccion[] $secciones
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Docente onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereApellido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereCedula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereNacD($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereNivel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Docente whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Docente withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Docente withoutTrashed()
 * @mixin \Eloquent
 */
class Docente extends Model
{
    use SoftDeletes;

    protected $table = "docentes";
    protected $fillable = ['nac_d','cedula','apellido','nombre','nivel','direccion','telefono'];
    protected $dates = ['deleted_at'];

    public function secciones(){
        return $this->belongsToMany('App\Seccion','secciones_docentes');
    }

    public function participacion(){
        return $this->belongsToMany('App\Participa','participa');
    }
}
