<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periodo;
use App\Http\Helpers\Helpers;
use App\Docente;
use App\Seccion;
use App\SeccionDocente;

class PeriodoController extends Controller
{
    public function registrarPeriodo(Request $request){

        if(!$this->areValidYear($request->input('fechaI'),$request->input('fechaF'))){
            return response()->json(3);
        }

        if(count($this->checkStatusPe()) == 0){
            $fechaI = Helpers::formatDate($request->input('fechaI'),'AAAA-MM-DD');
            $fechaF = Helpers::formatDate($request->input('fechaF'),'AAAA-MM-DD');

            $data = [
                'fechaI' => $fechaI." ".date('H:i:s'),
                'fechaF' => $fechaF." ".date('H:i:s'),
                'status' => 1,
                'descriP' => $request->input('descriP')
            ];
            return Periodo::create($data);
        }
        return response()->json(2);
    }

    public function areValidYear($year1,$year2){

        $yInit = explode('/',$year1);
        $yEnd  = explode('/',$year2);

        if($yInit[2] == $yEnd[2]){
            return false;
        }else if($yEnd[2] < $yInit[2]){
            return false;
        }
        return true;
    }

    public function traerTodosLosPeriodo(){
        return Periodo::all();
    }

    public function checkStatusPe(){
        return Periodo::where('status',1)->first();
    }

    public function obtenerPeriodoEspecifico(Request $request){
        return Periodo::where('id',$request->input('id'))->first();
    }

    public function actualizarDatosPeriodo(Request $request){

        $data = ['fechaI'   => $request->input('fechaI'),
                 'fechaF'   => $request->input('fechaF'),
                 'descriP'  => $request->input('descriP'),
                 'status'   => $request->input('status')
                ];

        return Periodo::where('id',$request->input('id'))->update($data);
    }

    public function eliminarPeriodo(Request $request){
        return Periodo::where('id',$request->input('id'))->delete();
    }

    public function getPeriodoActivo($format=''){
        if($format == ''){
            return response()->json(Periodo::where('status',1)->first());
        }else{
            return Periodo::where('status',1)->first();
        }
    }

    public function saveAttachDocenteSeccionPeriodo(Request $request){

        if($this->isPossibleAttach($request->input('seccion_id'),$request->input('docente_id'),$request->input('periodo')))
            return response()->json(SeccionDocente::create($request->all()));
        else
            return response()->json(false);
    }

    public function getPeriodoDocenteSecciones(){
        $docentes  = Docente::all();
        $periodos  = Periodo::all();
        $periodo   = $this->getPeriodoActivo('function');
        if(!isset($periodo->id)){
            return response()->json(null);
        }else{
            $secciones = Seccion::where('periodo_id',$periodo->id)->get();
            $data = ['docente'=>$docentes,'periodo'=>$periodo,'secciones'=>$secciones,'periodos'=>$periodos];
            return response()->json($data);
        }
    }

    public function isPossibleAttach($idSeccion,$idDocente,$periodo){
        $attach = SeccionDocente::where('seccion_id',$idSeccion)->where('docente_id',$idDocente)
                 ->where('periodo',trim($periodo))->first();
        if($attach == null){
            $secBusy = SeccionDocente::where('seccion_id',$idSeccion)->where('periodo',$periodo)->first();
            if($secBusy == null){
                return true;
            }else
                return false;
        }else
            return false;
    }
}
