<?php

namespace App\Http\Controllers;

use App\Estudiante;
use Illuminate\Http\Request;
use App\Representante;
use DB;

class RepresentanteController extends Controller
{
    public function registrarRepresentante(Request $request){
        if(count($this->checkExisRepre($request->input('cedula'))) == 0){
            $data = ['nacionalidad' => $request->input('nacionalidad'),
                    'cedula'       => $request->input('cedula'),
                    'apellido'     => ucwords($request->input('apellido')),
                    'nombre'       => ucwords($request->input('nombre')),
                    'telefono'     => $request->input('telefono'),
                    'nivel'        => $request->input('nivel'),
                    'direcRD'      => $request->input('direcRD'),
                    'tipovi'       => $request->input('tipovi'),
                    'trabaja'      => $request->input('trabaja'),
                    'direcRT'      => $request->input('direcRT')];
            return Representante::create($data);
        }
        return response()->json(false);
    }

    public function checkExisRepre($cedula){
        return Representante::where('cedula',$cedula)->get();
    }

    public function traerTodosLosRepresentantes(){
        return Representante::all();
    }

    public function traerRepresentante(Request $request){
        $output = Representante::where('id',$request->input('id'))
            ->orWhere('cedula',$request->input('cedula'))
            ->where('nacionalidad',$request->input('nac'))->first();
        if($output == null)
            return response()->json(false);
        else
            return response()->json($output);

    }

    public function checkExistRepresentante($cedula){
        return Estudiante::where('cedula',$cedula)->first();
    }

    public function actualizarDatosRepresentante(Request $request){

        if($this->checkExistRepresentante($request->input('cedula') != null)){
            return reponse()->json(false);
        }

        $data = ['nacionalidad' => $request->input('nacionalidad'),
                 'cedula'       => $request->input('cedula'),
                 'apellido'     => ucwords($request->input('apellido')),
                 'nombre'       => ucwords($request->input('nombre')),
                 'telefono'     => $request->input('telefono'),
                 'nivel'        => $request->input('nivel'),
                 'direcRD'      => $request->input('direcRD'),
                 'tipovi'       => $request->input('tipovi'),
                 'trabaja'      => $request->input('trabaja'),
                 'direcRT'      => $request->input('direcRT')];

        return Representante::where('id',$request->input('id'))->update($data);
    }

    public function eliminarRepresentante(Request $request){
        return Representante::where('id',$request->input('id'))->delete();
    }

    //REPORT GRAPH
    public function reportGraphTrabaja(){
        $si = DB::select("SELECT COUNT(*) AS SI FROM representantes WHERE trabaja = 1");
        $no = DB::select("SELECT COUNT(*) AS NO FROM representantes WHERE trabaja = 2");
        $data = ['si'=>$si[0]->SI,'no'=>$no[0]->NO];
        return response()->json($data);
    }
    public function reportGraphViviendas () {
        $rancho = Representante::where('tipovi',1)->count();
        $apaquin = Representante::where('tipovi',2)->count();
        $apaedi = Representante::where('tipovi',3)->count();
        $casas = Representante::where('tipovi',4)->count();
        $quintas = Representante::where('tipovi',5)->count();
        $data = ['rancho' => $rancho, 'apaquin' => $apaquin, 'apaedi' => $apaedi, 'casas' => $casas, 'quintas' => $quintas];
        return response()->json($data);
    }
}