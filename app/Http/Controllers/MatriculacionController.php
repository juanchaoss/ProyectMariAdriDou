<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante;
use App\Estu_secc;
use Illuminate\Database\Eloquent\Builder;

class MatriculacionController extends Controller
{
    public function getAllStudent(){
        $xx = Estudiante::with('seccion')->get();
        return response()->json($xx);
    }

    public function saveInscription(Request $request){
        return response()->json(Estu_secc::create($request->all()));
    }
}