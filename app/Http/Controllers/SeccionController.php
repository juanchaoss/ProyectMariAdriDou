<?php

namespace App\Http\Controllers;

use App\Docente;
use App\Estu_secc;
use App\Estudiante;
use App\SeccionDocente;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Seccion;
use App\Periodo;

class SeccionController extends Controller
{
    public function registrarSeccione(Request $request){
        return Seccion::create($request->all());
    }

    public function traerTodasLasSeccione(){
        return Seccion::all();
    }

    public function traerSeccioneEspecifico(Request $request){
        return Seccion::where('id',$request->input('id'))->first();
    }

    public function actualizarDatosSeccione(Request $request){

        $data = ['idsecc'    => $request->input('idsecc'),
                 'turnosecc' => $request->input('turnosecc'),
                 'edaddesde' => $request->input('edaddesde'),
                 'edadhasta' => $request->input('edadhasta'),
                 'obser'=> $request->input('obser')];

        return Seccion::where('id',$request->input('id'))->update($data);
    }

    public function eliminarSeccione(Request $request){
        return Seccion::where('id',$request->input('id'))->delete();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     */
    public function historySeccion(Request $request){
        $infSecc = Seccion::whereId($request->input('idSeccion'))->first();

        if($infSecc != null){
            $listEst = Estu_secc::whereSeccionId($request->input('idSeccion'))->where('periodo',$request->input('periodo'))->get();
            $countListEst = count($listEst);
            if($countListEst >= 1){
                $estudiantes = [];

                for($i = 0; $i < $countListEst; $i++){
                    $estu = Estudiante::whereId($listEst[$i]->estudiante_id)->first();
                    $estudiantes[] = $estu;
                }

                $docen = SeccionDocente::whereSeccionId($request->input('idSeccion'))->first();
                $docente = Docente::whereId($docen->docente_id)->first();
                $data = ['estudiantes'=>$estudiantes,'seccion'=>$infSecc,'docente'=>$docente,'periodo'=>$request->input('periodo')];
                return response()->json($data);
            }else
                return response()->json(false);
        }else
            return response()->json(false);
    }

    public function getSecciones($periodo_id){
        $secciones = Seccion::where('periodo_id',$periodo_id)->get();
        $data = ['secciones'=>$secciones];
        return response()->json($data);
    }
}