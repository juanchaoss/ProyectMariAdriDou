<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class ComunesController extends Controller
{
    public function validEmail(Request $request){
        return DB::table('users')->select('email')->where('email','LIKE','%'.$request->input('email')."%")
            ->get();
    }

    public static function checkUserExist($email){
        return User::where('email',$email)->firstOrFail();
    }
}
