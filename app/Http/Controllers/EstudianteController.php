<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante;
use DB;
use App\Inf_Enfermedad;
use App\Habito;
use App\Seccion;
use App\Estu_secc;
use App\Http\Helpers\Helpers;

class EstudianteController extends Controller
{

    public function checkExistStudent($cedulaEs){
        return Estudiante::where('cedula',$cedulaEs)->first();
    }

    public function getCountStudentByRepre($id){
        $count = Estudiante::where('representante_id',$id)->count();
        if(intval($count)+1 == $count){
            return intval($count)+2;
        }
        else{
            return intval($count)+1;
        }

    }

    public function estuSec($seccion_id){
        $estu = Estu_secc::whereSeccionId($seccion_id)->get();
        $countEst = count($estu);
        if($estu != null || $countEst >= 1){
            $estudiantes = [];
            $dataSecc = Seccion::whereId($seccion_id)->first();
            for($i = 0; $i < $countEst; $i++){
                $xx = Estudiante::whereId($estu[$i]->estudiante_id)->first();
                $estudiantes[] = $xx;
            }
            return response()->json(['seccion'=>$dataSecc,'estudiantes'=>$estudiantes]);
        }else return response()->json(false);

    }

    public function registrarEstudiante(Request $request){
        $idAux = $request->input('idEstu');
        if($idAux != null || !empty($idAux)){
            $edad = $this->calcularEdad($request->input('fechaNa'));
            $data = ['representante_id' => $request->input('representante_id'),
                'edad'      => $edad,
                'nombres'   => ucwords ($request->input('nombres')),
                'apellidos' => ucwords ($request->input('apellidos')),
                'fechaNa'   => Helpers::formatDate($request->input('fechaNa'),'AAAA-MM-DD'),
                'nacionalidad' => $request->input('nacionalidad'),
                'sexo'      => $request->input('sexo')];
            $estu = Estudiante::whereId($request->input('idEstu'))->first();
            $estu->update($data);
            return response()->json($estu);
        }else{
            $cedulaR = DB::table('representantes')->select('cedula')->where('id',$request->input('representante_id'))->get();
            $cedulaE = $cedulaR[0]->cedula.'-'.$this->getCountStudentByRepre($request->input('representante_id'));

            if($this->checkExistStudent($cedulaE) != null){
                return response()->json('false');
            }

            $edad = $this->calcularEdad($request->input('fechaNa'));

            $data = ['representante_id' => $request->input('representante_id'),
                'cedula'    => $cedulaE,
                'edad'      => $edad,
                'nombres'   => ucwords($request->input('nombres')),
                'apellidos' => ucwords($request->input('apellidos')),
                //'plantel'   => $request->input('plantel'),
                //'plantelE'  => $request->input('plantelE'),
                'fechaNa'   => Helpers::formatDate($request->input('fechaNa'),'AAAA-MM-DD'),
                'nacionalidad' => $request->input('nacionalidad'),
                'sexo'      => $request->input('sexo')];


            return response()->json(Estudiante::create($data));
        }
    }

    public function registrarEnfermedad(Request $request){
        if(Inf_Enfermedad::whereEstudianteId($request->input('estudiante_id'))->exists()){
            $input = $request->all();
            return response()->json(Inf_Enfermedad::whereEstudianteId($request->input('estudiante_id'))->update($input));
        }else{
            return response()->json(Inf_Enfermedad::create($request->all()));
        }
    }

    public function registrarHabito(Request $request){
        if(Habito::whereEstudianteId($request->input('estudiante_id'))->exists()){
            $input = $request->all();
            return response()->json(Habito::whereEstudianteId($request->input('estudiante_id'))->update($input));
        }else{
            return response()->json(Habito::create($request->all()));
        }
    }

    public function calcularEdad($fecha){

        $date = Helpers::formatDate($fecha,'AAAA-MM-DD');

        $anio = explode('-',$date);
        $edad=intval(date('Y')) - intval($anio[0]);

        if (date('m') > intval($anio[1]))
        {
            return $edad;
        }
        if (date('m') == intval($anio[1]))
        {
            if (date('d') <= intval($anio[2]))
            {
                return $edad;
            }
        }
        else
        {
            return $edad - 1;
        } 
    }

    public function traerTodosLosEstudiantes(){
        return response()->json(Estudiante::with(['habito','enfermedad'])->get());
    }

    public function traerEstudianteEspecifico(Request $request){
        return response()->json($this->getInfStudent($request));
    }

    public function traerEstudianteMat(Request $request){
        return Estudiante::where('id',$request->input('id'))->first();
        return response()->json($this->getInfStudent($request));
    }

    public function actualizarDatosEstudiante(Request $request){
        DB::beginTransaction();
        //dd($request->input('enfermedad'),$request->input('habito'));
        $data = ['nombres'    => ucwords($request->input('estudiante.nombres')),
                 'apellidos'   => ucwords($request->input('estudiante.apellidos')),
                 'fechaNa'=> $request->input('estudiante.fechaNa'),
                 'nacionalidad' => $request->input('estudiante.nacionalidad'),
                 'sexo' => $request->input('estudiante.sexo')];
        $xx = [
           'enfermedad'   => $request->input('enfermedad.enfermedad'),
           'medicamentoF' => $request->input('enfermedad.fiebre'),
           'condiFisica'  => $request->input('enfermedad.condiFi'),
           'retraso'      => $request->input('enfermedad.retraso'),
           'alergia'      => $request->input('enfermedad.alergia'),
           'regimen'      => $request->input('enfermedad.regimen'),
           'sea'          => $request->input('enfermedad.sae'),
            'estudiante_id'=>$request->input('id')
        ];

        $yy = [
            'come'     => $request->input('habito.come'),
            'desayuna' => $request->input('habito.desayuna'),
            'apetito'  => $request->input('habito.apetito'),
            'suenio'   => $request->input('habito.suenio'),
            'dormir'   => $request->input('habito.dormir'),
            'orina'    => $request->input('habito.orina'),
            'panales'  => $request->input('habito.panales'),
            'banio_so' => $request->input('habito.banio_so'),
            'estudiante_id'=>$request->input('id')
        ];

        $est  = Estudiante::where('id',$request->input('id'))->first();
        if($est != null){
            $est->update($data);
        }
        $enf  = Inf_Enfermedad::where('estudiante_id',$request->input('id'))->first();
        if($enf){
            $enf->save($xx);
        }else{
            $enf = Inf_Enfermedad::create($xx);
        }
        $habi = Habito::where('estudiante_id',$request->input('id'))->first();
        if($habi){
            $habi->save($yy);
        }else{
            $habi = Habito::create($yy);
        }

        if($est && $enf && $habi){
            DB::commit();
            return response()->json(true);
        }else{
            DB::rollBack();
            return response()->json(false);
        }
    }

    public function eliminarEstudiante(Request $request){
        return Estudiante::where('id',$request->input('id'))->delete();
    }

    public function cambiarStatus(Request $request){
        $output = Estudiante::where('id',$request->input('id'))->update(['status'=>0]);
        return response()->json($output);
    }
    //-------ejemplo
    public function verFecha(Request $request){
        return Helpers::formatDate($request->input('fecha'));
    }
    
    public function reportGraphEdades () {
        $tres = Estudiante::where('edad',3)->count();
        $cua = Estudiante::where('edad',4)->count();
        $cin = Estudiante::where('edad',5)->count();
        $data = ['tres' => $tres, 'cua' => $cua, 'cin' => $cin];
        return response()->json($data);
    }
    public function reportGraphCountGirlsAndBoy(){
        $girls = Estudiante::where('sexo',2)->count();
        $boys  = Estudiante::where('sexo',1)->count();
        $data = ['girls' => $girls,'boys' => $boys];
        return response()->json($data);
    }

    public function reportGraphCountSEA(){
        $sea = DB::select("SELECT COUNT(*) AS sea FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id = EN.estudiante_id) WHERE EN.sea = 1");
        $seaNo = DB::select("select count(*) as seaNo from estudiantes as E JOIN inf__enfermedads as EN on(E.id = EN.estudiante_id) where EN.sea = 2");
        //dd($sea[0]->sea,$seaNo[0]->seaNo);
        $data = ['sea'=>$sea[0]->sea,'seaNo'=>$seaNo[0]->seaNo];
        return response()->json($data);
    }

    public function reportGraphSoloBano(){
        $solo = DB::select("SELECT COUNT(*) AS SOLO FROM estudiantes AS E JOIN habitos AS H ON(E.id=H.estudiante_id) WHERE H.banio_so = 1");
        $noSolo = DB::select("SELECT COUNT(*) AS NOSOLO FROM estudiantes AS E JOIN habitos AS H ON(E.id=H.estudiante_id) WHERE H.banio_so = 2");
        $data = ['solo'=>$solo[0]->SOLO,'noSolo'=>$noSolo[0]->NOSOLO];
        return response()->json($data);
    }

    public function reportGraphDiscapacidadf(){
        $discapacidad = DB::select("SELECT COUNT(*) AS DISCA FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.condiFisica != 'N/A'");
        $discapacidadno = DB::select("SELECT COUNT(*) AS NODISCA FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.condiFisica = 'N/A'");
        $data = ['discapacidad'=>$discapacidad[0]->DISCA,'discapacidadno'=>$discapacidadno[0]->NODISCA];
        return response()->json($data);
    }

    public function reportGraphRetraso(){
        $retraso = DB::select("SELECT COUNT(*) AS RETRASO FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.retraso != 'N/A'");
        $retrasoNo = DB::select("SELECT COUNT(*) AS NORETRASO FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.retraso = 'N/A'");
        $data = ['retraso'=>$retraso[0]->RETRASO,'noRetraso'=>$retrasoNo[0]->NORETRASO];
        return response()->json($data);
    }

    public function reportGraphRegimenAlimenticio(){
        $regimen = DB::select("SELECT COUNT(*) AS regimen FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.regimen != 'N/A'");
        $sinRegimen = DB::select("SELECT COUNT(*) AS sinRegimen FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.regimen = 'N/A'");
        $data = ['regimen'=>$regimen[0]->regimen,'sinRegimen'=>$sinRegimen[0]->sinRegimen];
        return response()->json($data);
    }

    public function getAllStudienWithAll(){
         dd(Estudiante::with(['habito','enfermedad'])->get());
    }

    public function getInfStudent($request){
        $student = Estudiante::where('id',$request->input('id'))->first();

        if($student != null){
            $sickness = Inf_Enfermedad::where('estudiante_id',$student->id)->first();
            $habit    = Habito::where('estudiante_id',$student->id)->first();
            return ['estudiante'=>$student,'enfermedad'=>$sickness,"habito"=>$habit];
        }else
            return response()->json(false);

    }

    public function studenWithSec($idSecc){
        $idStu = Estu_secc::select('estudiante_id')->whereSeccionId($idSecc)->get();
        if($idStu != null || count($idStu) >= 1){
            $estudiantes = [];
            //dd($idStu);
            for($i = 0;$i < count($idStu); $i++){
                $est = Estudiante::whereId($idStu[$i]->estudiante_id)->whereSexo(2)->first();
                $estudiantes[] = $est;
            }
            return response()->json(['estudiantes'=>$estudiantes]);
        }else return response()->json(false);
    }

}
