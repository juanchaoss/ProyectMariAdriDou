<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Actividad;

class ActividadController extends Controller
{
    public function registrarActividad(Request $request){
        return Actividad::create($request->all());
    }

    public function traerTodosLosActividad(){
        return response()->json(Actividad::all());
    }

    public function obtenerActividadEspecifico(Request $request){
        return Actividad::where('id',$request->input('id'))->first();
    }

    public function getActivities(){
        return response()->json(Actividad::whereRaw("created_at >= NOW()")->get());
    }

    public function actualizarDatosActividad(Request $request){

        $data = ['tituloA'=> $request->input('tituloA'),
                 'fechaA' => $request->input('fechaA'),
                 'duraA'  => $request->input('duraA'),
                 'lugarA' => $request->input('lugarA'),
                 'descriA'=> $request->input('descriA'),];

        return response()->json(Actividad::where('id',$request->input('id'))->update($data));
    }

    public function eliminarActividad(Request $request){
        return response()->json(Actividad::where('id',$request->input('id'))->delete());
    }
}
