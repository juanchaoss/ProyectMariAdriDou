<?php

namespace App\Http\Controllers;

use App\Actividad;
use App\Docente;
use App\Estudiante;
use App\Seccion;
use Illuminate\Http\Request;
use App\Participa;
use DB;

class ParticipaController extends Controller
{
    public function saveParicipacion(Request $request){
        if($this->canSaveParti($request->input('actividad'))){

            if($request->input('todo') == 1){
                $input = [
                    'actividad_id' => $request->input('actividad'),
                    'seccion_id'   => null,
                    'todos'        => 1,
                    'docente_id'   => $request->input('docente'),
                    'valor'        => $request->input('valor')
                ];
                return response()->json(Participa::create($input));
            }else{

                try{
                    DB::beginTransaction();
                    for($i = 0;$i < count($request->input('estudiantes'));$i++){
                        $input = [
                            'actividad_id'  => $request->input('actividad'),
                            'seccion_id'    => $request->input('seccion'),
                            'todos'         => 0,
                            'estudiante_id' => $request->input('estudiantes')[$i],
                            'docente_id'    => $request->input('docente'),
                            'valor'         => $request->input('valor')
                        ];
                        Participa::create($input);
                    }
                    DB::commit();
                }catch (\Exception $e){
                    DB::rollBack();
                    return response()->json($e->getMessage());
                }
                return response()->json(true);
            }
        }else
            return response()->json(false);
    }

    public function getPartici(){//no sirve
        $all = Participa::with(['docentes'/*,'secciones'*/,'estudiantes'])->whereId(2)->get();
        dd($all);
    }

    public function canSaveParti($actividad_id){
        $parti = Participa::whereActividadId($actividad_id)->first();
        return $parti == null ? true : false;
    }

    public function reportGraphParticipa(){
        $si = Participa::where('valor',1)->count();
        $no  = Participa::where('valor',2)->count();
        $data = ['si' => $si,'no' => $no];
        return response()->json($data);
    }

    public function deleteInfParti(Request $request){
        return response()->json(Participa::whereActividadId($request->input('idAct'))->delete());
    }

    public function getInfPartiEsp(Request $request){
        $parti = Participa::whereActividadId($request->input('actId'))->first();
        if($parti->todos != 1){
            $idDoc  = Participa::select('docente_id')->whereActividadId($request->input('actId'))->get();
            $idAct  = Participa::select('actividad_id')->whereActividadId($request->input('actId'))->get();
            $idEstu = Participa::select('estudiante_id')->whereActividadId($request->input('actId'))->get();
            $idSecc = Participa::select('seccion_id')->whereActividadId($request->input('actId'))->get();

            $doc = Docente::whereId($idDoc[0]->docente_id)->first();
            $act = Actividad::whereId($idAct[0]->actividad_id)->first();
            $sec = Seccion::whereId($idSecc[0]->seccion_id)->first();
            $estudiantes = [];
            for($i = 0; $i < count($idEstu); $i++){
                $estu = Estudiante::whereId($idEstu[$i]->estudiante_id)->first();
                $estudiantes[] = $estu;
            }
            return response()->json(['actividad'=>$act,'docente'=>$doc,'seccion'=>$sec,'estudiantes'=>$estudiantes]);
        }
    }

    public function getParTodos(){
        $idsTodos = Participa::select('actividad_id')->whereTodos(1)->get();
        if(count($idsTodos) >= 1 || $idsTodos != null) {
            $activity = [];
            for ($i = 0; $i < count($idsTodos); $i++) {
                $inf = Actividad::whereId($idsTodos[$i]->actividad_id)->first();
                $activity[] = $inf;
            }
            return response()->json($activity);
        }else
            return response()->json(false);
    }

    public function getInfParti(){
        $data = [];

        $idsParti = DB::table('participa')->select('actividad_id','seccion_id','docente_id')->distinct()->whereTodos(0)->get();
        //dd($idsParti);
        for($i = 0; $i < count($idsParti); $i++){
            $infSecc = Seccion::whereId($idsParti[$i]->seccion_id)->first();
            $infActi = Actividad::whereId($idsParti[$i]->actividad_id)->first();
            $infDoc  = Docente::whereId($idsParti[$i]->docente_id)->first();
            $meta = ['seccion'=>$infSecc,'actividad'=>$infActi,'docente'=>$infDoc];
            $data[] = $meta;
        }
        //dd($data);
        return response()->json($data);

        //TEST THIS SHIT!!!
        //$idsEstu = [];
        /*for($i = 0; $i < count($idsParti); $i++){
            $idestu   = Participa::select('estudiante_id')
                                  ->where('actividad_id',$idsParti[$i]->estudiante_id)->first();
            $idsEstu[] = $idestu;
        }
        $infStu = [];
        for($i = 0; $i < count($idsEstu); $i++){
            $estu = Estudiante::whereId($idsEstu[$i]->estudiante_id)->first();
            $infStu[] = $estu;
        }*/
        //dd($idsParti);
        //dd($idsParti[0]->actividad_id);
    }
}