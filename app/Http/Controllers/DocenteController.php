<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;
use App\Seccion;
use App\SeccionDocente;

class DocenteController extends Controller
{
    public function registrarDocente(Request $request){
        
        if(count($this->checkEmailDocente($request->input('cedula'))) == 0){
            $data = ['nac_d'   => $request->input('nac_d'),
                    'cedula'   => $request->input('cedula'),
                    'apellido' => ucwords ($request->input('apellido')),
                    'nombre'   => ucwords ($request->input('nombre')),
                    'nivel'    => $request->input('nivel'),
                    'direccion'=> $request->input('direccion'),
                    'telefono' => $request->input('telefono')];

            return response()->json(Docente::create($data));
        }
        return response()->json(false);
    }

    public function checkEmailDocente($cedula){
        return Docente::where('cedula',$cedula)->get();
    }

    public function traerTodosLosDocentes(){
        return Docente::all();
    }

    public function docSec($seccion_id){
        $data = SeccionDocente::whereSeccionId($seccion_id)->first();
        $xx = Docente::whereId($data->docente_id)->first();
        return response()->json($xx);
    }

    public function traerUsuarioEspecifico(Request $request){
        return Docente::where('id',$request->input('id'))->firstOrFail();
    }

    public function actualizarDatosDocente(Request $request){

        $data = ['nac_d'   => $request->input('nac_d'),
                 'cedula'   => $request->input('cedula'),
                 'apellido' => ucwords ($request->input('apellido')),
                 'nombre'   => ucwords ($request->input('nombre')),
                 'nivel'    => $request->input('nivel'),
                 'direccion'=> $request->input('direccion'),
                 'telefono' => $request->input('telefono')];

        return Docente::where('id',$request->input('id'))->update($data);
    }

    public function eliminarDocente(Request $request){
        return Docente::where('id',$request->input('id'))->delete();
    }
}
