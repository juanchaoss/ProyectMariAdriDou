<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    public function createUser(Request $request){
        $data = [
            'name'     => $request->input('name'),
            'password' => bcrypt($request->input('password')),
            'email'    => $request->input('email')
        ];
        return response()->json(User::create($data));
    }

    public function getUserEsp(Request $request){
        return response()->json(User::where('id',$request->input('id'))->first());
    }

    public function getAllUsers(){
        return response()->json( DB::select("SELECT * FROM users limit 1,100000"));
    }

    public function updateUser(Request $request){
        $data = [
            'name'     => $request->input('name'),
            'password' => bcrypt($request->input('password')),
            //'email'    => $request->input('email')
        ];
        //dd($data);
        if($request->input('id') == 1)
            return response()->json(false);
        else
            return response()->json(User::where('id',$request->input('id'))->update($data));
    }

    public function deleteUser(Request $request){
        if($request->input('id') == 1)
            return response()->json(false);
        if($this->canEraseUser($request))
            return response()->json(User::where('id',$request->input('id'))->delete());
        else
            return response()->json(false);
    }

    public function canEraseUser(Request $request){
        return DB::table('users')->count() > 1;
    }
}
