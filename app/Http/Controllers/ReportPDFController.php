<?php

namespace App\Http\Controllers;
use App\Docente;
use App\Participa;
use App\Seccion;
use \Spipu\Html2Pdf\Html2Pdf;
use Illuminate\Http\Request;
use DB;
use App\Estudiante;
use App\Representante;
use App\Estu_secc;
use App\Actividad;

class ReportPDFController extends Controller
{

    public function validReportPDF(Request $request){
        switch ($request->input('typeReport')) {
            case 'allStu':
                return $this->caseAllStudient();
                break;
            case 'allTeach':
                return $this->caseAllDocentes();
                break;
            case 'allRep':
                return $this->caseAllRep();
                break;
            case 'allSec':
                return $this->caseAllSec();
                break;
            case 'sae':
                return $this->caseSae();
                break;
            case 'ret':
                return $this->caseRet();
                break;
            case 'reg':
                return $this->caseReg();
                break;
            case 'ban':
                return $this->caseBan();
                break;
            case 'estSex':
                return $this->caseStuSex();
                break;
            case 'estEdad':
                return $this->caseStuEdad();
                break;
            case 'disc':
                return $this->caseDiscapacidad();
                break;
            case 'allPar':
                return $this->caseParticipaciones();
                break;
            case 'viv':
                return $this->caseVivienda();
                break;
            default:
                return $this->caseTra();
                break;
        }
    }

    public function caseAllStudient(){
        $students = Estudiante::all();
        if(count($students) >= 1)
            return $this->makeStructureAllStudent($students);
        else
            return $this->reportFail();
    }

    public function caseParticipaciones(){
        return $this->makeStructurePaticipaciones();
    }


    public function caseAllDocentes(){
        $teachers = Docente::all();

        if(count($teachers) >= 1)
            return $this->makeStructureAllTeachers($teachers);
        else
            return $this->reportFail();
    }

    public function caseAllRep(){
        $repres = Representante::all();

        if(count($repres) >= 1)
            return $this->makeStructureAllRes($repres);
        else
            return $this->reportFail();
    }
    public function caseSpece(Request $xx){
         $spece = Estudiante::where('cedula',$xx->input('cedula'))->get();

        if($spece)
            return $this->makeStructureSpece($spece);
        else
            return $this->reportFail();
    }
    public function caseSpeceD(Request $yy){
         $speceD = Docente::where('cedula',$yy->input('cedula'))->get();

        if($speceD)
            return $this->makeStructureSpeceD($speceD);
        else
            return $this->reportFail();
    }
    public function caseSex(Request $zz){
        $sex = Estudiante::where('sexo',$zz->input('sexo'))->get();
        if($sex)
            return $this->makeStructureSex($sex);
        else
            return $this->reportFail();
    }
    public function caseSec(Request $ii){
        $ii->input('sec');
        $sec = DB::select("");
        if($sec)
            return $this->makeStructureSec($sec);
        else
            return $this->reportFail();
    }
    public function caseSpeceR (Request $ee){
        $re = Representante::where('cedula',$ee->input('cedula'))->get();
        if($re)
            return $this->makeStructureRe($re);
        else
            return $this->reportFail();
    }
    public function caseSae(){
        $sae = DB::select("SELECT * FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id = EN.estudiante_id) WHERE EN.sea = 1");

        if(count($sae) >= 1)
            return $this->makeStructureAllSae($sae);
        else
            return $this->reportFail();
    }
    public function caseRet(){
        $ret = DB::select("SELECT * FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.retraso != 'N/A'");

        if(count($ret) >= 1)
            return $this->makeStructureAllRet($ret);
        else
            return $this->reportFail();
    }
    public function caseReg(){
        $reg = DB::select("SELECT * FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.regimen != 'N/A'");

        if(count($reg) >= 1)
            return $this->makeStructureAllReg($reg);
        else
            return $this->reportFail();
    }
    public function caseDiscapacidad(){
        $dis = DB::select("SELECT * FROM estudiantes AS E JOIN inf__enfermedads AS EN ON(E.id=EN.estudiante_id) WHERE EN.condiFisica != 'N/A'");

        if(count($dis) >= 1)
            return $this->makeStructureDisca($dis);
        else
            return $this->reportFail();
    }
    public function caseBan(){
        $Ban = DB::select("SELECT * FROM estudiantes AS E JOIN habitos AS H ON(E.id=H.estudiante_id) WHERE H.banio_so = 1");

        if(count($Ban) >= 1)
            return $this->makeStructureAllBan($Ban);
        else
            return $this->reportFail();
    }
    public function caseTra(){
        $Tra = Representante::where('trabaja',1)->get();

        if(count($Tra) >= 1)
            return $this->makeStructureAllTra($Tra);
        else
            return $this->reportFail();
    }
    public function caseStuSex(){
        $ninas = Estudiante::where('sexo',2)->get();
        $ninos = Estudiante::where('sexo',1)->get();

        if(count($ninas) >= 1 || count($ninos) >= 1){
            return $this->makeStructureStuSex($ninas,$ninos);
        }
        else{
            return $this->reportFail();
        }
    }
    public function caseVivienda(){
        $rancho     = Representante::where('tipovi',1)->get();
        $apaquin    = Representante::where('tipovi',2)->get();
        $apaedi     = Representante::where('tipovi',3)->get();
        $casas      = Representante::where('tipovi',4)->get();
        $quintas    = Representante::where('tipovi',5)->get();

        if(count($rancho) >= 1 || count($apaquin) >= 1|| count($apaedi) >= 1|| count($casas) >= 1|| count($quintas) >= 1){
            return $this->makeStructureViv($rancho,$apaquin,$apaedi,$casas,$quintas);
        }
        else{
            return $this->reportFail();
        }
    }
    public function caseStuEdad(){
        $edadE = DB::select("SELECT * FROM estudiantes order by edad");

        if(count($edadE) >= 1){
            return $this->makeStructureStuEdad($edadE);
        }
        else{
            return $this->reportFail();
        }
    }
    public function caseMasSecc($idSecc){
        $idStu = Estu_secc::select('estudiante_id')->whereSeccionId($idSecc)->get();
        $seccion=[];
        $seccion=DB::select("SELECT idsecc FROM secciones WHERE id=$idSecc");
        $se = $seccion[0]->idsecc;
        if(count($idStu) >= 1 || $idStu != null){
            $estudiantes = [];
            for($i = 0;$i < count($idStu); $i++){
                $est = Estudiante::whereId($idStu[$i]->estudiante_id)->whereSexo(1)->first();
                $estudiantes[] = $est;
            }
            return $this->makeStructureMasSecc($estudiantes,$se);
        }
        else{
            return $this->reportFail();
        }
    }
    
    public function caseFemSecc($idSecc){
        $idStu = Estu_secc::select('estudiante_id')->whereSeccionId($idSecc)->get();
        $seccion=[];
        $seccion=DB::select("SELECT idsecc FROM secciones WHERE id=$idSecc");
        $se = $seccion[0]->idsecc;
        if(count($idStu) >= 1 || $idStu != null){
            $estudiantes = [];
            for($i = 0;$i < count($idStu); $i++){
                $est = Estudiante::whereId($idStu[$i]->estudiante_id)->whereSexo(2)->first();
                $estudiantes[] = $est;
            }
            return $this->makeStructureFemSecc($estudiantes,$se);
        }
        else{
            return $this->reportFail();
        }
    }
    public function caseSoloSec($idSecc){
        $idStu = Estu_secc::select('estudiante_id')->whereSeccionId($idSecc)->get();
        $seccion=[];
        $seccion=DB::select("SELECT idsecc FROM secciones WHERE id=$idSecc");
        $se = $seccion[0]->idsecc;
        if(count($idStu) >= 1 || $idStu != null){
            $estudiantes = [];
            for($i = 0;$i < count($idStu); $i++){
                $est = Estudiante::whereId($idStu[$i]->estudiante_id)->first();
                $estudiantes[] = $est;
            }
            return $this->makeStructureSoloSec($estudiantes,$se);
        }
        else{
            return $this->reportFail();
        }
    }

    public function reportFail(){
        return $this->PDFE(0,"<p>Sin data para mostrar</p>");
    }

    public function makeStructureAllStudent($students){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($students); $i++){
            $naci      = $students[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $students[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $students[$i]->nombres;
            $apellidos = $students[$i]->apellidos;
            $fecha     = $students[$i]->fechaNa;
            $edad      = $students[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(1,$tag);
    }

    public function makeStructurePaticipaciones(){
        $idspar = DB::table('participa')->select(DB::raw("DISTINCT actividad_id"))->get();
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Actividad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sección&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Docente&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Estudiantes&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        $list = "";
        $bodyTodo = "";

        for($i = 0; $i < count($idspar); $i++){

            $parti = Participa::whereActividadId($idspar[$i]->actividad_id)->first();

            if($parti->todos == 1){
                $parti = Actividad::whereId($parti->actividad_id)->first();
                $nameAct = $parti->tituloA;
                $bodyTodo .= "
                    <tr>
                        <td>$nameAct</td>
                        <td>Todos</td>
                        <td>Todos</td>
                        <td>Todos</td>
                    </tr>";
            }else{
                //get id of all
                $idSecc = Participa::select('seccion_id')->whereActividadId($idspar[$i]->actividad_id)->first();
                $idact  = Participa::select('actividad_id')->whereActividadId($idspar[$i]->actividad_id)->first();
                $iddoc  = Participa::select('docente_id')->whereActividadId($idspar[$i]->actividad_id)->first();
                $idest  = Participa::select('estudiante_id')->whereActividadId($idspar[$i]->actividad_id)->get();
                //get inf
                $infDoc  = Docente::whereId($iddoc->docente_id)->first();
                $infosecc = Seccion::whereId($idSecc->seccion_id)->first();
                $infAct  = Actividad::whereId($idact->actividad_id)->first();

                $nomDoc   = $infDoc->nombre;
                $idenSecc = $infosecc->idsecc;
                $nomAct   = $infAct->tituloA;

                //dd($idest,$iddoc,$iddoc,$idact,$idsecc); ---> DO THIS TO DEBUG THE PROCESS
                $body .= "
                    <tr>
                        <td>$nomAct</td>
                        <td>$idenSecc</td>
                        <td>$nomDoc</td>
                        <td><ul>";

                for($j = 0; $j < count($idest); $j++){
                    $estu = Estudiante::whereId($idest[$j]->estudiante_id)->first();
                    $nombres = $estu->nombres;
                    $list .="<li>$nombres</li>";
                }
                $listNa = $list;
                $body .=$listNa."</ul></td></tr>";
                $list = null;
            }
        }
        $tag = $html.$body.$bodyTodo."</table>";
        return $this->PDFE(22,$tag);
    }

    public function makeStructureAllTeachers($teachers){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Cedula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Telefono&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($teachers); $i++){
            $telefono  = $teachers[$i]->telefono;
            $nombres   = $teachers[$i]->nombre;
            $apellidos = $teachers[$i]->apellido;
            $direccion = $teachers[$i]->direccion;
            $cedula    = $teachers[$i]->cedula;
            $body .= "
                    <tr>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$direccion</td>
                        <td>$telefono</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(2,$tag);
    }

    public function makeStructureAllRes($repres){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Telefono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($repres); $i++){
            $nacionalidad  = $repres[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono  = $repres[$i]->telefono;
            $nombres   = $repres[$i]->nombre;
            $apellidos = $repres[$i]->apellido;
            $direccion = $repres[$i]->direcRD;
            $cedula    = $repres[$i]->cedula;
            $direcciont    = $repres[$i]->direcRT;

            $body .= "
                    <tr>
                        <td>$nacionalidad</td>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$direccion</td>
                        <td>$direcciont</td>
                        <td>$telefono</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(3,$tag);
    }

    public function makeStructureAllSec($secci){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Sección&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Turno&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edades comprendidas&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Observaciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($secci); $i++){
            $seccion  = $secci[$i]->idsecc;
            $turno  = $secci[$i]->turnosecc;
            $desde   = $secci[$i]->edaddesde;
            $hasta = $secci[$i]->edadhasta;
            $obser = $secci[$i]->obser;
            $body .= "
                    <tr>
                        <td>$seccion</td>
                        <td>$turno</td>
                        <td>de $desde a $hasta años</td>
                        <td>$obser</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(4,$tag);
    }
    public function makeStructureSpece($spece){
        $naci      = $spece[0]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
        $sexo      = $spece[0]->sexo = 1 ? "Masculino" : "Femenino";
        $nombres   = $spece[0]->nombres;
        $apellidos = $spece[0]->apellidos;
        $fecha     = $spece[0]->fechaNa;
        $edad      = $spece[0]->edad;
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>
                </table>";
        return $this->PDFE(5,$html);
    }
    public function makeStructureAllSae($sae){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($sae); $i++){
            $naci      = $sae[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $sae[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $sae[$i]->nombres;
            $apellidos = $sae[$i]->apellidos;
            $fecha     = $sae[$i]->fechaNa;
            $edad      = $sae[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(6,$tag);
    }
    public function makeStructureAllRet($ret){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($ret); $i++){
            $naci      = $ret[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $ret[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $ret[$i]->nombres;
            $apellidos = $ret[$i]->apellidos;
            $fecha     = $ret[$i]->fechaNa;
            $edad      = $ret[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(7,$tag);
    }
    public function makeStructureAllReg($reg){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($reg); $i++){
            $naci      = $reg[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $reg[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $reg[$i]->nombres;
            $apellidos = $reg[$i]->apellidos;
            $fecha     = $reg[$i]->fechaNa;
            $edad      = $reg[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(8,$tag);
    }
    public function makeStructureDisca($dis){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($dis); $i++){
            $naci      = $dis[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $dis[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $dis[$i]->nombres;
            $apellidos = $dis[$i]->apellidos;
            $fecha     = $dis[$i]->fechaNa;
            $edad      = $dis[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(20,$tag);
    }
    public function makeStructureAllBan($ban){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($ban); $i++){
            $naci      = $ban[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $ban[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $ban[$i]->nombres;
            $apellidos = $ban[$i]->apellidos;
            $fecha     = $ban[$i]->fechaNa;
            $edad      = $ban[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(9,$tag);
    }
    public function makeStructureAllTra($Tra){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Telefono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($Tra); $i++){
            $nacionalidad  = $Tra[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono  = $Tra[$i]->telefono;
            $nombres   = $Tra[$i]->nombre;
            $apellidos = $Tra[$i]->apellido;
            $direccion = $Tra[$i]->direcRD;
            $cedula    = $Tra[$i]->cedula;
            $direcciont    = $Tra[$i]->direcRT;

            $body .= "
                    <tr>
                        <td>$nacionalidad</td>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$telefono</td>
                        <td>$direcciont</td>
                        <td>$direccion</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(10,$tag);
    }
    public function makeStructureStuSex($ninas,$ninos){
        $html = "<table text-align='center'>
                    <tr><td bgcolor='#D7D7D7' colspan='6'>Niñas</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($ninas); $i++){
            $naci      = $ninas[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $nombres   = $ninas[$i]->nombres;
            $apellidos = $ninas[$i]->apellidos;
            $fecha     = $ninas[$i]->fechaNa;
            $edad      = $ninas[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                    </tr>";
        }
        $html2 = "<tr><td bgcolor='#D7D7D7' colspan='6'>Niños</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body2 = "";
        for($i = 0; $i < count($ninos); $i++){
            $naci2      = $ninos[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $nombres2   = $ninos[$i]->nombres;
            $apellidos2 = $ninos[$i]->apellidos;
            $fecha2     = $ninos[$i]->fechaNa;
            $edad2      = $ninos[$i]->edad;
            $body2 .= "
                    <tr>
                        <td>$nombres2</td>
                        <td>$apellidos2</td>
                        <td>$fecha2</td>
                        <td>$edad2</td>
                        <td>$naci2</td>
                    </tr>";
        }
        $tag = $html.$body.$html2.$body2."</table>";
        return $this->PDFE(11,$tag);
    }
    public function makeStructureViv($rancho,$apaquin,$apaedi,$casas,$quintas){
        $html = "<table text-align='center'>
                    <tr><td bgcolor='#D7D7D7' colspan='7'>Representantes que viven en Ranchos</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($rancho); $i++){
            $nacionalidad  = $rancho[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono      = $rancho[$i]->telefono;
            $nombres       = $rancho[$i]->nombre;
            $apellidos     = $rancho[$i]->apellido;
            $direccion     = $rancho[$i]->direcRD;
            $cedula        = $rancho[$i]->cedula;
            $direcciont    = $rancho[$i]->direcRT;

            $body .= "
                    <tr>
                        <td>$nacionalidad</td>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$telefono</td>
                        <td>$direcciont</td>
                        <td>$direccion</td>
                    </tr>";
        }
        $html2 = "<tr><td bgcolor='#D7D7D7' colspan='7'>Representantes que viven en Apartamentos en quintas o casas</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body2 = "";
        for($i = 0; $i < count($apaquin); $i++){
            $nacionalidad2  = $apaquin[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono2      = $apaquin[$i]->telefono;
            $nombres2       = $apaquin[$i]->nombre;
            $apellidos2     = $apaquin[$i]->apellido;
            $direccion2     = $apaquin[$i]->direcRD;
            $cedula2        = $apaquin[$i]->cedula;
            $direcciont2    = $apaquin[$i]->direcRT;

            $body2 .= "
                    <tr>
                        <td>$nacionalidad2</td>
                        <td>$cedula2</td>
                        <td>$nombres2</td>
                        <td>$apellidos2</td>
                        <td>$telefono2</td>
                        <td>$direcciont2</td>
                        <td>$direccion2</td>
                    </tr>";
        }
        $html3 = "<tr><td bgcolor='#D7D7D7' colspan='7'>Representantes que viven en Apartamentos en edificios</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body3 = "";
        for($i = 0; $i < count($apaedi); $i++){
            $nacionalidad3  = $apaedi[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono3      = $apaedi[$i]->telefono;
            $nombres3       = $apaedi[$i]->nombre;
            $apellidos3     = $apaedi[$i]->apellido;
            $direccion3     = $apaedi[$i]->direcRD;
            $cedula3        = $apaedi[$i]->cedula;
            $direcciont3    = $apaedi[$i]->direcRT;

            $body3 .= "
                    <tr>
                        <td>$nacionalidad3</td>
                        <td>$cedula3</td>
                        <td>$nombres3</td>
                        <td>$apellidos3</td>
                        <td>$telefono3</td>
                        <td>$direcciont3</td>
                        <td>$direccion3</td>
                    </tr>";
        }
        $html4 = "<tr><td bgcolor='#D7D7D7' colspan='7'>Representantes que viven en Casas</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body4 = "";
        for($i = 0; $i < count($casas); $i++){
            $nacionalidad4  = $casas[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono4      = $casas[$i]->telefono;
            $nombres4       = $casas[$i]->nombre;
            $apellidos4     = $casas[$i]->apellido;
            $direccion4     = $casas[$i]->direcRD;
            $cedula4        = $casas[$i]->cedula;
            $direcciont4    = $casas[$i]->direcRT;

            $body4 .= "
                    <tr>
                        <td>$nacionalidad4</td>
                        <td>$cedula4</td>
                        <td>$nombres4</td>
                        <td>$apellidos4</td>
                        <td>$telefono4</td>
                        <td>$direcciont4</td>
                        <td>$direccion4</td>
                    </tr>";
        }
        $html5 = "<tr><td bgcolor='#D7D7D7' colspan='7'>Representantes que viven en Quintas</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Teléfono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body5 = "";
        for($i = 0; $i < count($quintas); $i++){
            $nacionalidad5  = $quintas[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono5      = $quintas[$i]->telefono;
            $nombres5       = $quintas[$i]->nombre;
            $apellidos5     = $quintas[$i]->apellido;
            $direccion5     = $quintas[$i]->direcRD;
            $cedula5        = $quintas[$i]->cedula;
            $direcciont5    = $quintas[$i]->direcRT;

            $body5 .= "
                    <tr>
                        <td>$nacionalidad5</td>
                        <td>$cedula5</td>
                        <td>$nombres5</td>
                        <td>$apellidos5</td>
                        <td>$telefono5</td>
                        <td>$direcciont5</td>
                        <td>$direccion5</td>
                    </tr>";
        }
        $tag = $html.$body.$html2.$body2.$html3.$body3.$html4.$body4.$html5.$body5."</table>";
        return $this->PDFE(21,$tag);
    }
    public function makeStructureStuEdad($edadE){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($edadE); $i++){
            $naci      = $edadE[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $edadE[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $edadE[$i]->nombres;
            $apellidos = $edadE[$i]->apellidos;
            $fecha     = $edadE[$i]->fechaNa;
            $edad      = $edadE[$i]->edad;
            $body .= "
                    <tr>
                        <td>$edad</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(12,$tag);
    }
    public function makeStructureStuSecc($StuSec){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($StuSec); $i++){
            $naci      = $StuSec[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $StuSec[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $StuSec[$i]->nombres;
            $apellidos = $StuSec[$i]->apellidos;
            $fecha     = $StuSec[$i]->fechaNa;
            $edad      = $StuSec[$i]->edad;
            $body .= "
                    <tr>
                        <td>$edad</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(13,$tag);
    }
    public function makeStructureSpeceD($speceD){
            $telefono  = $speceD[0]->telefono;
            $nombres   = $speceD[0]->nombre;
            $apellidos = $speceD[0]->apellido;
            $direccion = $speceD[0]->direccion;
            $cedula    = $speceD[0]->cedula;
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Cedula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Telefono&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$direccion</td>
                        <td>$telefono</td>
                    </tr>
                </table>";
        return $this->PDFE(14,$html);
    }
    public function makeStructureSex($sex){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($sex); $i++){
            $naci      = $sex[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $nombres   = $sex[$i]->nombres;
            $apellidos = $sex[$i]->apellidos;
            $fecha     = $sex[$i]->fechaNa;
            $edad      = $sex[$i]->edad;
            $body .= "
                    <tr>
                        <td>$edad</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$naci</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(11,$tag);
    }
    public function makeStructureSec($sec){
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($sec); $i++){
            $naci      = $sec[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $sec[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $sec[$i]->nombres;
            $apellidos = $sec[$i]->apellidos;
            $fecha     = $sec[$i]->fechaNa;
            $edad      = $sec[$i]->edad;
            $body .= "
                    <tr>
                        <td>$edad</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(15,$tag);
    }
    public function makeStructureRe($re){
            $nacionalidad  = $re[0]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $telefono  = $re[0]->telefono;
            $nombres   = $re[0]->nombre;
            $apellidos = $re[0]->apellido;
            $direccion = $re[0]->direcRD;
            $cedula    = $re[0]->cedula;
            $direcciont    = $re[0]->direcRT;
        $html = "<table text-align='center'>
                    <tr bgcolor='#D7D7D7'>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Cédula&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Telefono&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Vivienda&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Dirección Trabajo&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td>$nacionalidad</td>
                        <td>$cedula</td>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$telefono</td>
                        <td>$direcciont</td>
                        <td>$direccion</td>
                    </tr>
                </table>";
        return $this->PDFE(16,$html);
    }
    public function makeStructureSoloSec($estudiantes,$se){
        $html = "<table text-align='center'>
                    <tr><td colspan='6' bgcolor='#D7D7D7' text-align='center'>Sección: $se</td></tr> 
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($estudiantes); $i++){
            if($estudiantes[$i] != null){
            $naci      = $estudiantes[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $estudiantes[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $estudiantes[$i]->nombres;
            $apellidos = $estudiantes[$i]->apellidos;
            $fecha     = $estudiantes[$i]->fechaNa;
            $edad      = $estudiantes[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
            }
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(18,$tag);
    }
    public function makeStructureMasSecc($estudiantes,$se){
        $html = "<table text-align='center'>
                    <tr><td colspan='6' bgcolor='#D7D7D7' text-align='center'>Sección: $se</td></tr> 
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Na.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($estudiantes); $i++){
            if($estudiantes[$i] != null){
                $naci      = $estudiantes[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
                $sexo      = $estudiantes[$i]->sexo = 1 ? "Masculino" : "Femenino";
                $nombres   = $estudiantes[$i]->nombres;
                $apellidos = $estudiantes[$i]->apellidos;
                $fecha     = $estudiantes[$i]->fechaNa;
                $edad      = $estudiantes[$i]->edad;
                $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
            }
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(17,$tag);
    }
    public function makeStructureFemSecc($estudiantes,$se){
        $html = "<table text-align='center'>
                    <tr><td colspan='6' bgcolor='#D7D7D7' text-align='center'>Sección $se</td></tr>
                    <tr bgcolor='#F4F3F3'>
                        <td>&nbsp;&nbsp;&nbsp;Nombres&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Apellidos&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Fecha de Nac.&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Edad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Nacionalidad&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;Sexo&nbsp;&nbsp;&nbsp;</td>
                    </tr>";
        $body = "";
        for($i = 0; $i < count($estudiantes); $i++){
            if($estudiantes[$i] != null){
            $naci      = $estudiantes[$i]->nacionalidad = 1 ? "Venezolano" : "Extrangero";
            $sexo      = $estudiantes[$i]->sexo = 1 ? "Masculino" : "Femenino";
            $nombres   = $estudiantes[$i]->nombres;
            $apellidos = $estudiantes[$i]->apellidos;
            $fecha     = $estudiantes[$i]->fechaNa;
            $edad      = $estudiantes[$i]->edad;
            $body .= "
                    <tr>
                        <td>$nombres</td>
                        <td>$apellidos</td>
                        <td>$fecha</td>
                        <td>$edad</td>
                        <td>$naci</td>
                        <td>$sexo</td>
                    </tr>";
            }
        }
        $tag = $html.$body."</table>";
        return $this->PDFE(19,$tag);
    }
    public function PDFE($case,$data){
        $path = public_path('img/mem.png');
        $mem  = "<img src='$path' style='float:center; width:100%'><br>";
        if($case == 1){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de todos los Estudiantes</h3><br>";
            $endHtml = $mem.$header.$data;
        }

        if($case == 2){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de todos los Docentes</h3><br>";
            $endHtml = $mem.$header.$data;
        }

        if($case == 3){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de todos los Representantes</h3><br>";
            $endHtml = $mem.$header.$data;
        }

        if($case == 4){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de todas las Secciones</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 5){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Estudiante en específico</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 6){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de Estudiantes que hacen uso del SAE</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 7){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de Estudiantes con alguna discapacidad de apredizaje</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 8){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de Estudiantes con algún régimen alimenticio</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 9){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de Estudiantes que van solos al baño</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 10){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de Representantes que trabajan</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 11){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de estudiantes por su sexo</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 12){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de estudiantes ordenados por edad</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 14){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Docente en específico</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 15){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Listado de estudiantes por sección</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 16){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Representante en específico</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 17){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Estudiantes de sexo masculino de la sección seleccionada</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 19){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Estudiantes de sexo femenino de la sección seleccionada</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 18){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Estudiantes de la sección seleccionada</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 20){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Estudiantes con alguna discapacidad física</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 21){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Representantes con sus tipos de vivienda</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 22){
            $header = "<h2 text-align='center'><u>Reporte</u></h2><h3 text-align='center'>Participaciones</h3><br>";
            $endHtml = $mem.$header.$data;
        }
        if($case == 0){
            $header = "<h2 text-align='center'><u>Reporte vacío</u></h2>";
            $endHtml = $mem.$header.$data;
        }

    	$pdf = new Html2Pdf('P','A4','en');
    	$pdf->writeHtml($endHtml);
    	return $pdf->output();
    }
}
