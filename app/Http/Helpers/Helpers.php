<?php
namespace  App\Http\Helpers;

class Helpers{

    public static function formatDate($date,$typeFormat){
        if ($typeFormat == 'AAAA-MM-DD'){
            $finalDate = explode('/',$date);
            return $finalDate[2]."-".$finalDate[1]."-".$finalDate[0];
        }
    }

    public static function getDateView($data){
        $dateView = explode('-',$data);
        return $dateView[2]."/".$dateView[1]."/".$dateView[0];
    }
}