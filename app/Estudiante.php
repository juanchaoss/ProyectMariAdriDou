<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Estudiante
 *
 * @property int $id
 * @property int $representante_id
 * @property string $nombres
 * @property string $apellidos
 * @property string $fechaNa
 * @property int $nacionalidad
 * @property int $edad
 * @property int $sexo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $cedula
 * @property int $status
 * @property-read \App\Inf_Enfermedad $enfermedad
 * @property-read \App\Habito $habito
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Participa[] $participacion
 * @property-read \App\Representante $representante
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Seccion[] $seccion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereCedula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereEdad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereFechaNa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereNacionalidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereNombres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereRepresentanteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereSexo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Estudiante whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Estudiante extends Model
{

    protected $table = "estudiantes";
    protected $fillable = ['representante_id','cedula','nombres','apellidos','fechaNa','nacionalidad','edad','sexo'];

    public function representante(){
        return $this->belongsTo('App\Representante');
    }

    public function enfermedad(){
        return $this->hasOne('App\Inf_Enfermedad');
    }

    public function habito(){
        return $this->hasOne('App\Habito');
    }

    public function seccion(){
        return $this->belongsToMany('App\Seccion','estu_seccs');
    }


    public function actividades(){
        return $this->belongsToMany('App\Actividad');
    }
}
