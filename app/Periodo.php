<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Periodo
 *
 * @property int $id
 * @property string $fechaI
 * @property string $fechaF
 * @property string $descriP
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Seccion[] $secciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereDescriP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereFechaF($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereFechaI($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Periodo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Periodo extends Model
{
    protected $table = "periodo";
    protected $fillable = ['fechaI','fechaF','descriP','status'];

    public function secciones(){
        return $this->hasMany('App\Seccion');
    }
}
