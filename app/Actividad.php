<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Actividad
 *
 * @property int $id
 * @property string $tituloA
 * @property string $fechaA
 * @property string $duraA
 * @property string $lugarA
 * @property string $descriA
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Participa[] $participa
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereDescriA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereDuraA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereFechaA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereLugarA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereTituloA($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Actividad whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Actividad extends Model
{
    protected $table = "actividad";
    protected $fillable = ['tituloA','fechaA','duraA','lugarA','descriA'];

    public function estudiantes(){
        return $this->belongsToMany('App\Estudiante','participa');
    }
}
