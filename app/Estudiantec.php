<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Estudiantec
 *
 * @mixin \Eloquent
 */
class Estudiantec extends Model
{
    protected $table = "estudiantesc";
    protected $fillable = ['enfer','enfermedad','fiebre','condi','condicionf','retraso','retrasoa','alergia','regimen','sae'];
}
