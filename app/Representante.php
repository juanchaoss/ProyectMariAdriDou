<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Representante
 *
 * @property int $id
 * @property int $nacionalidad
 * @property string $cedula
 * @property string $apellido
 * @property string $nombre
 * @property string $telefono
 * @property string $nivel
 * @property string $direcRD
 * @property int $tipovi
 * @property int $trabaja
 * @property string $direcRT
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Estudiante[] $estudiantes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereApellido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereCedula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereDirecRD($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereDirecRT($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereNacionalidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereNivel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereTipovi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereTrabaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Representante whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Representante extends Model
{
    protected $table = "representantes";
    protected $fillable = ['nacionalidad','cedula','apellido','nombre',
        'telefono','nivel','direcRD','tipovi','trabaja', 'direcRT'];

    public function estudiantes(){
        return $this->hasMany('App\Estudiante');
    }
}
