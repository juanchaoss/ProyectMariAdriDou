<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Participa
 *
 * @property int $id
 * @property int|null $actividad_id
 * @property int|null $seccion_id
 * @property int|null $estudiante_id
 * @property int $todos
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $docente_id
 * @property-read \App\Actividad|null $actividades
 * @property-read \App\Docente|null $docentes
 * @property-read \App\Estudiante|null $estudiantes
 * @property-read \App\Seccion|null $secciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereActividadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereDocenteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereEstudianteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereSeccionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereTodos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Participa whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Participa extends Model
{
    protected $table = "participa";
    protected $fillable = ['actividad_id','seccion_id','estudiante_id','todos', 'valor','docente_id'];


	public function actividades(){
       	return $this->belongsTo('App\Actividad','actividad_id');
    }

	public function secciones(){
        return $this->belongsTo('App\Seccion','seccion_id');
    }

	public function estudiantes(){
        return $this->belongsTo('App\Estudiante','estudiante_id');
    }

    public function docentes(){
        return $this->belongsTo('App\Docente','docente_id');
    }
}