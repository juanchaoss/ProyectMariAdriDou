<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\SeccionDocente
 *
 * @property int $id
 * @property int|null $docente_id
 * @property int|null $seccion_id
 * @property string $periodo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente whereDocenteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente wherePeriodo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente whereSeccionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeccionDocente whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SeccionDocente extends Model
{
    protected $table = "secciones_docentes";
    protected $fillable = ['docente_id','seccion_id','periodo'];
}
